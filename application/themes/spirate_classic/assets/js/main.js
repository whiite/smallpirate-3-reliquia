/* Main.js */

// !? TODO:
// - amd system
// - integrate require.js

// load core when document is ready
$(document).ready(function(){
	sp.init()
});

/* set main functions/objects */

// lang object
var lang = lang || {};

// main object
var sp = {
	
	init : function(){
		
		// load tipsy context
		$('.tipsy').tipsy();
		
		// load dropdowns
		$('.dropdown-area').each(function(){
		
			sp.dialog({
				'target' : this,
				'bind' : 'hover',
				'body' : $($(this).attr('target-dropdown')).html(),
				'dropdown' : true
			});
		
		});
		
		// set register button onclick event
		$('a#register').click(function(){
			
			sp.modal.set({
				'title' : 'Registro',
				'body' : $('#register-form').html(),
				'buttons' : [
					{
						'label' : 'Siguiente',
						'callback' : function(){
							
							sp.modal.alert('Bienvenido!', 'Has sido registrado exitosamente').set_class('success');
							
							
						}
					},
					{
						'label' : 'Cancelar',
						'class' : 'clear',
						'callback' : 'close'
					}
				]
			}).show();

			return false;
			
		});
		
		// set user functions
		
		if( user.logged_in )
		{
			
			var
			user_options = [
				{
					'target' : '#uicon_notifications',
					'body' : ' ',
					'bind' : 'click',
					'width' : 150,
					'position' : 'center'
				},				
				{
					'target' : '#uicon_messages',
					'body' : ' ',
					'bind' : 'click',
					'width' : 150,
					'position' : 'center'
				},
				{
					'target' : '#uicon_bookmarks',
					'body' : ' ',
					'bind' : 'click',
					'width' : 150,
					'position' : 'center'
				},
				{
					'target' : '#menu_username',
					'obj' : '#user-dropdown',
					'bind' : 'hover',
					'dropdown' : true,
					'width' : 150,
					'padding-y' : 17,
					'position' : 'center'
				}
			];

			for( var option in user_options)
				sp.dialog(user_options[option]);

			/*
			sp.notifications.settings({
				'cookie' : true,
				'favicon-alert' : true
			}).start([
				notifications : {
					'receive_url' : global.base_url + 'notifications/get',
					'post_data' : {},
					'alert_bubble' : ['#uicon_notifications', true, true],
					'count' : 10
				},
				messages : {
					'receive_url' : global.base_url + 'messages/get',
					'post_data' : {},
					'count' : 3
				}
			]).
			*/
			
			sp.alert_bubbles.init({
			'notifications' : {
			    'parent' : '#uicon_notifications',
			    'count' : 10,
			    'animate' : true,
			    'anim-counter' : true
			},
			'messages' : {
			    'parent' : '#uicon_messages',
			    'count' : 3,
			    'animate' : true,
			    'anim-counter' : true
			}}).show();
			
			Tinycon.setBubble(6);

		}
		else
			sp.dialog({
				'target' : 'a#login',
				'body' : $('#login-form').html(),
				'position' : 'center'
			});
		
	}, /* end sp.__ */
	
	modal : {
		
		dialog			: false,
		opened			: false,
		loading			: null,
		configured		: false,
		settings		: {},
		parent_dialog	: {},
		dialog_settings	: {},
		
		set : function(settings){
			
			var dialog_settings = {
					'modal' : true,
					'buttons' : [],
					'draggable' : false,
					'resizable' : false,
					'width' : 370,
					'dialogClass' : 'sp-modal'
				},
				settings_callbacks = {
					'body' : 'set_body',
					'title' : 'set_title',
					'height' : 'set_height',
					'width' : 'set_width'
				},
				previous_callbacks = {};
			
			// set default settings
			this.settings = {
				'no-close' : false,
				'animate-close' : false,
				'buttons-class' : 'sp-button'
			}
			
			if( sp.modal.configured === true )
				return this;
			
			this.dialog = $('<div />').attr('id', 'sp-dialog');
				
			for( var key in settings ){
				
				var value = settings[key];
				
				/* modal settings */
				
				// booleans
				if( $.inArray(key, new Array('no-close', 'animate-close')) !== -1 )
				{
					
					sp.modal.settings[key] = Boolean(value);
					
					continue;
				}
				
				// callbacks
				if( $.inArray(key, new Array('after-close', 'before-close', 'before-load', 'after-load')) !== -1 )
				{
					
					if(typeof val == 'string' && sp.modal._is_modal_function(value)){
						
						var internal_callback = value;
						
						value = function(){
							
							sp.modal[internal_callback]();
							
						}
						
					}
					
					dialog_settings[key] = value;
					continue;
				}
				
				/* dialog settings */
				
				// overlay
				if( key == 'overlay' )
				{
					dialog_settings['modal'] = Boolean(value);
					continue;
				}
				
				// make buttons
				if( key == 'buttons' )
				{
					
					var button_settings = new Array();
					
					for( var button in value ){
						
						var settings_length = array_keys(value[button]).length,
							index = 0;
						
						for( var setting in value[button]){
							
							var val = value[button][setting];
							
							if( setting == 'label' )
								button_settings['label'] = val;
							
							if( setting == 'callback' ){
								
								if( typeof val == 'string' ){
									
									// match function
									var function_regexp, func_match, func, args, arguments = new Array();
									
									function_regexp = new RegExp(/([a-zA-Z_]+)(\(([\,]?[a-zA-Z0-9]+[^\)]+)\))?/);
									func_match = (val.replace(/\s+/g, '')).match(function_regexp);
									
									if( func_match !== null )
									{
										
									
										func = func_match[1];
										args_source = typeof func_match[3] !== 'undefined' ? explode(',', func_match[3]) : '';
										
										if (args_source != '')
											for(var arg in args_source){ 
												
												if( !isNaN(parseInt(args_source[arg]))  || (args_source[arg] == 'true' || args_source[arg] == 'false') )
													arguments.push(args_source[arg]);
												else
													arguments.push('"' + args_source[arg] + '"');
												
											}
									
										arguments = implode(',', arguments);
										
										if( sp.modal._is_modal_function(func) )
											val = eval('(function (){sp.modal.' + func + '(' + arguments + ')});');
									}
								}
								
								button_settings['callback'] = val;
								
							}
							
							if( setting == 'class' )
								button_settings['class'] = val;
							
							index++;
							
							if( index >= settings_length )
							{
								
								dialog_settings['buttons'].push({
									
									text : button_settings['label'],
									'class' : implode(' ', new Array(
											sp.modal.settings['buttons-class'],
											button_settings['class']
										)),
									click : button_settings['callback']
								});
								
								index = 0;
							}
						}
					}
				}
				
				if( $.inArray(key, array_keys(settings_callbacks)) !== -1 )
				{
					
					if( $.inArray(key, sp.modal.settings) !== -1 )
						sp.modal.settings[key] = value;
					
					// call this functions when dialog is called
					previous_callbacks[settings_callbacks[key]] = value;
					
				}
				else
					continue;
					
			}
			
			// make $.dialog settings
			this.dialog_settings = dialog_settings;
			
			// initialize modal
			$(this.dialog ).dialog(this.dialog_settings).dialog('close');
			
			
			// set and get .ui-dialog
			this.parent_dialog = $(this.dialog).parents().filter('.ui-dialog');
			
			// set close event
			$(sp.modal.dialog).dialog({
				close : function(){
					sp.modal.close();
				}
			});
			
			// wrap close button
			$('<div />')
			.addClass('close-button clearfix rounded-top-right')
			.appendTo($(this.parent_dialog).find('.ui-dialog-titlebar'));
			
			$(this.parent_dialog)
			.find('.ui-dialog-titlebar-close')
			.remove();
			
			$('<a />')
			.addClass('ui-dialog-titlebar-close')
			.bind('click', function(){
				
				$(sp.modal.dialog).dialog('close');
				
			})
			.appendTo($(this.parent_dialog).find('.ui-dialog-titlebar > .close-button'));
			
			// change panel buttons
			$(this.parent_dialog).find('.ui-dialog-buttonpane button').each(function(i, event){
				
				var event_onclick = jQuery._data($(this)[0]).events.click[0].handler;
				
				var new_button = $(this).setElementType('a');
					new_button = new_button[0][0];
				
				$(new_button).bind('click', function(){
					
					event_onclick();
					
				});
				
			});
			
			// previous callbacks
			for( var func in previous_callbacks ){
				
				var arg = previous_callbacks[func];
				
				sp.modal[func](arg);
				
			}
			
			// okay, modal is configured
			sp.modal.configured = true;
			
			// chaining object
			return this;
			
		},
		
		alert : function(title, body, callback){
			
			if( sp.modal.opened )
				sp.modal.close();
			
			sp.modal.set({
				buttons :[
					{
						'label' : lang.buttons.ok,
						'class' : 'accept',
						'callback' : typeof callback == 'function' ? callback : 'close'
					}
				],
				'title' : title,
				'body' : body,
			}).show();
			
			// chaining object
			return sp.modal;
		},
		
		confirm : function(title, body, callback){
			
			if( sp.modal.opened )
				sp.modal.close();
			
			if( typeof callback != 'function' )
				return sp.modal;
			
			sp.modal.set({
				buttons :[
					{
						'label' : lang.buttons.ok,
						'class' : 'accept',
						'callback' : callback
					},
					{
						'label' : lang.buttons.cancel,
						'class' : 'clear',
						'callback' : 'close'
					}
				],
				'title' : title,
				'body' : body,
			}).show();
			
			// chaining object
			return sp.modal;
			
		},
		
		show : function(){
			
			if( !sp.modal.configured )
			{
				console.log("modal wasn't configured")	
				return false;
			}
			
			// dialog is hidden
			if( sp.modal.opened === true )
			{
				$(sp.modal.dialog ).dialog('open');
				return sp.modal;
			}
			
			/* settings */
			
			// close button
			if( sp.modal.settings['no-close'] )
				$(sp.modal.dialog ).parents().filter('.ui-dialog').find('.close-button').remove();
			
			// titlebar
			if( sp.modal.settings['title'] === false )
				$(sp.modal.parent_dialog).find('.ui-dialog-titlebar').hide();
			
			// show dialog
			$(sp.modal.dialog ).dialog('open');
			
			sp.modal.opened = true;
			
			sp.modal.center();
			
			// chaining object
			return sp.modal;
		},
		
		close : function(callback){
			
			// animate on close?
			if( sp.modal.settings['animate-close'] )
				$(sp.modal.dialog).dialog('option', 'hide', 'fade');
			
			// destroy dialog after close
			$(sp.modal.dialog).dialog({
				close : function(){
					
					$(sp.modal.dialog).dialog('destroy');
					$(sp.modal.parent_dialog ).remove();
					
					if( typeof callback == 'function' )
						setTimeout(function(){
							callback();
						}, 100);
				}
			});
			
			$(sp.modal.dialog).dialog('close');
			
			// set settings to default
			sp.modal.configured = false;
			sp.modal.opened = false;
			sp.modal.loading = null;
			sp.modal.settings = {};
			sp.modal.dialog_settings = {};
			
			// chaining object
			return sp.modal;
		},
		
		center : function(){
			
			$(sp.modal.dialog).dialog("option", "position", "center");
			
			// chaining object
			return sp.modal;
		},
		
		set_title : function(title){
			
			$(sp.modal.dialog).dialog('option', 'title', title)
			
			// chaining object
			return sp.modal;
		},
		
		set_body : function(body){
			
			$(sp.modal.dialog).html(body);
			
			// chaining object
			return sp.modal;
			
		},
		
		set_class : function(_class){
			
			var classes = new Array('info', 'error', 'success');
			
			if( $.inArray(_class, classes) === -1 )
				return sp.modal;
			
			for( var CLASS in classes )
				$(sp.modal.parent_dialog).removeClass(classes[CLASS]);
			
			$(sp.modal.parent_dialog).addClass(_class);
				
			// chaining object
			return sp.modal;
		},
		
		load : function(callback){
			
			if( !sp.modal.opened )
				sp.modal.show();
			
			if( sp.modal.loading )
				return false;
			
			if( sp.modal.loading === false )
			{
				
				$(sp.modal.parent_dialog).find('.load-overlay').fadeIn(function(){
					
					if( typeof callback == 'function')
						callback();
						
					sp.modal.loading = true;
					
				});
				
				return sp.modal;
			}
			
			// create wrap
			var load_wrap = $('<div />').addClass('load-wrap'),
				overlay = $('<div />').addClass('load-overlay rounded').hide();
			
			// insert wrap
			$(load_wrap).insertBefore($(sp.modal.parent_dialog).find('.ui-dialog-content'));
			
			// move dialog title
			$(sp.modal.parent_dialog).find('.ui-dialog-titlebar').appendTo(load_wrap);
			
			// move dialog content
			$(sp.modal.parent_dialog).find('.ui-dialog-content').appendTo(load_wrap);
			
			// move dialog buttons
			$(sp.modal.parent_dialog).find('.ui-dialog-buttonpane').appendTo(load_wrap);
			
			// insert overlay
			var w_wrap = $(load_wrap)[0].offsetWidth,
				h_wrap = $(load_wrap)[0].offsetHeight;
			
			$(overlay).appendTo(load_wrap).css({
				'width' : w_wrap,
				'height' : h_wrap,
			}).fadeIn(function(){
				
				if( typeof callback == 'function')
					callback();
					
				sp.modal.loading = true;
				
			});
			
			// chaining object
			return sp.modal;			
		},
		
		stop_load : function(callback){
			
			if( !sp.modal.loading )
				return false;
			
			$(sp.modal.parent_dialog).find('.load-overlay').fadeOut(function(){
				
				if( typeof callback == 'function')
					callback();
				
				sp.modal.loading = false;
				
			});
			
			// chaining object
			return sp.modal;
		},
		
		shake : function(callback){
			
			$(sp.modal.parent_dialog).effect( "shake", 'fast', function(){
				
				if( typeof callback == 'function' )
					callback();
				
			});
			
			// chaining object
			return sp.modal;
		},
		
		/* internals */
		_is_modal_function : function(func){
			
			var modal_methods = array_keys(sp.modal),
				modal_functions = new Array();
			
			for( var method in modal_methods )
			{
				
				// private functions
				if( modal_methods[method].substr(0, 1) == '_' )
					continue;
				
				if( typeof sp.modal[modal_methods[method]] == 'function')
					modal_functions.push(modal_methods[method]);
				
				else continue;
				
			}
			
			return ($.inArray(func, modal_functions) !== -1);
			
		}

	}, /* end sp.modal */

	notifications : {

		get_notifications : function(){



		}

	}, /* end sp.notifications */

	/*
	
		sp.alert_bubbles.init({
		'notifications' : {
		    'parent' : '#uicon_notifications',
		    'count' : 1,
		    'animate' : true
		},
		'messages' : {
		    'parent' : '#uicon_messages',
		    'count' : 3,
		    'animate' : true
		}}).show()

	 */

	alert_bubbles : {

		bubbles : {},
		configs : {},
		bubble_template : '#bubble-notifications-tmp',
		
		init : function(items){

			for( var item in items )
			{

				var 
				
				settings = {
					'parent' : [true, null],
					'count' : [false, 0],
					'animate' : [false, false],
					'anim-counter' : [false, false],
					'limit' : [false, 99],
					'position' : [false, 'NE'],
					'distance' : [false, [-6, -9]]
				},
				bubble_coords = {'NE' : ['top', 'right'], 'NO' : ['top', 'left'], 'SE' : ['bottom', 'right'], 'SO' : ['bottom', 'left']},
				bubble_id = item,
				bubble_configs = items[item];

				if( typeof this.bubbles[bubble_id] != 'undefined' )
					continue;

				/* welcome dear bubble :3 */
				this.bubbles[bubble_id] = {};

				// make settings for each bubble
				for( var config in settings )
				{

					var is_necessary = settings[config][0],
						default_value = settings[config][1],
						value = typeof bubble_configs[config] == 'undefined' ? default_value : bubble_configs[config];
					
					if( config == 'parent' )
					{

						if( $(value).length > 1 )
							value = $(value).get(0);

						else if ($(value).length == 0)
							value = null;

						else
							value = $(value);

						// bubble need a good place
						if( !empty(value) )
							$(value).css('position', 'relative');

					}

					if( config == 'position' )
					{

						value = value.toUpperCase().substring(0, 2);

						if( $.inArray(value, bubble_coords) === -1 )
							value = bubble_coords[default_value];
						else
							value = bubble_coords[value];

					}
					
					if( config == 'distance' )
					{
						
						var distances;

						if( typeof value == 'string' )
							distances = explode(' ', value.replace(/\s+/g, ' ').replace(/^\s*|\s*$/g, ''));
						else
							distances = value;

						if( distances.length == 0 )
							distances = explode(' ', default_value);
						else if( distances.length < 2 )
							distances.push(0);

						distances = array_slice(distances, 0, 2);
						distances = $.map(distances, function(n){ return parseInt(n) });

						value = distances;
						
					}
					
					// required setting is empty?
					if( empty(bubble_configs[config]) && is_necessary )
						continue;
					
					// check for defaults settings
					else if ( !bubble_configs[config] && empty(value) )
						bubble_configs[config] = default_value;
					
					// or... maybe custom setting
					else
						bubble_configs[config] = value;
					
				}
				
				var bubble_tmp = $(this.bubble_template).render({
					'count' : bubble_configs['anim-counter'] ? 0 : bubble_configs['count'],
					'bubble_attr' : ' bubble_id="' + bubble_id + '"'
				});
				
				this.bubbles[bubble_id]['options'] = bubble_configs;
				this.bubbles[bubble_id]['obj'] = $(bubble_configs['parent'])
					.append(bubble_tmp)
					.find('[bubble_id]');
				
				this.set_position();
				
				$(bubble_configs['parent']).find('[bubble_id]').hide();
				
			}
			
			return this;
			
		},
		
		set_position : function(bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			$(bubbles).each(function(k, b){

				var bubble = sp.alert_bubbles.bubbles[b],
					make_positions = {};

				$.each([0, 1], function(i){
					make_positions[bubble.options.position[i]] = bubble.options.distance[i]
				});
				
				$(bubble.obj).css(make_positions);

			});

			return this;

		},

		show : function (bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			for ( var b in bubbles )
			{

				if( typeof this.bubbles[bubbles[b]] == 'undefined' )
				{
					console.warn('bubble notification "' + bubbles[b] + '" not defined');
					continue;
				}

				var bubble = this.bubbles[bubbles[b]];

				$(bubble.obj).show();

				this.set_position(bubbles[b]);

				if( bubble.options.animate )
				{
					
					var anim_dir = bubble.options.position[0] == 'top' ? '+=' : '-=',
						anim_dir2 = anim_dir.substr(0, 1) == '+' ? '-=' : '+=',
						anim_options = {'before' : {}, 'after' : {}},
						current_bubble_key = bubbles[b];

					anim_options['before'][bubble.options.position[0]] = anim_dir + '5px';
					anim_options['after'][bubble.options.position[0]] = anim_dir2 + '5px';

					$(bubble.obj).animate(anim_options['before'], 100, function(){

						$(this).animate(anim_options['after'], 100);

					});

				}

				if( bubble.options['anim-counter'] )
					sp.alert_bubbles.animate_counter(current_bubble_key);

			}

			return this;

		},

		hide : function(bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			for ( var b in bubbles )
			{

				if( typeof this.bubbles[bubbles[b]] == 'undefined' )
				{
					console.warn('bubble notification "' + bubbles[b] + '" not defined');
					continue;
				}

				var bubble = this.bubbles[bubbles[b]];

				if( bubble.options.animate )
				{
					
					var anim_dir = bubble.options.position[0] == 'top' ? '+=' : '-=',
						anim_dir2 = anim_dir.substr(0, 1) == '+' ? '-=' : '+=',
						anim_options = {'before' : {}, 'after' : {}};

					anim_options['before'][bubble.options.position[0]] = anim_dir2 + '5px';
					anim_options['after'][bubble.options.position[0]] = anim_dir + '5px';

					$(bubble.obj).animate(anim_options['before'], 100, function(){
						$(this).animate(anim_options['after'], 100, function(){
							$(this).hide();
						});
					});

				}
				else
					$(bubble.obj).hide();

			}

			return this;

		},

		animate_counter : function(bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			for ( var b in bubbles )
			{

				var bubble = this.bubbles[bubbles[b]],
					ul_counter = $('<ul />').css('position', 'relative'),
					current_pos = 0,
					total_numbers = 0,
					duration;
				
				if( bubble.options.count == 0 )
					continue;
				
				for ( var i = 0; i <= bubble.options.count; i++)
				{
					
					var number = i.toString(),
						li;

					if( number.length < (bubble.options.count).toString().length )
						number = (str_repeat('0', (bubble.options.count).toString().length - number.length) + i);

					li = $('<li />').html(number);
					
					$(ul_counter).append(li)

					if( i == bubble.options.limit )
						break;

				}
				
				$(bubble.obj)
				.find('span > strong')
				.empty()
				.css('overflow-y', 'hidden')
				.append(ul_counter);

				total_numbers = $(bubble.obj).find('span strong ul li').length;
				
				$(bubble.obj).find('span strong ul li').each(function(i, obj){
			    	
			    	if( i == ($(bubble.obj).find('span strong ul li').length)-1 )
						return false;

			    	current_pos -= ($(this)[0].offsetHeight);
			    	duration = total_numbers-i <= 5 ? 300 : 10;
			    	
			    	// animate counter
				    $(bubble.obj).find('span ul').animate({
				        'top' : current_pos
				    }, duration, function(){
				    	
				    	// define last element
						var last_number = $(bubble.obj).find('span ul li').last();
						
						// set last element effects
						if ( i == (total_numbers-2) )
						{
							
							// remove all numbers except the last.
							$(bubble.obj).find('span strong').empty().html($('<i />').html($(last_number).html()));
							
							if( bubble.options.count > bubble.options.limit )
							{
								$(bubble.obj).find('span strong')
								.append('<i class="plus" />')
								.find('i.plus')
								.html('+')
								.hide()
								.fadeIn('fast');
							}

						}

				    });
				
				});

			}

			return this;

		},

		__get_bubbles_ids : function(bubbles, from){
			
			if( empty(bubbles) && !empty(this.bubbles) )
				return array_keys(this.bubbles);

			else if ( empty(bubbles) )
				return false;
			
			bubbles = explode(',', bubbles.replace(/\,[\s*]/g, ',').replace(/^\s*|\s*$/g, ''));

			return bubbles;

		}
	
	}, /* end sp.alert_bubbles */
	
	/* core internals */

	/* end core internals */
};

(function(sp){

sp.dialog = function( options ) {
	
	options = options || {};
	
	if( !options.target || $(options.target).length == 0 )
	{
		console.warn('dialog:', 'target not defined.');
		return false;
	}
	
	var dialog = {
		
		settings : {},
		dialog : {},
		target : {},
		positions: {},
		
		show : function(callback){
			
			$('body > .ui-dialog-context').hide();
			
			if( this.settings['animate'] ){
				
				$(this._dialog).fadeIn(500, function(){
					if( typeof callback == 'function' )
						callback.apply(sp.dialog);
				});

				this.set_position();
				
			}
			else
			{
				$(this._dialog).show();
				
				this.set_position();
				
				if( typeof callback == 'function' )
						callback.apply(sp.dialog);
			}
			
			$(this._dialog).show();
			
			return this;
		},
		
		close : function(){
			
			$(this._dialog).hide();
			
			return this;
		},
		
		bind_dialog : function(){
			
			var dialog = this,
				_dialog = this._dialog,
				target = this.target,
				custom_events = (this.settings['bind'][0] == 'dialog::show');
			
			if( this.settings['bind'] == 'none' )
				return false;
			
			$(custom_events ? this.target.add(this._dialog) : target).bind(this.settings['bind'][0], function(){
				
				dialog.show.apply(dialog);

				if( this.nodeName == 'A' )
					return false;
				
			});
			
			$(custom_events ? this.target.add(this._dialog) : _dialog).bind(this.settings['bind'][1], function(_event){
				
				if( dialog.settings['bind'][0] != 'dialog::show' )
				{
					if( $(_event.target)[0] === $(dialog.target)[0] )
						return;
					
					if( $(this).css('display') == 'none' )
						return;
					
					if( $(_event.target).parents().filter(dialog.target)[0] === $(dialog.target)[0] )
						return;
				}
				
				dialog.close.apply(dialog);
				
			});
			
		},
		
		set_position : function(){
			
			var make_positions = {'my' : [], 'at' : []},
				positions = this.positions,
				target = this.target,
				dialog_borders = (
					parseInt($(this._dialog).find('.sp-dialog .dialog').css('border-left-width'))
					+
					parseInt($(this._dialog).find('.sp-dialog .dialog').css('border-right-width'))
				),
				center_of_target = (
					($(target)[0].offsetWidth/2) // target width
					-20 // arrow dialog width
					-dialog_borders // dialog borders width
					+this.settings['padding-x'] // custom padding-x
				),
				padding_y = this.settings['padding-y'].toString(),
				pos_x = (padding_y.substr(1) == '-' ? '-' + padding_y : '+' + padding_y);
			
			center_of_target = center_of_target.toString();
			center_of_target = center_of_target.substring(0, 1) == '-' ? '+' + center_of_target.substr(1) : '-' + center_of_target;
			
			for( var pos in positions )
			{
				if( pos == 'my' ){
					
					//positions[pos][1] = (positions[pos][1] + pos_x);
					if( positions['my'][1] == 'bottom' || positions['my'][1] == 'top' )
						$(this._dialog).css('padding-top', this.settings['padding-y']);

					if( positions[pos][0] == 'center' ){
						positions[pos][0] = 'right' + center_of_target;

					}

				}
				
				make_positions[pos] = positions[pos];
			}

			$(this._dialog).position({
				my : implode(' ', make_positions['my']),
				at : implode(' ', make_positions['at']),
				of : $(target)
			})


		}
	};
	
		var defaults = {
			'bind' : 'click',
			'animate' : false,
			'padding-y' : 10,
			'padding-x' : 0,
			'position' : 'right top right bottom',
			'dropdown' : false,
			'obj' : false
		},
		_settings = {
			'body' : options['obj'] ? false : true,
			'target' : true,
			'bind' : false,
			'animate' : false,
			'padding-x' : false,
			'padding-y' : false,
			'position' : false,
			'dropdown' : false,
			'obj' : false,
			'width' : false,
			'height' : false
		},
		bind_events = {
			'click' : ['click', 'clickoutside'],
			'hover' : ['mouseover', 'mouseoveroutside'],
			'custom': ['dialog::show', 'dialog::close']
		};
	
	// prepare settings
	for ( var s in _settings )
	{
		
		var setting = s;
		
		if( !empty(options[setting]) )
			dialog.settings[setting] = options[setting];
			
		else if( $.inArray(setting, array_keys(defaults)) !== -1 )
			dialog.settings[setting] = defaults[setting];
			
		else
			dialog.settings[setting] = null;
		
	}
	
	// check settings
	for( setting in dialog.settings)
	{
		
		var value = dialog.settings[setting];
		
		if( value === null && _settings[setting] === true ){
			console.warn('dialog:', setting + ' not defined.');
			return false;
		}
		
		if( setting == 'target' )
			dialog.target = $(value);
		
		// set body dialog
		if( setting == 'body' )
		{
			
			if( dialog.settings['obj'] )
				value = $(dialog.settings.obj).html();
			
			// template to html object
			var wrapper = $('<div />')
			.attr('id', 'dialog-wrapper')
			.append($('#sp-dialog-template').render({'body' : value, 'dropdown' : dialog.settings['dropdown']})).prependTo('body');
			
			dialog._dialog = $(wrapper).find('.ui-dialog-context').css({
				'position' : 'absolute',
				'left' : -99999,
				'top' : -99999
			}).clone();
			
			$('body').prepend(dialog._dialog);
			$(wrapper).remove();
				
		}
		
		if( setting == 'position' )
		{
			var coords = explode(' ', value),
				default_coords = explode(' ', defaults['position']);
			
			for( var coord in default_coords)
			{
				var coord_option = (coord < 2 ? 'my' : 'at');
				
				if(typeof dialog.positions[coord_option] == 'undefined')
					dialog.positions[coord_option] = new Array();
				
				if( coords[coord] )
					dialog.positions[coord_option].push(coords[coord]);
				else
					dialog.positions[coord_option].push(default_coords[coord]);
			}
		}
		
		if( setting == 'bind' )
		{
			
			if( $.inArray(value, array_keys(bind_events))  !== -1 )
				dialog.settings['bind'] = bind_events[dialog.settings['bind']];
			else if( value === false )
				dialog.settings['bind'] = 'none';
			
		}
		
	}
	
	// set width and height
	if( dialog.settings.width )
		$(dialog._dialog).find('.dialog-content').css('width', dialog.settings.width);
		
	if( dialog.settings.height )
		$(dialog._dialog).find('.dialog-content').css('height', dialog.settings.height);
	
	// position near to target
	dialog.set_position.apply(dialog);
	
	// hide dialog. awaiting event
	$(dialog._dialog).hide();
	
	// bind event to show dialog
	if( dialog.settings['bind'] !== false )
		dialog.bind_dialog.apply(dialog);
	
	return dialog;
	
}
})( sp );



/*********************
 * PLUGINS
 *********************/

/* Jquery Tipsy */
;(function(e){function t(e,t){return typeof e=="function"?e.call(t):e}function n(e){while(e=e.parentNode){if(e==document)return true}return false}function r(t,n){this.$element=e(t);this.options=n;this.enabled=true;this.fixTitle()}r.prototype={show:function(){var n=this.getTitle();if(n&&this.enabled){var r=this.tip();r.find(".tipsy-inner")[this.options.html?"html":"text"](n);r[0].className="tipsy";r.remove().css({top:0,left:0,visibility:"hidden",display:"block"}).prependTo(document.body);var i=e.extend({},this.$element.offset(),{width:this.$element[0].offsetWidth,height:this.$element[0].offsetHeight});var s=r[0].offsetWidth,o=r[0].offsetHeight,u=t(this.options.gravity,this.$element[0]);var a;switch(u.charAt(0)){case"n":a={top:i.top+i.height+this.options.offset,left:i.left+i.width/2-s/2};break;case"s":a={top:i.top-o-this.options.offset,left:i.left+i.width/2-s/2};break;case"e":a={top:i.top+i.height/2-o/2,left:i.left-s-this.options.offset};break;case"w":a={top:i.top+i.height/2-o/2,left:i.left+i.width+this.options.offset};break}if(u.length==2){if(u.charAt(1)=="w"){a.left=i.left+i.width/2-15}else{a.left=i.left+i.width/2-s+15}}r.css(a).addClass("tipsy-"+u);r.find(".tipsy-arrow")[0].className="tipsy-arrow tipsy-arrow-"+u.charAt(0);if(this.options.className){r.addClass(t(this.options.className,this.$element[0]))}if(this.options.fade){r.stop().css({opacity:0,display:"block",visibility:"visible"}).animate({opacity:this.options.opacity})}else{r.css({visibility:"visible",opacity:this.options.opacity})}}},hide:function(){if(this.options.fade){this.tip().stop().fadeOut(function(){e(this).remove()})}else{this.tip().remove()}},fixTitle:function(){var e=this.$element;if(e.attr("title")||typeof e.attr("original-title")!="string"){e.attr("original-title",e.attr("title")||"").removeAttr("title")}},getTitle:function(){var e,t=this.$element,n=this.options;this.fixTitle();var e,n=this.options;if(typeof n.title=="string"){e=t.attr(n.title=="title"?"original-title":n.title)}else if(typeof n.title=="function"){e=n.title.call(t[0])}e=(""+e).replace(/(^\s*|\s*$)/,"");return e||n.fallback},tip:function(){if(!this.$tip){this.$tip=e('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>');this.$tip.data("tipsy-pointee",this.$element[0])}return this.$tip},validate:function(){if(!this.$element[0].parentNode){this.hide();this.$element=null;this.options=null}},enable:function(){this.enabled=true},disable:function(){this.enabled=false},toggleEnabled:function(){this.enabled=!this.enabled}};e.fn.tipsy=function(t){function i(n){var i=e.data(n,"tipsy");if(!i){i=new r(n,e.fn.tipsy.elementOptions(n,t));e.data(n,"tipsy",i)}return i}function s(){var e=i(this);e.hoverState="in";if(t.delayIn==0){e.show()}else{e.fixTitle();setTimeout(function(){if(e.hoverState=="in")e.show()},t.delayIn)}}function o(){var e=i(this);e.hoverState="out";if(t.delayOut==0){e.hide()}else{setTimeout(function(){if(e.hoverState=="out")e.hide()},t.delayOut)}}if(t===true){return this.data("tipsy")}else if(typeof t=="string"){var n=this.data("tipsy");if(n)n[t]();return this}t=e.extend({},e.fn.tipsy.defaults,t);if(!t.live)this.each(function(){i(this)});if(t.trigger!="manual"){var u=t.live?"live":"bind",a=t.trigger=="hover"?"mouseenter":"focus",f=t.trigger=="hover"?"mouseleave":"blur";this[u](a,s)[u](f,o)}return this};e.fn.tipsy.defaults={className:null,delayIn:0,delayOut:0,fade:false,fallback:"",gravity:"n",html:false,live:false,offset:0,opacity:.8,title:"title",trigger:"hover"};e.fn.tipsy.revalidate=function(){e(".tipsy").each(function(){var t=e.data(this,"tipsy-pointee");if(!t||!n(t)){e(this).remove()}})};e.fn.tipsy.elementOptions=function(t,n){return e.metadata?e.extend({},n,e(t).metadata()):n};e.fn.tipsy.autoNS=function(){return e(this).offset().top>e(document).scrollTop()+e(window).height()/2?"s":"n"};e.fn.tipsy.autoWE=function(){return e(this).offset().left>e(document).scrollLeft()+e(window).width()/2?"e":"w"};e.fn.tipsy.autoBounds=function(t,n){return function(){var r={ns:n[0],ew:n.length>1?n[1]:false},i=e(document).scrollTop()+t,s=e(document).scrollLeft()+t,o=e(this);if(o.offset().top<i)r.ns="n";if(o.offset().left<s)r.ew="w";if(e(window).width()+e(document).scrollLeft()-o.offset().left<t)r.ew="e";if(e(window).height()+e(document).scrollTop()-o.offset().top<t)r.ns="s";return r.ns+(r.ew?r.ew:"")}}})(jQuery);

/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,c,b){$.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "),function(d){a(d)});a("focusin","focus"+b);a("focusout","blur"+b);$.addOutsideEvent=a;function a(g,e){e=e||g+b;var d=$(),h=g+"."+e+"-special-event";$.event.special[e]={setup:function(){d=d.add(this);if(d.length===1){$(c).bind(h,f)}},teardown:function(){d=d.not(this);if(d.length===0){$(c).unbind(h)}},add:function(i){var j=i.handler;i.handler=function(l,k){l.target=k;j.apply(this,arguments)}}};function f(i){$(d).each(function(){var j=$(this);if(this!==i.target&&!j.has(i.target).length){j.triggerHandler(e,[i.target])}})}}})(jQuery,document,"outside");

/*! JsRender v1.0pre: http://github.com/BorisMoore/jsrender */
/*
* Optimized version of jQuery Templates, for rendering to string.
* Does not require jQuery, or HTML DOM
* Integrates with JsViews (http://github.com/BorisMoore/jsviews)
* Copyright 2013, Boris Moore
* Released under the MIT License.
*/
(function(e,t,n){"use strict";function _(e,t){if(t&&t.onError){if(t.onError(e)===false){return}}this.name="JsRender Error";this.message=e||"JsRender error"}function D(e,t){var n;e=e||{};for(n in t){e[n]=t[n]}return e}function P(e,t,n){if(!it.rTag||arguments.length){a=e?e.charAt(0):a;f=e?e.charAt(1):f;l=t?t.charAt(0):l;c=t?t.charAt(1):c;h=n||h;e="\\"+a+"(\\"+h+")?\\"+f;t="\\"+l+"\\"+c;o="(?:(?:(\\w+(?=[\\/\\s\\"+l+"]))|(?:(\\w+)?(:)|(>)|!--((?:[^-]|-(?!-))*)--|(\\*)))"+"\\s*((?:[^\\"+l+"]|\\"+l+"(?!\\"+c+"))*?)";it.rTag=o+")";o=new RegExp(e+o+"(\\/)?|(?:\\/(\\w+)))"+t,"g");u=new RegExp("<.*>|([^\\\\]|^)[{}]|"+e+".*"+t)}return[a,f,l,c,h]}function H(e,t){if(!t){t=e;e=n}var r,i,s,o,u=this,a=!t||t==="root";if(e){o=u.type===t?u:n;if(!o){r=u.views;if(u._.useKey){for(i in r){if(o=r[i].get(e,t)){break}}}else for(i=0,s=r.length;!o&&i<s;i++){o=r[i].get(e,t)}}}else if(a){while(u.parent.parent){o=u=u.parent}}else while(u&&!o){o=u.type===t?u:n;u=u.parent}return o}function B(){var e=this.get("item");return e?e.index:n}function j(e){var t,r=this,i=(r.ctx||{})[e];i=i===n?r.getRsc("helpers",e):i;if(i){if(typeof i==="function"){t=function(){return i.apply(r,arguments)};D(t,i)}}return t||i}function F(e,t,r){var i,s,o,u=+r===r&&r,a=t.linkCtx;if(u){r=(u=t.tmpl.bnds[u-1])(t.data,t,M)}o=r.args[0];if(e||u){s=a&&a.tag||{_:{inline:!a},tagName:e+":",flow:true,_is:"tag"};s._.bnd=u;if(a){a.tag=s;s.linkCtx=a;r.ctx=Z(r.ctx,a.view.ctx)}s.tagCtx=r;r.view=t;s.ctx=r.ctx||{};delete r.ctx;t._.tag=s;e=e!=="true"&&e;if(e&&((i=t.getRsc("converters",e))||J("Unknown converter: {{"+e+":"))){s.depends=i.depends;o=i.apply(s,r.args)}o=u&&t._.onRender?t._.onRender(o,t,u):o;t._.tag=n}return o}function I(e,t){var r,i=this,s=M[e];r=s&&s[t];while(r===n&&i){s=i.tmpl[e];r=s&&s[t];i=i.parent}return r}function q(e,t,r,i){var s,o,u,a,f,l,c,h,p,d,v,m,g,y,b="",w=+i===i&&i,E=t.linkCtx||0,S=t.ctx,x=r||t.tmpl,T=t._;if(e._is==="tag"){o=e;e=o.tagName}if(w){i=(g=x.bnds[w-1])(t.data,t,M)}h=i.length;o=o||E.tag;for(c=0;c<h;c++){d=i[c];m=d.tmpl;m=d.content=m&&x.tmpls[m-1];r=d.props.tmpl;if(!c&&(!r||!o)){y=t.getRsc("tags",e)||J("Unknown tag: {{"+e+"}}")}r=r||!c&&y.template||m;r=""+r===r?t.getRsc("templates",r)||et(r):r;D(d,{tmpl:r,render:$,index:c,view:t,ctx:Z(d.ctx,S)});if(!o){if(y.init){o=new y.init(d,E,S);o.attr=o.attr||y.attr||n}else{o={render:y.render}}o._={inline:!E};if(E){E.attr=o.attr=E.attr||o.attr;E.tag=o;o.linkCtx=E}if(o._.bnd=g||E){o._.arrVws={}}o.tagName=e;o.parent=l=S&&S.tag,o._is="tag"}T.tag=o;d.tag=o;o.tagCtxs=i;o.rendering={};if(!o.flow){v=d.ctx=d.ctx||{};u=v.parentTags=S&&Z(v.parentTags,S.parentTags)||{};if(l){u[l.tagName]=l}v.tag=o}}for(c=0;c<h;c++){d=o.tagCtx=i[c];o.ctx=d.ctx;if(s=o.render){p=s.apply(o,d.args)}b+=p!==n?p:d.tmpl?d.render():""}delete o.rendering;o.tagCtx=o.tagCtxs[0];o.ctx=o.tagCtx.ctx;if(o._.inline&&(a=o.attr)&&a!=="html"){b=a==="text"?tt.html(b):""}return b=w&&t._.onRender?t._.onRender(b,t,w):b}function R(e,t,r,i,s,o,u,a){var f,l,c,h=t==="array",p={key:0,useKey:h?0:1,id:""+N++,onRender:a,bnds:{}},d={data:i,tmpl:s,content:u,views:h?[]:{},parent:r,ctx:e,type:t,get:H,getIndex:B,getRsc:I,hlp:j,_:p,_is:"view"};if(r){f=r.views;l=r._;if(l.useKey){f[p.key="_"+l.useKey++]=d;c=l.tag;p.bnd=h&&(!c||!!c._.bnd&&c)}else{f.splice(p.key=d.index=o!==n?o:f.length,0,d)}d.ctx=e||r.ctx}return d}function U(e){var t,n,r,i,s;for(t in O){i=O[t];if((s=i.compile)&&(n=e[t+"s"])){for(r in n){n[r]=s(r,n[r],e,t,i)}}}}function z(e,t,n){var r,i;if(typeof t==="function"){t={depends:t.depends,render:t}}else{if(i=t.template){t.template=""+i===i?et[i]||et(i):i}if(t.init!==false){r=t.init=t.init||function(e){};r.prototype=t;(r.prototype=t).constructor=r}}if(n){t._parentTmpl=n}return t}function W(r,i,s,o,a,f){function l(n){if(""+n===n||n.nodeType>0){try{h=n.nodeType>0?n:!u.test(n)&&t&&t(e.document).find(n)[0]}catch(i){}if(h){n=h.getAttribute(k);r=r||n;n=et[n];if(!n){r=r||"_"+T++;h.setAttribute(k,r);n=et[r]=W(r,h.innerHTML,s,o,a,f)}}return n}}var c,h;i=i||"";c=l(i);f=f||(i.markup?i:{});f.tmplName=r;if(s){f._parentTmpl=s}if(!c&&i.markup&&(c=l(i.markup))){if(c.fn&&(c.debug!==i.debug||c.allowCode!==i.allowCode)){c=c.markup}}if(c!==n){if(r&&!s){A[r]=function(){return i.render.apply(i,arguments)}}if(c.fn||i.fn){if(c.fn){if(r&&r!==c.tmplName){i=Z(f,c)}else{i=c}}}else{i=X(c,f);Q(c,i)}U(f);return i}}function X(e,t){var n,r=st.wrapMap||{},s=D({markup:e,tmpls:[],links:{},tags:{},bnds:[],_is:"template",render:$},t);if(!t.htmlTag){n=w.exec(e);s.htmlTag=n?n[1].toLowerCase():""}n=r[s.htmlTag];if(n&&n!==r.div){s.markup=i.trim(s.markup);s._elCnt=true}return s}function V(e,t){function r(s,o,u){var a,f,l,c;if(s&&""+s!==s&&!s.nodeType&&!s.markup){for(l in s){r(l,s[l],o)}return M}if(o===n){o=s;s=n}if(s&&""+s!==s){u=o;o=s;s=n}c=u?u[i]=u[i]||{}:r;f=t.compile;if(a=it.onBeforeStoreItem){f=a(c,s,o,f)||f}if(!s){o=f(n,o)}else if(o===null){delete c[s]}else{c[s]=f?o=f(s,o,u,e,t):o}if(o){o._is=e}if(a=it.onStoreItem){a(c,s,o,f)}return o}var i=e+"s";M[i]=r;O[e]=t}function $(e,t,r,s,o,u){var a,f,l,c,h,p,d,v,m,g,y,b,w,E=this,S=!E.attr||E.attr==="html",x="";if(s===true){d=true;s=0}if(E.tag){v=E;E=E.tag;g=E._;b=E.tagName;w=v.tmpl;t=Z(t,E.ctx);m=v.content;if(v.props.link===false){t=t||{};t.link=false}r=r||v.view;e=e===n?r:e}else{w=E.jquery&&(E[0]||J('Unknown template: "'+E.selector+'"'))||E}if(w){if(!r&&e&&e._is==="view"){r=e}if(r){m=m||r.content;u=u||r._.onRender;if(e===r){e=r.data;o=true}t=Z(t,r.ctx)}if(!r||r.data===n){(t=t||{}).root=e}if(!w.fn){w=et[w]||et(w)}if(w){u=(t&&t.link)!==false&&S&&u;y=u;if(u===true){y=n;u=r._.onRender}if(i.isArray(e)&&!o){c=d?r:s!==n&&r||R(t,"array",r,e,w,s,m,u);for(a=0,f=e.length;a<f;a++){l=e[a];h=R(t,"item",c,l,w,(s||0)+a,m,u);p=w.fn(l,h,M);x+=c._.onRender?c._.onRender(p,h):p}}else{c=d?r:R(t,b||"data",r,e,w,s,m,u);if(g&&!E.flow){c.tag=E}x+=w.fn(e,c,M)}return y?y(x,c):x}}return""}function J(e){if(st.debugMode){throw new M.sub.Error(e)}}function K(e){J("Syntax error\n"+e)}function Q(e,t,n,r){function i(t){t-=c;if(t){p.push(e.substr(c,t).replace(v,"\\n"))}}function s(t){t&&K('Unmatched or missing tag: "{{/'+t+'}}" in template:\n'+e)}function u(t,o,u,l,g,w,E,S,x,T,N,C){if(w){g=":";l="html"}T=T||n;var k,L,A=o&&[],O="",M="",_="",D=!T&&!g&&!E;u=u||g;i(C);c=C+t.length;if(S){if(f){p.push(["*","\n"+x.replace(m,"$1")+"\n"])}}else if(u){if(u==="else"){if(b.test(x)){K('for "{{else if expr}}" use "{{else expr}}"')}A=d[6];d[7]=e.substring(d[7],C);d=h.pop();p=d[3];D=true}if(x){x=x.replace(v," ");O=Y(x,A).replace(y,function(e,t,n){if(t){_+=n+","}else{M+=n+","}return""})}M=M.slice(0,-1);O=O.slice(0,-1);k=M&&M.indexOf("noerror:true")+1&&M||"";a=[u,l||!!r||"",O,D&&[],'params:"'+x+'",props:{'+M+"}"+(_?",ctx:{"+_.slice(0,-1)+"}":""),k,A||0];p.push(a);if(D){h.push(d);d=a;d[7]=c}}else if(N){L=d[0];s(N!==L&&L!=="else"&&N);d[7]=e.substring(d[7],C);d=h.pop()}s(!d&&N);p=d[3]}var a,f=t&&t.allowCode,l=[],c=0,h=[],p=l,d=[,,,l];e=e.replace(g,"\\$1");s(h[0]&&h[0][3].pop()[0]);e.replace(o,u);i(e.length);if(c=l[l.length-1]){s(""+c!==c&&+c[7]===c[7]&&c[0])}return G(l,t||e,n)}function G(e,n,r){var i,s,o,u,a,f,l,c,h,p,d,m,g,y,b,w,E,S,x,T,N,C,k,L,A,O,M,_,D=0,P="",H="",B={},j=e.length;if(""+n===n){w=r?'data-link="'+n.replace(v," ").slice(1,-1)+'"':n;n=0}else{w=n.tmplName||"unnamed";if(x=n.allowCode){B.allowCode=true}if(n.debug){B.debug=true}d=n.bnds;b=n.tmpls}for(i=0;i<j;i++){s=e[i];if(""+s===s){P+='\nret+="'+s+'";'}else{o=s[0];if(o==="*"){P+=""+s[1]}else{u=s[1];a=s[2];T=s[3];f=s[4];H=s[5];N=s[7];if(!(A=o==="else")){D=0;if(d&&(m=s[6])){D=d.push(m)}}if(O=o===":"){if(u){o=u==="html"?">":u+o}if(H){M="prm"+i;H="try{var "+M+"=["+a+"][0];}catch(e){"+M+'="";}\n';a=M}}else{if(T){E=X(N,B);E.tmplName=w+"/"+o;G(T,E);b.push(E)}if(!A){S=o;L=P;P="";g=i}k=e[i+1];k=k&&k[0]==="else"}f+=",args:["+a+"]}";if(O&&m||u&&o!==">"){_=new Function("data,view,j,u"," // "+w+" "+D+" "+o+"\n"+H+"return {"+f+";");_.paths=m;_._ctxs=o;if(r){return _}p=true}P+=O?"\n"+(m?"":H)+(r?"return ":"ret+=")+(p?(p=true,'c("'+u+'",view,'+(m?(d[D-1]=_,D):"{"+f)+");"):o===">"?(c=true,"h("+a+");"):(h=true,"(v="+a+")!="+(r?"=":"")+'u?v:"";')):(l=true,"{tmpl:"+(T?b.length:"0")+","+f+",");if(S&&!k){P="["+P.slice(0,-1)+"]";if(r||m){P=new Function("data,view,j,u"," // "+w+" "+D+" "+S+"\nreturn "+P+";");if(m){(d[D-1]=P).paths=m}P._ctxs=o;if(r){return P}}P=L+'\nret+=t("'+S+'",view,this,'+(D||P)+");";m=0;S=0}}}}P="// "+w+"\nvar j=j||"+(t?"jQuery.":"js")+"views"+(h?",v":"")+(l?",t=j._tag":"")+(p?",c=j._cnvt":"")+(c?",h=j.converters.html":"")+(r?";\n":',ret="";\n')+(st.tryCatch?"try{\n":"")+(B.debug?"debugger;":"")+P+(r?"\n":"\nreturn ret;\n")+(st.tryCatch?"\n}catch(e){return j._err(e);}":"");try{P=new Function("data,view,j,u",P)}catch(F){K("Compiled template code:\n\n"+P,F)}if(n){n.fn=P}return P}function Y(e,t){function n(n,l,c,h,d,v,m,g,y,b,w,E,S,x,T,N,C,k,L){function A(e,n,r,s,o,u,a){if(n){t&&!i&&t.push(h);if(n!=="."){var f=(r?'view.hlp("'+r+'")':s?"view":"data")+(a?(o?"."+o:r?"":s?"":"."+n)+(u||""):(a=r?"":s?o||"":n,""));f=f+(a?"."+a:"");return f.slice(0,9)==="view.data"?f.slice(5):f}}return e}d=d||"";c=c||l||w;h=h||g;if(t&&T){t.push({_jsvOb:L.slice(o[u-1]+1,k+1)})}y=y||N||"";if(v){K(e)}else{var O=f?(f=!E,f?n:'"'):a?(a=!S,a?n:'"'):(c?(u++,o[u]=k,c):"")+(C?u?"":r?(r=i=false,"\b"):",":m?(u&&K(e),r=h,i=h.charAt(0)==="~","\b"+h+":"):h?h.split("^").join(".").replace(p,A)+(y?(s[++u]=true,y):d):d?d:x?(s[u--]=false,x)+(y?(s[++u]=true,y):""):b?(s[u]||K(e),","):l?"":(f=E,a=S,'"'));return O}}var r,i,s={},o={0:-1},u=0,a=false,f=false;return(e+" ").replace(d,n)}function Z(e,t){return e&&e!==t?t?D(D({},t),e):e:t&&D({},t)}function ot(e){return C[e]}if(t&&t.views||e.jsviews){return}var r="v1.0pre",i,s,o,u,a="{",f="{",l="}",c="}",h="^",p=/^(?:null|true|false|\d[\d.]*|([\w$]+|\.|~([\w$]+)|#(view|([\w$]+))?)([\w$.^]*?)(?:[.[^]([\w$]+)\]?)?)$/g,d=/(\()(?=\s*\()|(?:([([])\s*)?(?:([#~]?[\w$.^]+)?\s*((\+\+|--)|\+|-|&&|\|\||===|!==|==|!=|<=|>=|[<>%*!:?\/]|(=))\s*|([#~]?[\w$.^]+)([([])?)|(,\s*)|(\(?)\\?(?:(')|("))|(?:\s*((\))(?=\s*\.|\s*\^)|\)|\])([([]?))|(\s+)/g,v=/\s*\n/g,m=/\\(['"])/g,g=/([\\'"])/g,y=/\x08(~)?([^\x08]+)\x08/g,b=/^if\s/,w=/<(\w+)[>\s]/,E=/<(\w+)[^>\/]*>[^>]*$/,S=/[><"'&]/g,x=/[><"'&]/g,T=0,N=0,C={"&":"&","<":"<",">":">","\0":"&#0;","'":"&#39;",'"':"&#34;"},k="data-jsv-tmpl",L=[].slice,A={},O={template:{compile:W},tag:{compile:z},helper:{},converter:{}},M={jsviews:r,render:A,settings:{delimiters:P,debugMode:true,tryCatch:true},sub:{View:R,Error:_,tmplFn:Q,parse:Y,extend:D,error:J,syntaxError:K},_cnvt:F,_tag:q,_err:function(e){return st.debugMode?"Error: "+(e.message||e)+". ":""}};(_.prototype=new Error).constructor=_;B.depends=function(){return[this.get("item"),"index"]};for(s in O){V(s,O[s])}var et=M.templates,tt=M.converters,nt=M.helpers,rt=M.tags,it=M.sub,st=M.settings;if(t){i=t;i.fn.render=$}else{i=e.jsviews={};i.isArray=Array&&Array.isArray||function(e){return Object.prototype.toString.call(e)==="[object Array]"}}i.render=A;i.views=M;i.templates=et=M.templates;rt({"else":function(){},"if":{render:function(e){var t=this,n=t.rendering.done||!e&&(arguments.length||!t.tagCtx.index)?"":(t.rendering.done=true,t.selected=t.tagCtx.index,t.tagCtx.render());return n},onUpdate:function(e,t,n){var r,i,s;for(r=0;(i=this.tagCtxs[r])&&i.args.length;r++){i=i.args[0];s=!i!==!n[r].args[0];if(!!i||s){return s}}return false},flow:true},"for":{render:function(e){var t,r,s=this,o=s.tagCtx,u=!arguments.length,a="",f=u||0;if(!s.rendering.done){if(u){a=n}else if(e!==n){a+=o.render(e);f+=i.isArray(e)?e.length:1}if(s.rendering.done=f){s.selected=o.index}}return a},onUpdate:function(e,t,n){},onArrayChange:function(e,t){var n,r=this,i=t.change;if(this.tagCtxs[1]&&(i==="insert"&&e.target.length===t.items.length||i==="remove"&&!e.target.length||i==="refresh"&&!t.oldItems.length!==!e.target.length)){this.refresh()}else{for(n in r._.arrVws){n=r._.arrVws[n];if(n.data===e.target){n._.onArrayChange.apply(n,arguments)}}}e.done=true},flow:true},include:{flow:true},"*":{render:function(e){return e},flow:true}});tt({html:function(e){return e!=n?String(e).replace(x,ot):""},attr:function(e){return e!=n?String(e).replace(S,ot):e===null?null:""},url:function(e){return e!=n?encodeURI(String(e)):e===null?null:""}});P()})(this,this.jQuery);



/*********************
 * LIBRARIES 
 *********************/

/*!
  Tinycon - A small library for manipulating the Favicon
  Tom Moor, http://tommoor.com
  Copyright (c) 2012 Tom Moor
  MIT Licensed
  @version 0.5
*/
(function(){var Tinycon={};var currentFavicon=null;var originalFavicon=null;var originalTitle=document.title;var faviconImage=null;var canvas=null;var options={};var defaults={width:7,height:9,font:'10px arial',colour:'#ffffff',background:'#F03D25',fallback:true,abbreviate:true};var ua=(function(){var agent=navigator.userAgent.toLowerCase();return function(browser){return agent.indexOf(browser)!==-1}}());var browser={ie:ua('msie'),chrome:ua('chrome'),webkit:ua('chrome')||ua('safari'),safari:ua('safari')&&!ua('chrome'),mozilla:ua('mozilla')&&!ua('chrome')&&!ua('safari')};var getFaviconTag=function(){var links=document.getElementsByTagName('link');for(var i=0,len=links.length;i<len;i++){if((links[i].getAttribute('rel')||'').match(/\bicon\b/)){return links[i]}}return false};var removeFaviconTag=function(){var links=document.getElementsByTagName('link');var head=document.getElementsByTagName('head')[0];for(var i=0,len=links.length;i<len;i++){var exists=(typeof(links[i])!=='undefined');if(exists&&(links[i].getAttribute('rel')||'').match(/\bicon\b/)){head.removeChild(links[i])}}};var getCurrentFavicon=function(){if(!originalFavicon||!currentFavicon){var tag=getFaviconTag();originalFavicon=currentFavicon=tag?tag.getAttribute('href'):'/favicon.ico'}return currentFavicon};var getCanvas=function(){if(!canvas){canvas=document.createElement("canvas");canvas.width=16;canvas.height=16}return canvas};var setFaviconTag=function(url){removeFaviconTag();var link=document.createElement('link');link.type='image/x-icon';link.rel='icon';link.href=url;document.getElementsByTagName('head')[0].appendChild(link)};var log=function(message){if(window.console)window.console.log(message)};var drawFavicon=function(label,colour){if(!getCanvas().getContext||browser.ie||browser.safari||options.fallback==='force'){return updateTitle(label)}var context=getCanvas().getContext("2d");var colour=colour||'#000000';var src=getCurrentFavicon();faviconImage=new Image();faviconImage.onload=function(){context.clearRect(0,0,16,16);context.drawImage(faviconImage,0,0,faviconImage.width,faviconImage.height,0,0,16,16);if((label+'').length>0)drawBubble(context,label,colour);refreshFavicon()};if(!src.match(/^data/)){faviconImage.crossOrigin='anonymous'}faviconImage.src=src};var updateTitle=function(label){if(options.fallback){if((label+'').length>0){document.title='('+label+') '+originalTitle}else{document.title=originalTitle}}};var drawBubble=function(context,label,colour){if(typeof label=='number'&&label>99&&options.abbreviate){label=abbreviateNumber(label)}var len=(label+'').length-1;var width=options.width+(6*len);var w=16-width;var h=16-options.height;context.font=(browser.webkit?'bold ':'')+options.font;context.fillStyle=options.background;context.strokeStyle=options.background;context.lineWidth=1;context.fillRect(w,h,width-1,options.height);context.beginPath();context.moveTo(w-0.5,h+1);context.lineTo(w-0.5,15);context.stroke();context.beginPath();context.moveTo(15.5,h+1);context.lineTo(15.5,15);context.stroke();context.beginPath();context.strokeStyle="rgba(0,0,0,0.3)";context.moveTo(w,16);context.lineTo(15,16);context.stroke();context.fillStyle=options.colour;context.textAlign="right";context.textBaseline="top";context.fillText(label,15,browser.mozilla?7:6)};var refreshFavicon=function(){if(!getCanvas().getContext)return;setFaviconTag(getCanvas().toDataURL())};var abbreviateNumber=function(label){var metricPrefixes=[['G',1000000000],['M',1000000],['k',1000]];for(var i=0;i<metricPrefixes.length;++i){if(label>=metricPrefixes[i][1]){label=round(label/metricPrefixes[i][1])+metricPrefixes[i][0];break}}return label};var round=function(value,precision){var number=new Number(value);return number.toFixed(precision)};Tinycon.setOptions=function(custom){options={};for(var key in defaults){options[key]=custom.hasOwnProperty(key)?custom[key]:defaults[key]}return this};Tinycon.setImage=function(url){currentFavicon=url;refreshFavicon();return this};Tinycon.setBubble=function(label,colour){label=label||'';drawFavicon(label,colour);return this};Tinycon.reset=function(){setFaviconTag(originalFavicon)};Tinycon.setOptions(defaults);window.Tinycon=Tinycon})();

// Copyright (c) 2012 Florian H., https://github.com/js-coder https://github.com/js-coder/cookie.js
!function(e,t){var n=function(){return n.get.apply(n,arguments)},r=n.utils={isArray:Array.isArray||function(e){return Object.prototype.toString.call(e)==="[object Array]"},isPlainObject:function(e){return!!e&&Object.prototype.toString.call(e)==="[object Object]"},toArray:function(e){return Array.prototype.slice.call(e)},getKeys:Object.keys||function(e){var t=[],n="";for(n in e)e.hasOwnProperty(n)&&t.push(n);return t},escape:function(e){return String(e).replace(/[,;"\\=\s%]/g,function(e){return encodeURIComponent(e)})},retrieve:function(e,t){return e==null?t:e}};n.defaults={},n.expiresMultiplier=86400,n.set=function(n,i,s){if(r.isPlainObject(n))for(var o in n)n.hasOwnProperty(o)&&this.set(o,n[o],i);else{s=r.isPlainObject(s)?s:{expires:s};var u=s.expires!==t?s.expires:this.defaults.expires||"",a=typeof u;a==="string"&&u!==""?u=new Date(u):a==="number"&&(u=new Date(+(new Date)+1e3*this.expiresMultiplier*u)),u!==""&&"toGMTString"in u&&(u=";expires="+u.toGMTString());var f=s.path||this.defaults.path;f=f?";path="+f:"";var l=s.domain||this.defaults.domain;l=l?";domain="+l:"";var c=s.secure||this.defaults.secure?";secure":"";e.cookie=r.escape(n)+"="+r.escape(i)+u+f+l+c}return this},n.remove=function(e){e=r.isArray(e)?e:r.toArray(arguments);for(var t=0,n=e.length;t<n;t++)this.set(e[t],"",-1);return this},n.empty=function(){return this.remove(r.getKeys(this.all()))},n.get=function(e,n){n=n||t;var i=this.all();if(r.isArray(e)){var s={};for(var o=0,u=e.length;o<u;o++){var a=e[o];s[a]=r.retrieve(i[a],n)}return s}return r.retrieve(i[e],n)},n.all=function(){if(e.cookie==="")return{};var t=e.cookie.split("; "),n={};for(var r=0,i=t.length;r<i;r++){var s=t[r].split("=");n[decodeURIComponent(s[0])]=decodeURIComponent(s[1])}return n},n.enabled=function(){if(navigator.cookieEnabled)return!0;var e=n.set("_","_").get("_")==="_";return n.remove("_"),e},typeof define=="function"&&define.amd?define(function(){return n}):typeof exports!="undefined"?exports.cookie=n:window.cookie=n}(document);

/* Copyright (c) 2010-2012 Marcus Westin */
this.JSON||(this.JSON={}),function(){function f(e){return e<10?"0"+e:e}function quote(e){return escapable.lastIndex=0,escapable.test(e)?'"'+e.replace(escapable,function(e){var t=meta[e];return typeof t=="string"?t:"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+e+'"'}function str(e,t){var n,r,i,s,o=gap,u,a=t[e];a&&typeof a=="object"&&typeof a.toJSON=="function"&&(a=a.toJSON(e)),typeof rep=="function"&&(a=rep.call(t,e,a));switch(typeof a){case"string":return quote(a);case"number":return isFinite(a)?String(a):"null";case"boolean":case"null":return String(a);case"object":if(!a)return"null";gap+=indent,u=[];if(Object.prototype.toString.apply(a)==="[object Array]"){s=a.length;for(n=0;n<s;n+=1)u[n]=str(n,a)||"null";return i=u.length===0?"[]":gap?"[\n"+gap+u.join(",\n"+gap)+"\n"+o+"]":"["+u.join(",")+"]",gap=o,i}if(rep&&typeof rep=="object"){s=rep.length;for(n=0;n<s;n+=1)r=rep[n],typeof r=="string"&&(i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i))}else for(r in a)Object.hasOwnProperty.call(a,r)&&(i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i));return i=u.length===0?"{}":gap?"{\n"+gap+u.join(",\n"+gap)+"\n"+o+"}":"{"+u.join(",")+"}",gap=o,i}}typeof Date.prototype.toJSON!="function"&&(Date.prototype.toJSON=function(e){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(e){return this.valueOf()});var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","	":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;typeof JSON.stringify!="function"&&(JSON.stringify=function(e,t,n){var r;gap="",indent="";if(typeof n=="number")for(r=0;r<n;r+=1)indent+=" ";else typeof n=="string"&&(indent=n);rep=t;if(!t||typeof t=="function"||typeof t=="object"&&typeof t.length=="number")return str("",{"":e});throw new Error("JSON.stringify")}),typeof JSON.parse!="function"&&(JSON.parse=function(text,reviver){function walk(e,t){var n,r,i=e[t];if(i&&typeof i=="object")for(n in i)Object.hasOwnProperty.call(i,n)&&(r=walk(i,n),r!==undefined?i[n]=r:delete i[n]);return reviver.call(e,t,i)}var j;text=String(text),cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(e){return"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),typeof reviver=="function"?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}(),function(){function o(){try{return r in t&&t[r]}catch(e){return!1}}var e={},t=window,n=t.document,r="localStorage",i="__storejs__",s;e.disabled=!1,e.set=function(e,t){},e.get=function(e){},e.remove=function(e){},e.clear=function(){},e.transact=function(t,n,r){var i=e.get(t);r==null&&(r=n,n=null),typeof i=="undefined"&&(i=n||{}),r(i),e.set(t,i)},e.getAll=function(){},e.serialize=function(e){return JSON.stringify(e)},e.deserialize=function(e){if(typeof e!="string")return undefined;try{return JSON.parse(e)}catch(t){return e||undefined}};if(o())s=t[r],e.set=function(t,n){return n===undefined?e.remove(t):(s.setItem(t,e.serialize(n)),n)},e.get=function(t){return e.deserialize(s.getItem(t))},e.remove=function(e){s.removeItem(e)},e.clear=function(){s.clear()},e.getAll=function(){var t={};for(var n=0;n<s.length;++n){var r=s.key(n);t[r]=e.get(r)}return t};else if(n.documentElement.addBehavior){var u,a;try{a=new ActiveXObject("htmlfile"),a.open(),a.write('<script>document.w=window</script><iframe src="/favicon.ico"></frame>'),a.close(),u=a.w.frames[0].document,s=u.createElement("div")}catch(f){s=n.createElement("div"),u=n.body}function l(t){return function(){var n=Array.prototype.slice.call(arguments,0);n.unshift(s),u.appendChild(s),s.addBehavior("#default#userData"),s.load(r);var i=t.apply(e,n);return u.removeChild(s),i}}var c=new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]","g");function h(e){return e.replace(c,"___")}e.set=l(function(t,n,i){return n=h(n),i===undefined?e.remove(n):(t.setAttribute(n,e.serialize(i)),t.save(r),i)}),e.get=l(function(t,n){return n=h(n),e.deserialize(t.getAttribute(n))}),e.remove=l(function(e,t){t=h(t),e.removeAttribute(t),e.save(r)}),e.clear=l(function(e){var t=e.XMLDocument.documentElement.attributes;e.load(r);for(var n=0,i;i=t[n];n++)e.removeAttribute(i.name);e.save(r)}),e.getAll=l(function(t){var n=t.XMLDocument.documentElement.attributes,r={};for(var i=0,s;s=n[i];++i){var o=h(s.name);r[s.name]=e.deserialize(t.getAttribute(o))}return r})}try{e.set(i,i),e.get(i)!=i&&(e.disabled=!0),e.remove(i)}catch(f){e.disabled=!0}e.enabled=!e.disabled,typeof module!="undefined"&&typeof module!="function"?module.exports=e:typeof define=="function"&&define.amd?define(e):this.store=e}();



/********************
 * INTERNAL FUNCTIONS
 ********************/

/* empty php.js (http://phpjs.org) */
function empty(e){var t,n,r,i;var s=[t,null,false,0,"","0"];for(r=0,i=s.length;r<i;r++){if(e===s[r]){return true}}if(typeof e==="object"){for(n in e){return false}return true}return false};

/* str_repeat php.js (http://phpjs.org) */
function str_repeat(e,t){var n="";while(true){if(t&1){n+=e}t>>=1;if(t){e+=e}else{break}}return n}

/* explode php.js (http://phpjs.org) */
function explode(e,t,n){if(arguments.length<2||typeof e=="undefined"||typeof t=="undefined")return null;if(e===""||e===false||e===null)return false;if(typeof e=="function"||typeof e=="object"||typeof t=="function"||typeof t=="object"){return{0:""}}if(e===true)e="1";e+="";t+="";var r=t.split(e);if(typeof n==="undefined")return r;if(n===0)n=1;if(n>0){if(n>=r.length)return r;return r.slice(0,n-1).concat([r.slice(n-1).join(e)])}if(-n>=r.length)return[];r.splice(r.length+n);return r};

/* implode php.js (http://phpjs.org) */
function implode(e,t){var n="",r="",i="";if(arguments.length===1){t=e;e=""}if(typeof t==="object"){if(Object.prototype.toString.call(t)==="[object Array]"){return t.join(e)}for(n in t){r+=i+t[n];i=e}return r}return t};

/* array_keys php.js (http://phpjs.org) */
function array_keys(e,t,n){var r=typeof t!=="undefined",i=[],s=!!n,o=true,u="";if(e&&typeof e==="object"&&e.change_key_case){return e.keys(t,n)}for(u in e){if(e.hasOwnProperty(u)){o=true;if(r){if(s&&e[u]!==t){o=false}else if(e[u]!=t){o=false}}if(o){i[i.length]=u}}}return i};

/* sprintf php.js (http://phpjs.org) */
function sprintf(){var e=/%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;var t=arguments,n=0,r=t[n++];var i=function(e,t,n,r){if(!n){n=" "}var i=e.length>=t?"":Array(1+t-e.length>>>0).join(n);return r?e+i:i+e};var s=function(e,t,n,r,s,o){var u=r-e.length;if(u>0){if(n||!s){e=i(e,r,o,n)}else{e=e.slice(0,t.length)+i("",u,"0",true)+e.slice(t.length)}}return e};var o=function(e,t,n,r,o,u,a){var f=e>>>0;n=n&&f&&{2:"0b",8:"0",16:"0x"}[t]||"";e=n+i(f.toString(t),u||0,"0",false);return s(e,n,r,o,a)};var u=function(e,t,n,r,i,o){if(r!=null){e=e.slice(0,r)}return s(e,"",t,n,i,o)};var a=function(e,r,a,f,l,c,h){var p;var d;var v;var m;var g;if(e=="%%"){return"%"}var y=false,b="",w=false,E=false,S=" ";var x=a.length;for(var T=0;a&&T<x;T++){switch(a.charAt(T)){case" ":b=" ";break;case"+":b="+";break;case"-":y=true;break;case"'":S=a.charAt(T+1);break;case"0":w=true;break;case"#":E=true;break}}if(!f){f=0}else if(f=="*"){f=+t[n++]}else if(f.charAt(0)=="*"){f=+t[f.slice(1,-1)]}else{f=+f}if(f<0){f=-f;y=true}if(!isFinite(f)){throw new Error("sprintf: (minimum-)width must be finite")}if(!c){c="fFeE".indexOf(h)>-1?6:h=="d"?0:undefined}else if(c=="*"){c=+t[n++]}else if(c.charAt(0)=="*"){c=+t[c.slice(1,-1)]}else{c=+c}g=r?t[r.slice(0,-1)]:t[n++];switch(h){case"s":return u(String(g),y,f,c,w,S);case"c":return u(String.fromCharCode(+g),y,f,c,w);case"b":return o(g,2,E,y,f,c,w);case"o":return o(g,8,E,y,f,c,w);case"x":return o(g,16,E,y,f,c,w);case"X":return o(g,16,E,y,f,c,w).toUpperCase();case"u":return o(g,10,E,y,f,c,w);case"i":case"d":p=+g||0;p=Math.round(p-p%1);d=p<0?"-":b;g=d+i(String(Math.abs(p)),c,"0",false);return s(g,d,y,f,w);case"e":case"E":case"f":case"F":case"g":case"G":p=+g;d=p<0?"-":b;v=["toExponential","toFixed","toPrecision"]["efg".indexOf(h.toLowerCase())];m=["toString","toUpperCase"]["eEfFgG".indexOf(h)%2];g=d+Math.abs(p)[v](c);return s(g,d,y,f,w)[m]();default:return e}};return r.replace(e,a)};

/* is_int php.js (http://phpjs.org) */
function is_int(e){return e===+e&&isFinite(e)&&!(e%1)};

/* is_float php.js (http://phpjs.org) */
function is_float(e){return+e===e&&(!isFinite(e)||!!(e%1))};

/* array_slice (http://phpjs.org) */
function array_slice(e,t,n,r){var i="";if(Object.prototype.toString.call(e)!=="[object Array]"||r&&t!==0){var s=0,o={};for(i in e){s+=1;o[i]=e[i]}e=o;t=t<0?s+t:t;n=n===undefined?s:n<0?s+n-t:n;var u={};var a=false,f=-1,l=0,c=0;for(i in e){++f;if(l>=n){break}if(f==t){a=true}if(!a){continue}++l;if(this.is_int(i)&&!r){u[c++]=e[i]}else{u[i]=e[i]}}return u}if(n===undefined){return e.slice(t)}else if(n>=0){return e.slice(t,t+n)}else{return e.slice(t,n)}};

/* array_merge (http://phpjs.org) */
function array_merge(){var e=Array.prototype.slice.call(arguments),t=e.length,n,r={},i="",s=0,o=0,u=0,a=0,f=Object.prototype.toString,l=true;for(u=0;u<t;u++){if(f.call(e[u])!=="[object Array]"){l=false;break}}if(l){l=[];for(u=0;u<t;u++){l=l.concat(e[u])}return l}for(u=0,a=0;u<t;u++){n=e[u];if(f.call(n)==="[object Array]"){for(o=0,s=n.length;o<s;o++){r[a++]=n[o]}}else{for(i in n){if(n.hasOwnProperty(i)){if(parseInt(i,10)+""===i){r[a++]=n[i]}else{r[i]=n[i]}}}}}return r};

/* change node name. @jakov, @Andrew Whitaker and @jazzbo ( stackoverflow ) */
$.fn.setElementType=function(e){var t=[];$(this).each(function(){var n={};$.each(this.attributes,function(e,t){n[t.nodeName]=t.nodeValue});var r=$("<"+e+"/>",n).append($(this).contents());$(this).replaceWith(r);t.push(r)});return $(t)};


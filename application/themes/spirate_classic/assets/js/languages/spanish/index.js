if(!lang)
	var lang = {};

lang['close'] = 'Cerrar';
lang['buttons'] = {
	'ok' : 'aceptar',
	'cancel' : 'cancelar',
	'next' : 'siguiente',
	'prev' : 'anterior',
	'finalize' : 'terminar'
}
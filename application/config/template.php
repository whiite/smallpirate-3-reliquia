<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// default themes dir
$config['template']['themes_dir'] = APPPATH . 'themes/';

// default theme
$config['template']['default_theme'] = 'default';

// template default regions
$config['template']['default_regions'] = array
(
	'header',
	'breadcrumb',
	'content',
	'footer'
);

// default base_template
$config['template']['default_base_template'] = 'default';

// template extensions
$config['template']['templates_extension'] = '.html';

// title pages separator
$config['template']['title_separator'] = ' - ';

/* twig vars */

// twig environment
$config['template']['twig']['environment'] = array
(
	'cache' => FALSE,
	'charset' => 'utf-8',
	'base_template_class' => 'Twig_Template',
	'auto_reload' => NULL,
	'strict_variables' => FALSE,
	'autoescape' => FALSE,
	'optimizations' => -1
);

// twig autoload filters/functions
$config['template']['twig']['autoload'] = array
(
	'functions' => array('base_url', 'site_url', 'uri_string', 'urlencode'),
	'filters' => array()
);
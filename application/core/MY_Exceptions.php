<?php defined('BASEPATH') OR exit('No direct script access allowed');

if( !class_exists('CI_Controller'))
	require BASEPATH.'core/Controller.php';

class MY_Exceptions extends CI_Exceptions{

	var $CI;

	public function __construct(){

		parent::__construct();
		$this->CI =& get_instance();

	}

	public function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	{
		
		if( !isset($this->CI) )
			return parent::show_error($heading, $message, $template, $status_code);

		return Modules::run('errors/errors/_handle_error', array(
			'heading' => $heading,
			'message' => $message,
			'view' => $template,
			'status_code' => $status_code
		));

	}

	public function show_404($page = '', $log_error = TRUE){

		$heading = "404 Page Not Found";
		$message = "The page you requested was not found.";

		// By default we log this, but allow a dev to skip it
		if ($log_error)
		{
			log_message('error', '404 Page Not Found --> ' . $page);
		}

		echo $this->show_error($heading, $message, 'error_404', 404);
		exit;

	}

}
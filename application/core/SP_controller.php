<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SP_controller extends MY_controller{
	
	public function __contruct(){
		
		parent::__construct();

		$this->notifications->get_notifications();

		// append metatags
		$this->template->append_metadata(array(
			array('type' => 'meta', 'http-equiv' => 'Content-Type', 'content' => 'text/html; charset=' . strtolower($this->config->item('charset'))),
			array('type' => 'meta', 'http-equiv' => 'X-Frame-Options', 'content' => 'Deny'),
			array('type' => 'link', 'rel' => 'apple-touch-icon image_src', 'href' => ''),
			array('type' => 'meta', 'name' => 'description', 'content' => $this->settings->get('site_meta_description')),
			array('type' => 'og', 'property' => 'site_name', 'content' => $this->settings->get('site_name'))
		));
		
		$this->template->set(array(
			'current_user' => $this->current_user,
			'user_is_logged' => $this->ion_auth->logged_in(),
		), TRUE);
		
		/*/ set title and slogan
		$this->template->title(array(
			$this->settings->get('site_name'),
			$this->settings->get('site_slogan')
		));
		*/
		$this->template

		// add path to js langs
		->add_dir_assets(APPPATH . 'assets/js/languages/spanish/')

		// main languages
		->add_asset('index.js')
		
		// set main twiggy vars
		->set(array(
			'base_url' => base_url('/'),
			'current_user' => $this->current_user,
			'user_is_logged' => $this->ion_auth->logged_in(),
		), TRUE)
		
		// set global javascript vars
		->set_js_var(array(
			'base_url' => base_url(),
			'path_url' => '/' . uri_string(),
			'theme_url' => $this->template->theme['theme_url'],
			'default_theme_url' => $this->template->theme['default_theme_url'],
			'smileys_url' => $this->template->theme['default_theme_url'] . '/assets/images/smileys/'
		), 'global')
		
		// set user javascript vars
		->set_js_var(array(
			'id' => (int) $this->current_user['ID'],
			'logged_in' => $this->ion_auth->logged_in(),
			'lang' => 'spanish',
			'notifications' => $this->notifications->count,
			'messages' => (int) $this->current_user['private_messages']
		), 'user');
		
	}
	
}
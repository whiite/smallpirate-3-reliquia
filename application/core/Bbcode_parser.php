<?php
	
/**
 * Spirate
 *
 * Social linksharing script
 *
 * @package		Spirate
 * @author		Spirate team
 * @license		http://spirate.net/license.txt
 * @link		http://spirate.net
 * @version		3.0
 */

/**
 * Spirate BBCODE library
 *
 * This class enables you to parse bbcode to html or html to bbcode
 *
 * @package		Spirate
 * @subpackage	Libraries
 * @subpackage	nbbc
 * @category  	Libraries
 * @author		Spirate Team
 * @version		1.0
 */


/* ------------------------------------------------------------------------ */

require_once dirname(__FILE__) . '/nbbc/nbbc.php';
 
class Bbcode_parser{

	var $CI;
	var $parser = NULL;

	public function __construct(){

		// get CI instance
		$this->CI =& get_instance();

		// set parser instance
		$this->parser = new BBCode;

		// unset default nbbc smileys ( We have other smileys :) )
		$this->parser->defaults->smileys = array();
		$this->parser->smiley_url = '';
		$this->parser->smiley_dir = '';

		// load defaults
		$this->load_default_tags();

		// load smileys
		$this->CI->load->model('smileys_model');
		$this->load_smileys();

	}

	public function parse_bbcode( $text = '' ){

		return $this->parser->parse($text);

	}
	
	public function parse_html( $html = '' ){

		return $html;
	}

	public function add_bbcode(){



	}

	private function _add_bbcode($rules = array()){



	}

	private function load_default_tags()
	{

		$rules = array
		(
			'hr' => $this->parser->tag_rules['rule'],
			'justify' => array(
				'simple_start' => '<div style="text-align: justify">',
				'simple_end' => '</div>',
				'class' => 'block',
				'allow_in' => array('block', 'code')
			),
			'li' => array(
				'simple_start' => "\n<li>",
				'simple_end' => "</li>",
				'class' => 'listitem',
				'allow_in' => array('list'),
				'before_tag' => "a",
				'after_tag' => "a",
				'before_endtag' => "a",
				'after_endtag' => "a",
			),
			'ul' => array(
				'simple_start' => "\n" . '<ul class="bbcode_ul">',
				'simple_end' => "\n</ul>\n",
				'class' => 'list',
				'allow_in' => array('block')
			),
			'youtube' => array(
				'mode' => BBCODE_MODE_CALLBACK,
				'method' => array($this, 'valid_nbbcodes'),
				'class' => 'block',
				'allow_in' => array('listitem', 'block', 'columns', 'inline', 'link')
			)

		);

		foreach( $rules as $tag => $rule )
		{

			$this->parser->AddRule($tag, $rule);

		}

	}

	public function valid_nbbcodes($bbcode, $action, $name, $default, $params, $content){

		
		switch( preg_replace('~\s*~', '', $name) )
		{

			case 'youtube':
				
				if( $action == BBCODE_CHECK )
					return TRUE;

			 	if( !preg_match('~^(?:http://((?:www|au|br|ca|es|fr|de|hk|ie|in|il|it|jp|kr|mx|nl|nz|pl|ru|tw|uk)\.)?youtube\.com/(?:[^"]*?)(?:(?:video_)?id=|(?:v|p)(?:/|=)))?([0-9a-f]{16}|[0-9a-z-_]{11})~i', $content, $match) )
					return 'bad video url: ' . htmlspecialchars($content);

				$youtube_id = $match[0];

				return '<iframe width="560" height="315" frameborder="0" allowfullscreen="" data-youtube-id="' . $youtube_id . '" src="http://www.youtube.com/embed/' . $youtube_id . '?wmode=opaque"></iframe>';

			break;

		}


	}

	private function load_smileys(){

		$this->_smileys = $this->CI->smileys_model->get_smileys_list(TRUE, FALSE);
		$this->parser->SetSmileyURL(preg_replace('~\/?$~', '', $this->CI->smileys_model->current_smileys_url));

		foreach( $this->_smileys as $code => $path )
			$this->parser->AddSmiley($code, $path);

	}

}
<?php
require APPPATH . "third_party/MX/Controller.php";

class MY_controller extends MX_Controller {
		
	public $module;
	public $controller;
	public $method;
	public $vars;
	
	function __construct() {
		
		parent::__construct();

		//start benchmark
		$this->benchmark->mark('my_controller_start');
		
		// load sparks libraries
		$this->load->spark('Sprinkle/1.0.5');
		
		// load migration library
		$this->load->library('migration');

		// load cache library
		$this->load->driver('cache');
		
		// load models
		$this->load->model(array(
			'settings/settings_model',
			'users/ion_auth_model',
			'users/users_model',
			'permissions/permissions_model'
		));

		// load errors language
		$this->lang->load('errors');
		
		// load auth library
		$this->load->library('users/ion_auth');
		
		// define class, function, method ( work out module )
		foreach( array('module' => 'fetch_module', 'controller' => 'fetch_class', 'method' => 'fetch_method') as $obj => $fetch )
			ci()->{$obj} = $this->{$obj} = $this->router->{$fetch}();
		
		// define models AS controller method
		ci()->settings = $this->settings = $this->settings_model;
		ci()->permissions = $this->permissions_model;
		ci()->template = $this->template;
		ci()->current_user = $this->current_user = $this->users_model->get_user_data();

		// load notifications class
		$this->load->library('notifications/notifications');
		
		// global settings
		$this->configs = $this->settings->get_all('global');
		
		// set current theme
		$theme = $this->settings->get('current_theme') ? $this->settings->get('current_theme') : FALSE;
		$this->template->set_theme($theme);

		// add path to module assets
		$this->template->add_module_assets();
		
		$this->vars['theme_path'] = APPPATH . 'themes/' . $theme . '/';
		$this->vars['assets_theme_path'] = $this->vars['theme_path'] . 'assets/';

		$this->benchmark->mark('my_controller_end');
		
	}

}

function ci() {

	return get_instance();

}

?>

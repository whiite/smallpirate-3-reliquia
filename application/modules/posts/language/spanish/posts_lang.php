<?php
	
$lang['last_posts'] = '&Uacute;ltimos posts.';
$lang['empty_posts_list'] = 'No se encontraron posts en la base de datos.';
$lang['empty_comments_list'] = 'No se encontr&oacute; ningun comentario.';
$lang['empty_post_id'] = 'La url del post es incorrecta.';
$lang['post_not_exists'] = 'Ese post no existe o fue eliminado.';
$lang['error_tag_not_allowed'] = 'El tag "%s" no esta permitido.';
$lang['error_tags_not_allowed'] = 'Los tags "%s" no estan permitidos.';
$lang['error_tags_min_length'] = 'Debes ingresar m&aacute;s de %d tags.';
$lang['error_tags_max_length'] = 'Debes ingresar m&aacute;s de %d tags.';
$lang['error_distinct_tags'] = 'Debes ingresar m&aacute;s de %d tags distintos.';
$lang['error_unchecked_sources'] = 'Algunas fuentes no fueron comprobadas.';
$lang['error_empty_sources'] = 'Debes ingresar las fuentes para tu post.';
$lang['error_empty_category'] = 'Debes seleccionar una categoria.';
$lang['error_incorrect_category'] = 'La categoria seleccionada no existe.';
$lang['error_allowed_groups'] = 'Debes seleccionar almenos un grupo visible para tu post.';
$lang['post_in_moderation'] = 'El post actualmente se encuentra en moderaci&oacute;n.';

?>

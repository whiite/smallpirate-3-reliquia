<style type="text/css">#main_content .page_content{padding:0!important}</style>
<div id="main_post">

	<div class="header_post">
		<div class="title_post clearfix">
			<h1 property="dc:title"><?=$post['title']?></h3>
		</div>
		
		<div class="sub_header_post" style="height:170px">
			<div class="author_info">
				<div class="avatar">
					<div class="wrapper_avatar"></div>
					<a href=""><img src="<?=$author['avatar']?>"></a>
				</div>
				<div class="follow-button">
						<button class="sp-button">Seguir usuario</button>
				</div>
				<div class="author_summary">
					<div>
						<a class="ui_icon clearfix username" href="<?php echo $author['href']?>" rel="author"><i class="icon male"></i><span class="text"><?=$author['name']?></span></a>
						<strong class="ui_icon clearfix"><i class="icon group"></i><span class="text" style="color:<?=$author['group']['color']?>"><?=$author['group']['name']?></span></strong>
						<strong class="ui_icon clearfix"><i class="icon mail"></i><a class="text">Enviarle un mensaje</a></strong>
					</div>
				</div>
				<div class="author_stats">
					<ul class="stats_list clearfix">
						<li>
							<span class="stat_count posts clearfix"><?=sp_number_format($author['stats']['posts'])?></span>
							<div class="ui_icon stat_text clearfix"><i class="icon posts"></i><span class="text">Posts</span></div>
						</li>
						<li>
							<span class="stat_count points clearfix"><?=sp_number_format($author['stats']['points'])?></span>
							<div class="ui_icon stat_text clearfix"><i class="icon points"></i><span class="text">Puntos</span></div>
						</li>
						<li>
							<span class="stat_count followers clearfix"><?=sp_number_format($author['stats']['followers'])?></span>
							<div class="ui_icon stat_text clearfix"><i class="icon followers"></i><span class="text">Seguidores</span></div>
						</li>
					</ul>
				</div>
			</div>
			<div class="post_settings">
				<div class="right clearfix">
					<strong class="navigation">Ir a otro post: </strong>
					<a class="option navigation tt-tipsy prev" href="<?=base_url('/posts/ver/' . $post['id'] . '/anterior/')?>" title="Post anterior"></a>
					<a class="option navigation tt-tipsy random" href="<?=base_url('/posts/ver/' . $post['id'] . '/aleatorio')?>" title="Post aleatorio"></a>
					<a class="option navigation tt-tipsy next" href="<?=base_url('/posts/ver/' . $post['id'] . '/siguiente')?>" title="post siguiente"></a>
				</div>
				<?php if($this->current_user['is_logged_in']): ?>
				<hr>
				<a class="ui_icon clearfix option denounce"><i class="icon flag hover-state"></i><span class="text">Denunciar este post</span></a>
				<?php endif ?>
				<?php if( $this->permissions->has_permission('moderate_page') || $author['is_own'] ): ?>
				<a class="ui_icon clearfix option delete" onclick="post.remove(<?=$post['id']?>)"><i class="icon trash"></i><span class="text">Eliminar este post</span></a>
				<a class="ui_icon clearfix option edit" href="<?=base_url(sprintf('/posts/editar/%d', $post['id']))?>"><i class="icon edit"></i><span class="text">Editar este post</span></a>
				<?php endif ?>
			</div>
		</div>
		
	</div>
	<div class="post_context clearfix">
		<!-- <div class="left_advertise"><img src="http://placehold.it/160x500/f1f1f1/222222&text=publicidad"></div>-->
		<div class="left_column equal-height">
			
			<div class="post_body">
				<div class="advertise"><img src="http://placehold.it/700x80/f1f1f1/222222"></div>
				<span class="msg" property="dc:content">
					<?=$post['msg']?>
				</span>
				<div class="advertise"><img src="http://placehold.it/700x80/f1f1f1/222222"></div>
			</div>

			<div class="post_options rounded">
				<div class="content rounded">
					<h3 class="hr-vert"><span>Opciones del post</span></h3>
					
					<div class="share-buttons">
						<!---->
					</div>
					<div class="clearfix">
						<div class="vote_post_wrapper">
							<div class="points_response rounded">
								<strong>Puntos agregados!</strong>
							</div>
							<div class="post_points rounded">
								<div class="wrapper_points_bar">
									<ul class="points_bar clearfix">
										<li class="first"><strong>+</strong></li>
										<?php

											foreach($spectator['points'] as $point )
												echo '
												<li><a href="javascript:void(0)" title="' . sprintf('Dar %d punto%s', $point, $point > 1 ? 's' : '') . '">' . $point . '</a></li>';
											
											if( $spectator['hidden_points'] )
											{
												
												echo '
												<li class="dropdown">
												<a><i></i></a>
												<ul class="clearfix">';

												foreach($spectator['hidden_points'] as $point )
													echo '<li><a href="javascript:void(0)" title="' . sprintf('Dar %d punto%s', $point, $point > 1 ? 's' : '') . '">' . $point . '</a></li>';
											
												echo '
												</ul></li>';

											}

										?>
										<li class="last"><strong>puntos</strong></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="total_points">
							<div class="total_points_wrapper"><span class="count"><?php echo sp_number_format($post['stats']['points']) ?></span><span class="text">Puntos</span></div>
							<div class="post_score clearfix">
								<?php if($post['stats']['score']): ?>
								<div class="rating"><span>Score:</span> <span class="value"><?php echo $post['stats']['score'] ?></span>/<span class="best">10</span></div>
								<?php endif ?>
								<div><span>Votos:</span> <span class="votes"><?php echo sp_number_format($post['stats']['votes']) ?></span></div>
							</div>
						</div>
					</div>
					<hr />
					<div class="clearfix">
						<div class="options_buttons">
							<button class="sp-button yellow">
								<div class="ui_icon clearfix"><i class="icon bookmarks active"></i><span class="text">A favoritos</span></div>
							</button>
							<button class="sp-button">
								<div class="ui_icon clearfix"><i class="icon followers active"></i><span class="text">Seguir post</span></div>
							</button>
						</div>
						<ul class="post_stats clearfix">
							<li>
								<strong><?=sp_number_format($post['stats']['followers'])?></strong>
								<span>Seguidores</span>
							</li>
							<li>
								<strong><?=sp_number_format($post['stats']['bookmarks'])?></strong>
								<span>Favoritos</span>
							</li>
							<li>
								<strong><?=sp_number_format($post['stats']['visits'])?></strong>
								<span>Visitas</span>
							</li>
						</ul>

					</div>
				</div>
			</div>
			<?php if(true): ?>
			<div class="post_comments">
				<h3 class="hr-vert white"><span><?=$post['stats']['comments']?> comentario</span></h3>
				<div class="comments">
					
					<div class="comment-context">
						<div class="avatar">
							<a><img src="<?= $this->current_user['avatar']['medium'] ?>"></a>
						</div>
						<div class="comment">
							<div class="comment-info">
								<a class="item nick">Jonathan</a>
								<span class="item time" data-livestamp="<?=time()?>"><?=$post['time']['date']?></span>
								<span class="item rounded edit">Editado</span>
								<span class="item action">comento:</span>
							</div>
							<div class="comment_options rounded">
								<ul class="clearfix">
									<li class="first"><a class="icon delete"></a></li>
									<li><a class="icon edit"></a></li>
									<li><a class="icon reply_comment"></a></li>
									<li><a class="icon vote_down"></a></li>
									<li class="last"><a class="icon vote_up"></a></li>
								</ul>
							</div>
							<span class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus lacus metus, imperdiet in varius feugiat, pulvinar in lacus. Donec nec purus feugiat, convallis quam id, dignissim risus. Suspendisse vel sodales lorem, eget lobortis purus. Nulla facilisi. Integer lobortis molestie ornare. Phasellus risus velit, faucibus mollis ante sed, venenatis viverra risus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec ac mi id libero pellentesque posuere. Aliquam erat volutpat. Vivamus tristique lectus id interdum condimentum. Proin vulputate dignissim turpis at placerat. Vivamus purus urna, sodales vitae nulla eget, viverra feugiat risus. Proin odio nibh, faucibus elementum tellus vel, semper volutpat orci. Sed eu sagittis nibh, vel imperdiet nisl. Proin fermentum lectus diam, eu pellentesque purus pretium dignissim. Aenean pulvinar, elit vel elementum tempus, ligula enim ultrices mi, ut sollicitudin nulla lectus non quam. </span>
						</div>
						<div class="subcomments">
							
							<div class="comment-context">
								<div class="avatar">
									<a><img src="<?= $this->current_user['avatar']['small'] ?>"></a>
								</div>
								<div class="comment">
									<div class="comment-info">
										<a class="item nick">Jonathan</a>
										<span class="item time">Hace 20 minutos</span>
										<span class="item action">respondio:</span>
									</div>
									<div class="comment_options rounded">
										<ul class="clearfix">
											<li class="first"><a class="icon delete"></a></li>
											<li><a class="icon vote_down"></a></li>
											<li class="last"><a class="icon vote_up"></a></li>
										</ul>
									</div>
									<span class="msg"><?=str_repeat(' lorem ipssum', 30) ?></span>
								</div>
							</div>
							
						</div>

					</div>
					
				</div>
			</div>
			<?php endif ?>
			<div class="comment_box">
				<h3 class="hr-vert white"><span>Agregar un comentario</span></h3>
				<?php if( $this->permissions->has_permission('add_comment_post') ): ?>
				<div class="comment_form rounded">
					<textarea id="comment_textarea"></textarea>
					<div id="comments_left_chars"><span class="rounded"></span></div>
					<div class="buttons_form">
						<button class="sp-button gray">Comentar</button>
					</div>
				</div>
				<?php else: ?>
				<?php 

					if( $this->current_user )
						echo '
					<div class="box_error">No tienes permisos para agregar comentarios.</div>';
					else
						echo '
					<div class="box_info">Debes <a href="'.base_url('/users/ingresar').'">ingresar</a> para poder agregar un comentario</div>';
				
				?>
				<?php endif ?>
			</div>

		</div>
		<div class="right_column">
		<?php if( !empty($related_posts) ): ?>
			<div class="item">
				<h4>Posts relacionados</h4>
				<div class="content">
					<ul class="related_posts">
					<?php

					foreach( $related_posts as $post )
						echo '
						<li>
							<a class="ui_icon clearfix" href="' . $post['href'] . '" title="' . $post['title'] . '">
								<i class="categories-icon sprite-categories-' . $post['category']['id'] . '"></i>
								<span class="text">' . $post['title'] . '</span>
							</a>
						</li>';
					?>
					</ul><br />
				</div>
			</div>
		<?php endif ?>
			<div class="item">
				<h4>Tags</h4>
				<div class="content">
					<?php
						foreach( $tags as $tag )
						{

							echo '<div class="tag-post"><span><a title="' . $tag['name'] . '" href="' . $tag['href'] . '">' . $tag['name'] . '</a></span></div>';

						}
					?>
				</div>
			</div>
			<div class="item">
				<h4>Publicidad</h4>
				<div class="content">
					<div class="advertise"><img src="http://placehold.it/180x600/f1f1f1/222222"></div>
				</div>
			</div>
		</div>
	</div>
		
</div>
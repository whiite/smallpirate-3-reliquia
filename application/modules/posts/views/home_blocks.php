<div class="posts-grid clearfix">
block
	<!-- left column -->
 	<div class="section l">
 		<div class="item-block-posts" id="lastest_posts">
 			<div class="title clearfix">
	 			<h3><?= $this->lang->line('last_posts') ?></h3>
	 			<i>rss</i>
	 			
	 			<div class="right">
	 			<span>
	 				<select name="categories" id="categories">
	 				<option value="-1">Filtrar por categoria</option>
	 				 <optgroup label="&nbsp;"></optgroup>
	 				 <optgroup label="Seleccionar categoria:"><?php
					
	 				foreach($categories as $category)
						echo '
						<option ' . ($current_category['ID'] == $category['ID'] ? 'selected="selected" ' : '') . ' slug="' . $category['slug'] . '" value="' . $category['ID'] . '">' . $category['name'] . '</option>';
					
					?>
					</optgroup>
					</select>
	 			</span>
	 			</div>
 			</div>
 			<div class="content">
 			<ul class="posts-list">
 			<?php

 			if( empty($last_posts) )
 			{
 				
 				if( $on_newbies_section && $total_posts > 0 )
 				{

 					if( $current_category )
 						echo '<div class="box_info">No se encontraron' , $current_page > 0 ? ' m&aacute;s ' : ' ' , 'posts en la categoria <strong>' . $current_category['name'] . '</strong> de la seccion <strong>newbies</strong>.</div>';

 					else
 						echo '<div class="box_info">No se encontraron' , $current_page > 0 ? ' m&aacute;s ' : ' ' , 'posts en la seccion <strong>newbies</strong>.</div>';

 				}
 				
 				else if ( $current_category && $total_posts > 0 )
 					echo '<div class="box_info">No se encontr&oacute;' , $current_page > 0 ? ' m&aacute;s ' : ' ' , 'posts en la categoria <strong>' . $current_category['name'] . '</strong>.</div>';

 				else if ( $current_page > 1 && $total_posts > 0)
 					echo '<div class="box_info">No se encontraron mas posts.</div>';

 				else
 					echo '<div class="box_error">' . $this->lang->line('empty_posts_list') . '</div>';
			
			}
 			
 			if( isset( $last_posts['sticky']) )
 			{

 				echo '<li class="sticky-list-wrap"><ul class="sticky-list">';
 				
 				foreach( $last_posts['sticky'] as $post )
 					echo '
	 				<li>
	 					<div class="post sticky clearfix">
	 						<i class="categories-icon sprite-categories-' . $post['category']['id'] . '"></i>
	 						<a class="ui_icon clearfix" href="' . $post['href'] . '">' . $post['title'] . '</a>
	 					</div>
	 				</li>';

	 			echo '</ul></li>';

	 		}

			if ( isset($last_posts['normal']) )
				foreach($last_posts['normal'] as $post)
					echo '
	 				<li>
	 					<div class="post clearfix">
	 						<i class="categories-icon sprite-categories-' . $post['category']['id'] . '"></i>
	 						<a class="ui_icon clearfix" href="' . $post['href'] . '">' . $post['title'] . '</a>
	 					</div>
	 				</li>';
				
			?>
			</ul>
			<?php
				if( $pagination ){
					
					echo '<br /><strong>P&aacute;ginas:</strong>					
					<div>
						' . $pagination . '
					</div>';
				}
			?>
 			</div>
		</div>
 	</div>
 	<!-- end left column -->
 	
 	<!-- center column -->
 	<div class="section c">
 	
 		<div class="item-block-posts" id="statistics">
 			<div class="title clearfix">
	 			<h3>Estadisticas</h3>
	 			<span></span>
	 			<i></i>
 			</div>
			<div class="content">
				
			</div>
		</div>
		
		<div class="item-block-posts" id="lastest_commments">
 			
 			<div class="title clearfix">
	 			<h3>Comentarios recientes</h3>
	 			<span></span>
	 			<i></i>
 			</div>

			<div class="content">
				<ul id="comments"><?php

					if( empty($last_comments) )
						echo '<div class="box_error">' . $this->lang->line('empty_comments_list') . '</div>';
					
					foreach( $last_comments as $comment )
					echo '
					<li>
						<span class="user">
							<a href="' . $comment['user']['href'] . '" title="' . $comment['user']['nick'] . '">' . $comment['user']['nick'] . '</a>
						</span>
						<span class="post">
							<a href="' . $comment['post']['href'] . '" title="' . $comment['post']['title'] . '">' . $comment['post']['title'] . '</a>
						</span>
					</li>';

				?>

				</ul>
			</div>

			
		</div>
		<?php if( $this->input->ip_address() !== '127.0.0.1' ): ?>
		<!-- remover luego -->
		<style>
		.changelog li{
			padding: 4px 4px 19px 4px;
			position: relative;
		}
		.changelog li:nth-child(even){
			background: #f1f1f1;
		}
		.changelog li div.thumbnail img{
			max-width: 100px;
		}
		.changelog li span.time{
			color: #777;
			font-size: 10px;
			position: absolute;
			bottom: 0;
			right: 0;
		}
		</style>
		<script type="text/javascript">
			function changelog_thumbnail(a){
				
				sp.modal.set({
					'title' : $(a).find('img').attr('title'),
					'buttons' : [
						{
							'label' : 'Cerrar',
							'class' : 'clear',
							'callback' : 'close'
						}
					]
				}).load();

				var img = $('<img />').attr('src', $(a).find('img').attr('src') + '?' + Math.random()).load(function(){

					$(img).appendTo('body').css({'position' : 'absolute', 'top' : -9999, 'left' : -9999});

					sp.modal
					.set_width($(img)[0].clientWidth + 20)
					.set_body('<img src="' + $(a).find('img').attr('src') + '" style="max-width:600px">')
					.stop_load()
					.center();
				});				

			}
		</script>
		<div class="item-block-posts" id="changelog">
 			<div class="title clearfix">
	 			<h3>Changelog V 3.0</h3>
	 			<span></span>
	 			<i></i>
 			</div>
			<div class="content" style="height: 200px; overflow-y:scroll;margin-top:10px">
				<ul class="changelog">
					<li>
						<div>
							<span class="update">Sistema de avatars via: Archivo, webcam y urls en <strong style="color:green">progreso</strong>.</span>
							<span class="time">25/jul/2013 - 02:17</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Agregada libreria GD <strong style="color:darkorange">ImageWorkShop</strong>.</span>
							<span class="time">25/jul/2013 - 23:53</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Incompatibilidades con IE corregidas en un peque&ntilde;o porcentaje.</span>
							<span class="time">24/jul/2013 - 11:31</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Sistema de Tags creado.</span>
							<span class="time">23/jul/2013 - 06:20</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Seccion <a href="<?=base_url()?>posts/agregar" target="_blank">publicar posts</a> <strong>terminada</strong>.</span>
							<span class="time">22/jul/2013 - 01:00</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">modulo <a href="<?=base_url()?>ajax/" target="_blank">ajax</a> creado.</span>
							<span class="time">20/jul/2013 - 11:00</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">creada la libreria <strong style="color:darkred">BBCode</strong> y <strong style="color:darkgreen">Scraper</strong></span>
							<span class="time">19/jul/2013 - 02:30</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update"><strong>Tooltips</strong> de validacion <a href="<?=base_url()?>posts/agregar/" target="_blank">agregados</a> en fomulario de posts</span>
							<span class="time">16/jul/2013 - 05:17</span>
						</div>
						<div class="thumbnail">
							<a onclick="changelog_thumbnail(this)" href="javascript:void(0)">
								<img src="http://i.imgur.com/R30ZbNS.png?1" title="Tooltips" />
							</a>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Agregada validacion ( solo javascript ) y vista previa para el <a href="<?=base_url()?>posts/agregar/" target="_blank">formulario</a> de posts</span>
							<span class="time">16/jul/2013 - 03:06</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Agregado diseño para <a href="<?=base_url()?>posts/agregar/" target="_blank">publicacion</a> de posts</span>
							<span class="time">14/jul/2013 - 22:46</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Agregado editor wisywig/bbcode en <a href="<?=base_url()?>posts/agregar/" target="_blank">/agregar/</a></span>
							<span class="time">14/jul/2013 - 12:31</span>
						</div>
					</li>
					<li>
						<div>
							<span class="update">Usuario <a href="<?=base_url()?>users/demo/" target="_blank">demo</a> agregado ( user: demo pass: spirate )</span>
							<span class="time">12/jul/2013 - 17:12</span>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<?php endif ?>
		
	</div>
	<!-- end center column -->
	
	<!-- right column -->
 	<div class="section r">
 		
 		<?php if ( !empty($newbie_users) ): ?>
 		<div class="item-block-posts" id="lastest_users">
 			<div class="title clearfix">
	 			<h3>Nuevos usuarios</h3>
	 			<span></span>
	 			<i></i>
 			</div>
			<div class="content newbies-list">
				<?php

					foreach( $newbie_users as $newbie )
					{

						echo '
						<div class="item drop-shadow-light">
							<a class="avatar tt-tipsy" href="' . $newbie['href'] . '" title="' . $newbie['username'] . '"><img src="' . $newbie['avatar'] . '" alt="' . $newbie['username'] . '"></a>
						</div>'; 

					}

				?>			
			</div>
		</div>
		<?php endif ?>
		
		<?php if( !empty($popular_tags) ): ?>
		<div class="item-block-posts" id="lastest_users">
 			<div class="title clearfix">
	 			<h3>Tags populares</h3>
	 			<span></span>
	 			<i></i>
 			</div>
			<div class="content cloudtags">
				<?php
					foreach( $popular_tags as $tag )
					{

						echo '<a class="tag" style="font-size: ' . $tag['size'] . 'px;' . ($tag['color'] ? ' color:' . $tag['color'] . ';' : '') . '" href="' . base_url('buscar/' . urlencode($tag['name'])) . '">' . $tag['name'] . '</a>';

					}
				?>
			</div>
		</div>
		<?php endif ?>

		<div class="item-block-posts" id="advertise_home">
 			<div class="title clearfix">
	 			<h3>Publicidad</h3>
	 			<span></span>
	 			<i></i>
 			</div>
			<div class="content">
				<img src="http://placehold.it/210x600/f1f1f1/222222&text=">
			</div>
		</div>
		
	</div>
	<!-- end right column -->
	
</div>
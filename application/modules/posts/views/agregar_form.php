<div class="add_post_form" id="add_post_form">
	<div class="l_section">

		<?php echo form_open('posts/publicar', array('id' => 'add_post_form')) ?>
		<?php if( isset($errors) && !empty($errors) ): ?>
			<div class="section post_errors clearfix">
				<h3 class="section_title">Error al agregar tu post</h3>
				<div class="info line">Ocurrieron los siguientes errores al intentar agregar tu post:</div>
				<ul class="errors_on_submit">
					<?php echo validation_errors('<li>', '</li>'); ?>
					<?php
						foreach( $errors as $error )
							echo '<li>' . $error . '</li>';
					?>
				</ul>
				<br />
				
			</div>
		<?php endif ?>

		<div class="section post_content clearfix">
			
			<h3 class="section_title">
				Contenido del post
			</h3>

			<div class="sub_section">
				<h3>Titulo del post</h3>
				<?php echo form_label('Ingresa el titulo de tu post', 'post_title', array('class' => 'info')) ?>
				<?php 
					echo form_input(array(
					'class' => 'title drop-shadow-light',
					'type' => 'text',
					'name' => 'title',
					'id' => 'post_title',
					'value' => set_value('title'),
					'maxlength' => $this->settings->get('title_max_length', 'posts')
					));
				?>
			</div>
			
			<hr class="dashed" />

			<div class="sub_section post_body">
				<h3>Cuerpo del post</h3>
				<?php echo form_label('El cuerpo de tu post debe ser mayor a 60 caracteres', 'post_body', array('class' => 'info')) ?>
				<?php 
				
				echo form_textarea(array(
					'id' => 'post_body',
					'class' => 'body',
					'name' => 'body',
					'value' => set_value('body')
				));

				?>
			</div>

			<hr class="dashed" />
			
			<div class="sub_section left sources margin">
				<h3>Fuentes del post</h3>
				<?php echo form_label('Ingresa las url de donde obtuviste el contenido de tu post', 'add_source', array('class' => 'info')) ?>
				<div>
					<?php echo form_input(array('value' => 'http://taringa.net', 'placeholder' => 'http://es.wikipedia.org/wiki/Copyleft', 'class' => 'sources text drop-shadow-light', 'id' => 'add_source')) ?>
					<a class="sp-button ok" onclick="post.form.add_source()" placeholder="http://wikipedia.com">Agregar</a>
				</div>
				<ul class="sources_list clearfix">
					<?php
					$submited_sources = !is_array(set_value('sources')) ? array() : set_value('sources');
					$sources_list = array();

					foreach( $submited_sources as $source )
					{
						
						echo '
						<li class="ui_icon clearfix">
							<i class="icon" style="background: url(https://getfavicon.appspot.com/' . $source['url'] . ') no-repeat"></i>
							<a class="text" href="' . $source['url'] . '" title="' . $source['title'] . '" target="_blank">' . $source['title'] . '</a>
						</li>';

						$sources_list[$source['url']] = $source['url'];

					}
					?>
				</ul>
				<?php 

					if( empty($submited_sources) )
						echo form_hidden('sources', 'SOURCES_PRESENCE');
					else
						foreach( $submited_sources as $source )
							echo form_hidden('sources[]', $source['url']);

				?>
			</div>
			<div class="sub_section left own_content">
				<?php echo form_label('Contenido propio', 'own_content', array('class' => 'info', 'style' => 'font-weight:bold')) ?>
				<?php echo form_checkbox(array('onclick' => 'post.form.toggle_sources()', 'name' => 'own_content', 'checked' => ((bool)set_checkbox('own_content', 1)), 'id' => 'own_content', 'value' => 1)) ?>
				<div>La información del post es de mi autoría, no utilicé contenido de terceros.</div>

			</div>

			<hr class="dashed" />
			
			<div class="sub_section tags left margin">
				<h3>Tags del post</h3>				
				<?php 

				echo form_label('Ingresa una lista separada por comas', 'post_tags', array('class' => 'info'));
				
				echo form_textarea(array(
					'id' => 'post_tags',
					'class' => 'rounded drop-shadow-light',
					'name' => 'tags',
					'placeholder' => 'Ejemplo: Spirate, descargas, ' . date('Y') . ', minecraft',
					'value' => set_value('tags')
				));

				?>
			</div>

			<div class="sub_section categories right">
				
				<h3>Categoria del post</h3>
				<?php echo form_label('Selecciona la categoria de tu post', 'post_category', array('class' => 'info')) ?>
				<select id="post_category" name="category" tabindex="5" size="10">
					<option value="-1" selected="selected">Elegir una categoria</option>
				<?php 

					foreach( $categories as $category )
						echo '
					<option class="sprite-categories-' . $category['ID'] . '" value="' . $category['ID'] . '"' . set_select('category', $category['ID']) . '>
						' . $category['name'] . '
					</option>';

				?>
				</select>		
			</div>

		</div>

		<div class="section post_options clearfix">
		
			<h3 class="section_title">
				Opciones del post
			</h3>

			<div class="sub_section post_option comments">
				
				<h3>Comentarios</h3>
				<div class="line info"> si crees que tu post va a ser <strong>polémico</strong> es mejor cerrar los comentarios.</div>
				<?php 
					
					echo '<div class="line">';
					echo form_radio(array(
						'name' => 'allowed_comments',
						'checked' => set_radio('allowed_comments', '1'),
						'id' => 'post_comments_option',
						'class' => 'radio',
						'value' => '1',
						));
					echo form_label('Todos pueden comentar', 'post_comments_option', array('class' => 'info'));
					echo '</div>';

					echo '<div class="line">';
					echo form_radio(array(
						'name' => 'allowed_comments',
						'checked' => set_radio('allowed_comments', '0', TRUE),
						'id' => 'post_comments_option',
						'class' => 'radio',
						'value' => '0'
						));
					echo form_label('Cerrar los comentarios', 'post_comments_option', array('class' => 'info'));
					echo '</div>';
				?>

			</div>

			<div class="sub_section post_option visibility r-item">
				
				<h3>Visibilidad</h3>
				<div class="line info"> Para quien será visible este post?. selecciona el rango de los usuarios que veran este post.</div>
				<?php

					echo form_checkbox(array(
						'name' => 'all_groups',
						'checked' => set_checkbox('all_groups', 1, TRUE) ? true : false,
						'value' => 1,
						'id' => 'all_groups',
						'class' => 'checkbox',
						'onclick' => 'post.form.toggle_post_visibility()'
						))
				?>
				<?php echo form_label('Todos pueden ver tu post', 'all_groups', array('class' => 'info')) ?>
				<select name="allowed_groups[]" id="allowed_groups" multiple="multiple" tabindex="5" size="5">
				<?php 
					
					foreach( $users_groups as $id => $group )
					echo '
						<option value="' . $id . '"' . ( isset($postdata) && in_array($id, $postdata['allowed_groups']) ? ' selected="selected"' : '' ) .'>' . $group . '</option>';

				?>
				</select>
			</div>

			<div class="sub_section post_option sticky">
				
				<h3>Post fijo</h3>
				<div class="line info">Deseas este post en la secci&oacute;n stickys?</div>
				<?php 
					
					echo '<div class="line">';
					echo form_checkbox(array('name' => 'sticky', 'checked' => set_checkbox('sticky', '1') ? true : false, 'id' => 'post_sticky', 'value' => '1', 'class' => 'checkbox'));
					echo form_label('Agregar a sticky', 'post_sticky', array('class' => 'info'));
					echo '</div>';
				
				?>

			</div>
			
		</div>

		<div class="section post_submit clearfix">
			<h3 class="section_title left" style="margin-bottom:0!important;">Publicar Post</h3>
			<div class="right">
				<a class="sp-button green" id="submit_post">Publicar</a>
				<a class="sp-button" id="preview_post">Previsualizar</a>
			</div>
		</div>

		<?php echo form_close() ?>

	</div>
	<div class="r_section">

		<div class="mini_protocol">

			<h3>Consejos para tu post</h3>

			<h3>El t&iacute;tulo...</h3>
			<ul>
				<li>Debe ser descriptivo.</li>
				<li>No debe estar en MAYUSCULAS o parcialmente en mayusculas.</li>
				<li>No debe ser EXAGERADO!!!</li>
			</ul>

			<h3>El contenido no puede contener...</h3>
			<ul>
				<li>Información personal o de terceros.</li>
				<li>Fotos de personas menores de edad.</li>
				<li>Muertos, sangre, vómitos, etc.</li>
				<li>Con contenido racista y/o peyorativo.</li>
				<li>Poca calidad (una imagen, texto pobre).</li>
				<li>Haciendo preguntas o críticas.</li>
				<li>Con intención de armar polémica.</li>
				<li>Apología de delito.</li>
				<li>Software spyware, malware, virus o troyanos.</li>
			</ul>

		</div>

	</div>
</div>
<script type="text/javascript">
<?php
	echo "
		post.form.settings = array_merge(post.form.settings, {
			'title_length' : " . $this->settings->get('title_min_length', 'posts') . ",
			'body_length'  : " . $this->settings->get('body_min_length', 'posts') . ",
			'tags_length'  : " . 3 . "
		});\n"
?>
<?php if( !empty($sources_list) ): ?>
	post.form.sources = <?php echo json_encode($sources_list) ?>;
<?php endif ?>
	post.form.smileys = <?php echo json_encode($smileys) ?>;
</script>
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
	
class Posts_model extends  MY_Model{
	
	public $limit_home_posts = false;
	public $fields_postdata = array();
	static $error_post_form = false;
	private $pagination_configs;
	private $filter_posts = array();
	private $total_posts = 0;
	private $_cache_categories = array();
	private $_config = array();

	public function __construct(){

		// load date helper
		$this->load->helper('date');

		// load default settings
		$this->load->config('posts/config');
		$this->_config = $this->config->item('posts');

	}
	
	public function get_categories(){
		
		$result = $this->db
		->select('ID, slug, name')
		->from('categories');
		
		if( $this->current_user )
			$result->where_in('allowed_groups', $this->current_user['group']);
		
		$result = $result->get();
		
		$categories = $result->result_array();
		$result->free_result();
		
		return $categories;
	}
	
	public function category_by_slug($slug){
		
		if( empty($slug) )
			return false;

		if( isset($this->_cache_categories[$slug]) )
			return $this->_cache_categories[$slug];
		
		$categories = $this->db
		->select('ID, slug, name')
		->where('slug', $slug)
		->where_in('allowed_groups', 1)
		->limit(1)
		->get('categories');
		
		if( $categories->num_rows() == 0 )
			return false;
		
		$data = $categories->result_array();
		$categories->free_result();
		$this->_cache_categories[$slug] = $data[0];
		
		return $data[0];
	}

	public function parse_msg($msg){

		// cargamos la libreria bbcode
		$this->load->library('bbcode_parser');

		// parse bbcode
		$msg = $this->bbcode_parser->parse_bbcode($msg);

		// set bad chars
		$msg = strtr($msg, array(
			'&amp;' => '&'
		));
		
		// devolvemos el texto formateado
		return $msg;

	}

	public function get_post_from_nav($post_id, $direction = ''){

		$condition = array(
			'siguiente' => ' posts.ID > ',
			'aleatorio' => 'posts.ID != ',
			'anterior' => 'posts.ID < '
		);

		$condition = $condition[$direction];

		$result = $this->db->select('posts.ID, posts.title, categories.slug')
		->from('posts AS posts')
		->join('categories as categories', 'categories.ID = posts.category')
		->where($condition, $post_id, FALSE);

		if( $direction == 'aleatorio' )
			$result->order_by('RAND()');
		else
			$result->order_by('posts.ID', ($direction == 'siguiente' ? 'ASC' : 'DESC'));
		
		$result->limit(1);
		$result = $result->get();

		$found = $result->num_rows() > 0;
		$post = $result->row_array();

		$result->free_result();

		if( !$found )
			return FALSE;

		return $this->_set_posts_href($post['ID'], $post['title'], $post['slug']);

	}
	
	public function get_latest_posts(){

		$newbies_group = $this->settings->get('newbies_group', 'posts');
		
		if( empty($this->limit_home_posts) )
			$this->limit_home_posts = $this->_config['limit_home_posts'];

		list($category, $page, $newbies) = func_get_args();
		
		$this->category = $category;
		$this->page = $page;
		$this->on_newbies = $newbies;
		
		if( $category && !empty( $category['ID'] ) )
		{
			
			$this->filter_posts += array('posts.category' => (int) $category['ID']);
			
		}
		
		if( $newbies )
		{
			
			$this->filter_posts += array('users.group' => $newbies_group);
			
		}
		else if( $this->settings->get('newbies_enabled', 'posts') )
			$this->filter_posts += array('users.group !=' => $newbies_group);
		
		$count_posts = $this->count_posts();
		$offset = ($page * $this->limit_home_posts) - $this->limit_home_posts;
		$total_pages = ceil($count_posts/$this->limit_home_posts);
		
		if( $page >= $total_pages ){
			
			if( $this->limit_home_posts > $count_posts )
				$offset = 0;
			else
				$offset = ($count_posts - $this->limit_home_posts);
			
		}
		
		$this->set_pagination(array(
			'total_rows' => $count_posts,
			'per_page' => $this->limit_home_posts
		));
		
		$result = $this->db
		->select('posts.ID AS post_id, posts.title, categories.slug, categories.ID as category_id, posts.sticky')
		->join('categories', 'categories.ID = posts.category')
		->join('users', 'users.ID = posts.user_id')
		->where($this->filter_posts)
		->where('posts.current_status', 1)
		->order_by('posts.ID', 'DESC')
		->limit($this->limit_home_posts, $offset)
		->get('posts');
		
		if( $result->num_rows() == 0 )
		{
			$result->free_result();
			return array();
		}
		
		$data = $result->result_array();
		$result->free_result();
		$last_posts = array();
		
		foreach( $data as $row )
		{
			
			$last_posts[$row['sticky'] ? 'sticky' : 'normal'][] = array(
				'id' => $row['post_id'],
				'title' => $row['title'],
				'href' => $this->_set_posts_href($row['post_id'], $row['title'], $row['slug']),
				'category' => array(
					'id' => $row['category_id'],
					'slug' => $row['slug']
				)
			);
			
		}

		if( isset($last_posts['sticky']) )
			$last_posts['sticky'] = array_reverse($last_posts['sticky']);
		
		return $last_posts;
	}
	
	public function set_pagination($data = array()){
		
		$defaults = array(
			'use_page_numbers' => TRUE,
			'page_query_string' => FALSE,
			'first_url' => 1
		);
		
		$configs = array();
		
		foreach( $defaults as $conf => $value )
			if( !isset($data[$conf]) )
				$data[$conf] = $value;
		
		foreach( $data as $conf => $value )
		{
			
			$this->pagination_configs[$conf] = $value;
			
		}
		
		return $this;
	}
	
	public function make_pagination(){
		
		// initialize the library
		$this->load->library('pagination');
		
		// initialize pagination
		$this->pagination->initialize($this->pagination_configs);
		
		// output links
		return $this->pagination->create_links();
		
	}
	
	public function count_posts(){
		
		$result = $this->db
		->join('users', 'users.ID = posts.user_id')
		->where($this->filter_posts)
		->from('posts');
		
		$count = $this->db->count_all_results();
		
		return $count;		
	}

	public function get_last_comments($configs = array(), $home = true){

		// set default settings
		$default_settings = array(
			
			'home' => array(
				'comment_columns' => array('ID' => 'comment_id', 'user_id', 'post_id', 'time'), // table posts_comments
				'post_columns' => array('title' => 'post_title'),
				'category_columns' => array('slug' => 'category_slug'),
				'user_columns' => array('username'),
				'limit' => $this->settings->get('limit_home_comments', 'posts') ? $this->settings->get('limit_home_comments', 'posts') : $this->_config['limit_home_comments'],
				'order' => 'posts_comments.ID, DESC'
			),

			'post' => array(
				'comment_columns' => array('ID', 'user_id', 'parent', 'body', 'time'),
				'user_columns' => array('username', 'avatar_id'),
				'limit' => $this->settings->get('limit_post_comments'),
				'offset' => 0,
				'order' => 'posts_comments.ID, ASC'
			),

		);
		
		// join tables
		$join_tables = array(
			'post' => array(
				'users' => array('users.ID = posts_comments.user_id', 'inner'),
				'posts' => array('posts.ID = posts_comments.post_id', 'inner'),
				'categories' =>  array('categories.ID = posts.category', 'left'),
				'avatars' => array('avatars.avatar_id = users.ID')
			),
			'home' => array(
				'users' => array('users.ID = posts_comments.user_id', 'inner'),
				'posts' => array('posts.ID = posts_comments.post_id', 'inner'),
				'categories' =>  array('categories.ID = posts.category', 'left')
			)
		);
		
		$join_tables = $join_tables[$home ? 'home' : 'post'];

		// set correct column names
		$key_columns = array(
			'comment_columns' => 'posts_comments',
			'user_columns' => 'users',
			'post_columns' => 'posts',
			'category_columns' => 'categories'
		);

		// home or post context?
		$default_settings = $default_settings[$home ? 'home' : 'post'];
		$configs = (array) $configs;
		$settings = array();
		$make_settings = array();

		foreach( $default_settings as $setting => $val )
			if( !isset($configs[$setting]) )
				$settings[$setting] = $val;

		$settings += $configs;

		// make settings
		foreach( $settings as $setting => $val ){

			if( in_array($setting, array_keys($key_columns)) )
			{
				$repeated = 0;
				foreach( $val as $col => $row )
				{
					
					if( isset( $make_settings['columns'][$row] ) ){
						$repeated++;
						$row = $row . '_' . $repeated;
					}

					// alias is a string
					if( is_string($col) )
						$make_settings['columns'][$row] = $key_columns[$setting] . '.' . $col . ' AS ' . $row;
					else
						$make_settings['columns'][$row] = $key_columns[$setting] . '.' . $row;

					$order_columns[$row] = $key_columns[$setting] . '.' . $row;

				}

			}

			if( $setting == 'limit' || $setting == 'offset')
				$make_settings[$setting] = (int) $val;

			if( $setting == 'order' )
			{
				
				$order = str_replace(' ', '', explode(',', $val));
				
				$make_settings['order']['by'] = $order[0];;
				$make_settings['order']['dir'] = $order[1];

			}

		}

		$result = $this->db
		->select($make_settings['columns'])
		->limit($make_settings['limit'], isset($make_settings['offset']) ? $make_settings['offset'] : 0)
		->order_by($make_settings['order']['by'], $make_settings['order']['dir']);
		
		foreach( $join_tables as $table => $opts )
			$result->join($table, $opts[0], $opts[1]);
		
		$result = $result->get('posts_comments'); 
		$data = $result->result_array();
		$result->free_result();

		$comments = array();

		foreach( $data as $row )
		{

			
			$comment = array(
				'id' => $row['comment_id'],
				'post' => array(
					'id' => $row['post_id'],
					'title' => $row['post_title'],
					'href' => $this->_set_posts_href($row['post_id'], $row['post_title'], $row['category_slug'])
				),
				'user' => array(
					'id' => $row['user_id'],
					'nick' => $row['username'],
					'href' => semantic_url('/users/' . $row['username'], false)
				)
			);

			if( !$home )
				$comment += array(
					'user' => array(
						'avatar' => $this->users_model->set_avatar()
					)
				);

			$comments[] = $comment;

		}

		return $comments;
		
	}

	public function get_newbie_users( $limit = 10 ){

		$limit = $this->settings->get('limit_newbies_users') ?: $limit;
		$newbies = array();

		$result = $this->db
		->select('users.ID, users.username, users.created_on, avatars.folder, avatars.hash')
		->join('avatars AS avatars', 'avatars.ID = avatar_id', 'left')
		->order_by('ID', 'DESC')
		->get('users AS users');

		$newbies_result = $result->result_array();

		$result->free_result();

		foreach( $newbies_result as $user )
		{

			$newbies[] = array(
				'id' => $user['ID'],
				'username' => $user['username'],
				'href' => base_url('/users' . '/' . $user['username']),
				'time' => $user['created_on'],
				'avatar' => $this->users_model->make_avatars_src($user['folder'], $user['hash'], 'small')
			);

		}

		return $newbies;

	}

	public function get_popular_tags( $limit = 10 ){

		$popular_tags = array();
		$limit = $this->settings->get('limit_cloudtags', 'posts') ?: $limit;
		
		if ( ($cached_tags = $this->cache->file->get('cloud_tagss')) === FALSE )
		{
			
			$result = $this->db->select('name, count')->order_by('count', 'DESC')->limit(10)->get('tags');
			$tags = $result->result_array();

			$result->free_result();

			foreach( $tags as $tag )
			{

				$popular_tags[] = array(
					'name' => $tag['name'],
					'count' => $tag['count']
				);

			}

			// create cloud tags
			$popular_tags = $this->make_cloud_tags($popular_tags);

			// shuffle!
			//shuffle($popular_tags);

			$this->cache->file->save('cloud_tags', $popular_tags, 1800);

		}
		else
			$popular_tags = $cached_tags;
		
		// output tags
		return $popular_tags;

	}

	public function get_post_tags($post_id = 0){

		$post_id = (int) $post_id;

		// *? 
		if( empty($post_id) )
			return array();

		$result = $this->db
		->select('tags.ID, tags.name')
		->join('tags', 'tags.ID = posts_tags.tag_id')
		->where('posts_tags.post_id', $post_id)
		->get('posts_tags');

		$rows = $result->result_array();
		$tags = array();

		foreach( $rows as $row )
		{

			$tags[strlen($row['name'])] = array(
				'id' => $row['ID'],
				'name' => $row['name'],
				'href' => base_url('/buscar/' . urlencode($row['name']))
			);

		}

		krsort($tags);

		$result->free_result();

		return $tags;

	}

	public function get_related_posts($post_id = 0, $by_title = NULL){

		$post_id = (int) $post_id;
		$limit_posts = (int) $this->settings->get('limit_related_posts', 'posts');
		$limit_posts = empty($limit_posts) ? 15 : $limit_posts;

		if( empty($post_id) )
			return array();

		$result = $this->db
					   ->select('tags.ID')
					   ->join('tags', 'tags.ID = posts_tags.tag_id')
					   ->where('posts_tags.post_id', $post_id)
					   ->limit($limit_posts)
					   ->get('posts_tags');

		$tags_result = $result->result_array();
		$result->free_result();

		$rows = array();

		if( !empty($tags_result) )
		{
			foreach( $tags_result as $tag )
				$tags_post[] = $tag['ID'];

			$result = $this->db
						   ->select('posts.ID AS post_id, posts.title, posts.category AS category_id, categories.slug AS category_slug')
						   ->select('COUNT(posts_tags.tag_id) as relevance')
						   ->from('posts_tags AS posts_tags')
						   ->join('posts', 'posts.ID = posts_tags.post_id')
						   ->join('categories', 'posts.category = categories.ID')
						   ->where_in('posts_tags.tag_id', $tags_post)
						   ->where('posts_tags.post_id !=', $post_id)
						   ->group_by('posts_tags.post_id')
						   ->order_by('relevance', 'DESC')
						   ->order_by('RAND()')
						   ->limit($limit_posts)
						   ->get();

			$rows = $result->result_array();
			$result->free_result();
		}

		if( empty($rows) )
		{

			if( !$by_title || empty($by_title) )
			{
				$result = $this->db->select('posts.title')->where('posts.ID', $post_id)->limit(1)->get('posts');
			
				$title = $result->row('title');
				$result->free_result();
			}
			else
				$title = $by_title;

			$result = $this->db
						   ->select('posts.ID AS post_id, posts.title, posts.category AS category_id, categories.slug AS category_slug')
						   ->select(sprintf('MATCH (posts.title) AGAINST ("%s") AS score', $title), FALSE)
						   ->from('posts as posts')
						   ->join('categories', 'categories.ID = posts.category')
						   ->where(sprintf('MATCH (posts.title) AGAINST ("%s")', $title), NULL, FALSE)
						   ->where('posts.ID !=', $post_id, FALSE)
						   ->order_by('score', 'DESC')
						   ->limit($limit_posts)
						   ->get();

			$rows = $result->result_array();
			$result->free_result();

		}

		$related_post = array();

		foreach( $rows as $row )
			$related_post[] = array(
				'id' => $row['post_id'],
				'title' => $row['title'],
				'href' => $this->_set_posts_href($row['post_id'], $row['title'], $row['category_slug']),
				'category' => array(
					'id' => $row['category_id'],
					'slug' => $row['category_slug']
				)
			);

		return $related_post;

	}

	public function make_cloud_tags($tags, $min_size = 11, $max_size = 25, $color = NULL){

		// cargamos la libreria colors
		$this->load->library('colors');

		// definimos las frecuencias de todos los tags
		foreach( $tags as $tag )
			$frequencies[$tag['name']] = $tag['count'];

		// la minima frecuencia entre todos los tags
		$min_frequency = min(array_values($frequencies));

		// la maxima frecuencia entre todos los tags
		$max_frequency = max(array_values($frequencies));
		
		// color base de los tags
		$color = $this->settings->get('color_cloudtags', 'posts') ? $this->settings->get('color_cloudtags', 'posts') : $color;

		if( $color && !empty($color) && preg_match('~^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$~', $color) )
		{
			$color = $this->colors->set($color);
			$max_dark_amount = $color->isDark() ? 15 : 30;
		}
		else
			$color = FALSE;

		// agregamos el tamanio y color a cada tag
		foreach( $tags as $key => $tag )
		{

			// calculamos el tamanio del tag
			$font_size = floor(($max_size * ($tag['count'] - $min_frequency)) / ($max_frequency - $min_frequency));

			// si el tamanio del tag es menor a min_size lo dejamos por defecto
			if( $font_size < $min_size )
				$font_size = $min_size;

			// agregamos el tamanio al array
			$tags[$key]['size'] = $font_size;

			if( $color )
			{
				// definimos el porcentaje de obscurecimiento
				$dark_amount = floor(($max_dark_amount * $tag['count'] - 1) / $max_frequency - 1);

				// definimos el nuevo color en hexadecimal
				$newcolor = '#' . $color->darken($dark_amount);

			}
			else
				$newcolor = FALSE;

			// agregamos el color a la matriz
			$tags[$key]['color'] = $newcolor;

		}

		// ordenamos los tags
		uasort($tags, create_function(
			'$a, $b',
			'
				if ( abs( $a["count"] - $b["count"] ) > 2 )
					return -1;

				return ($a["count"] == $b["count"]) ? 0 : 1;
			'
		));

		// devolvemos los tags modificados
		return $tags;

	}

	public function get_post_data($post_id = NULL){

		if( empty($post_id) )
			return array();

		$check_post = $this->db->select('ID, current_status')->where('ID', $post_id)->get('posts');
		$post_exists = $check_post->num_rows() > 0;
		$post_status = $check_post->row('current_status');
		$check_post->free_result();

		$check_post->free_result();
		
		if( !$post_exists )
			show_error($this->lang->line('post_not_exists'));

		if( $post_status == 0 )
			show_error($this->lang->line('post_in_moderation'));
		
		$current_group_id = isset($this->current_user['group']) ? $this->current_user['group'] : 0;

		$result = $this->db
		->select('posts.ID, posts.title, posts.msg, posts.category, posts.created_time, posts.allowed_comments, posts.own_content')
		->select('posts.modified_time, posts.modified_reason, posts_stats.votes, posts_stats.points, posts_stats.bookmarks')
		->select('posts_stats.comments, posts_stats.followers, posts_stats.visits, users.ID AS user_id, users.username, users.avatar_id')
		->select('categories.slug AS category_slug, categories.name as category_name')
		->select('groups.ID AS group_id, groups.name as group_name, groups.color AS group_color')
		->select('users_stats.posts AS user_posts, users_stats.followers as user_followers, users_stats.points as user_points')
		->from('posts as posts')
		->join('categories', 'categories.ID = posts.category')
		->join('posts_stats', 'posts_stats.post_id = posts.ID')
		->join('users', 'users.ID = posts.user_id')
		->join('groups', 'groups.ID = posts.user_id')
		->join('users_stats', 'users_stats.user_id = users.ID')
		->where('posts.ID', $post_id)
		->where(sprintf('FIND_IN_SET(%s, posts.allowed_groups)', $current_group_id), NULL, FALSE)
		->limit(1)
		->get();

		$rows = $result->row_array();
		$num_rows = $result->num_rows();
		$result->free_result();

		if( empty($num_rows) )
			return array();

		$post_data = array(
			'post' => array(
				'id' => $rows['ID'],
				'title' => $rows['title'],
				'msg' => $this->parse_msg($rows['msg']),
				'raw_msg' => $rows['msg'],
				'href' => $this->_set_posts_href($rows['ID'], $rows['title'], $rows['category_slug']),
				'time' => array(
					'timestamp' => $rows['created_time'],
					'date' => mdate('%d %m %Y - %g:%i:%s')
				),
				'allow' => array(
					'comments' => (bool) $rows['allowed_comments']
				),
				'own_content' => (bool) $rows['own_content'],
				'modified' => array(
					'time' => $rows['modified_time'],
					'reason' => $rows['modified_reason']
				),
				'stats' => array(
					'points' => $rows['points'],
					'bookmarks' => $rows['bookmarks'],
					'comments' => $rows['comments'],
					'followers' => $rows['followers'],
					'visits' => $rows['visits'],
					'votes' => $rows['votes'],
					'score' => $this->_format_score($rows['points'],  $rows['votes'])
				)
			),
			'author' => array(
				'id' => $rows['user_id'],
				'name' => ucwords($rows['username']),
				'avatar' => $this->users_model->get_user_avatars($rows['user_id'], 'large'),
				'href' => base_url('/users/' . $rows['username']),
				'group' => array(
					'id' => $rows['group_id'],
					'name' => ucwords($rows['group_name']),
					'color' => $rows['group_color']
				),
				'stats' => array(
					'posts' => $rows['user_posts'],
					'followers' => $rows['user_followers'],
					'points' => $rows['user_points']
				),
				'is_own' => $rows['user_id'] == $this->current_user['ID']
			)
		);

		return $post_data;


	}

	public function prepare_post_ogdescription($msg){

		// replace break lines and tabs
		$msg = preg_replace('~\n|\r|\t~', ' ', $msg);

		// replace bbcode
		$msg = preg_replace(array(
			'~\[youtube\]([^\[]+)\[\/youtube\]~',
			'~\[[^\]]+\]([^\[]+)\[\/[^\]]+\]~',
			'~\[\/?[^\]]+\]~'
		), array(
			'http://www.youtube.com/watch?v=$1',
			'$1',
			''
		), $msg);

		// set character limit
		$msg = character_limiter($msg, 250);

		// encode description
		$msg = htmlspecialchars($msg, ENT_QUOTES, 'UTF-8');

		// fix amperstands
		$msg = str_replace('&amp;', '&', $msg);

		return $msg;

	}

	public function update_post_stats($stats = array(), $post_id = 0){

		$available_stats = array(
			'points',
			'bookmarks',
			'comments',
			'followers',
			'visits'
		);

		if( empty($post_id) || empty($stats) )
			return FALSE;

		$update_rows = array();

		foreach( $stats as $stat => $action )
		{

			if( !in_array($stat, $available_stats) )
				continue;

			if( !is_int($action) && !in_array($action, array('+', '-')) )
				continue;

			$action = $action == '+' ? $stat . ' + 1' : ($action == '-' ? $stat . ' - 1 ' : $stat . ' = ' . $action);

			$update_rows[$stat] = $action;

		}

		if( empty($update_rows) )
			return FALSE;

		$this->db->set($update_rows, NULL, FALSE)->where('post_id', $post_id)->update('posts_stats');

		return TRUE;
	}

	public function add_new_post( $post_values = array() ){

		if( !$this->ion_auth->logged_in() )
			show_error('Debes ingresar para poder agregar un nuevo post.');

		$this->form_validation->destroy_token();

		// prepare _POST values
		$datatypes = array(
			'title' => 'str',
			'body' => 'str',
			'sources' => 'str',
			'own_content' => 'bool',
			'tags' => 'str',
			'category' => 'category',
			'allowed_comments' => 'bool',
			'sticky' => 'bool',
			'allowed_groups' => 'int'
		);

		foreach( $datatypes as $input => $type )
			if( !isset($post_values[$input]) )
				$inputs[$input] = false;
			else
				$inputs[$input] = $this->sanitize_post_input($post_values[$input], $type);

		$posts_rows = array(
			'user_id' => $this->current_user['id'],
			'title' => $inputs['title'],
			'msg' => $inputs['body'],
			'category' => $inputs['category'],
			'allowed_comments' => $inputs['allowed_comments'] ? 1 : 0,
			'allowed_groups' => '0,1,2,3,4,5',
			'sticky' => $inputs['sticky'] ? 1 : 0,
			'own_content' => !$inputs['own_content'] ? 1 : 0,
			'created_time' => time()
		);
		
		// insertamos el nuevo post
		$this->db->insert('posts', $posts_rows);

		// obtenemos el id del post insertado
		$post_id = $this->db->insert_id();

		// insertamos las tags del post y las anexamos a este
		$this->insert_tags($inputs['tags'], $post_id);

		// insertamos las estadisticas del post
		$this->db->insert('posts_stats', array('post_id' => $post_id));

		// contamos + 1 post al usuario
		$this->users_model->update_user_stats(array('posts' => '+'));

		// devolvemos del id del post
		return $post_id;
		
	}

	public function insert_tags($tags, $post_id){

		$tags = explode(',', $tags);
		$checked_tags = array();

		if( empty($post_id) || empty($tags) )
			return;		

		foreach( $tags as $tag ){

			// sanitize
			$tag = strtolower(htmlspecialchars(trim($tag), NULL, FALSE));

			// censored words
			if( in_array($tag, $this->settings->get('censored_words', 'posts')) )
				continue;

			$checked_tags[] = $tag;

		}

		// comprobamos tags existentes
		$check_tags = $this->db->select('ID, name')->where_in('name', $checked_tags)->get('tags');
		
		// definimos el total de tags existentes
		$total_existing_tags = $check_tags->num_rows();

		// definimos los identificadores de todos los tags existentes
		$existing_tags = $check_tags->result_array();

		// free result
		$check_tags->free_result();

		// agregamos los tags existentes en un array para actualizarlos posteriormente
		$update_tags = array();

		foreach( $existing_tags as $key => $value )
		{
			$update_tags[$value['ID']] = $value['name'];
		}

		// sumamos las veces que fueron ingresados los tags existentes
		if( $total_existing_tags > 0 )
		{

			$this->db->set('count', 'count+1', FALSE)->where_in('name', array_values($update_tags))->update('tags');

		}

		// definimos la variable para guardar el nombre de los tags
		$nametags = array();

		// agregamos los nuevos tags en un array
		$insert_tags = array();

		foreach( $checked_tags as $tag ){

			if( !in_array($tag, array_values($update_tags)))
			{
				$insert_tags[] = array($tag, 1, time());

				// guardamos el nombre del tag en el array
				$nametags[] = $tag;
			}

		}
		
		if( empty($insert_tags) && empty($update_tags) )
			return;

		$insert_post_tags = array();

		if( !empty($insert_tags) )
		{
			
			// insertamos los nuevos tags en la base de datos
			$this->db_insert_rows('tags', array('name', 'count', 'added_time'), $insert_tags);

			// anexamos los tags al post
			$tags_query = $this->db->select('ID')->where_in('name', $nametags)->get('tags');
			$tags_result = $tags_query->result_array();
			$tags_query->free_result();

			foreach( $tags_result as $row )
			{

				$insert_post_tags[] = array($post_id, $row['ID']);

			}

		}

		// anexamos tambien las tags existentes.
		if( !empty($update_tags) )
			foreach( $update_tags as $tagid => $dummy )
				$insert_post_tags[] = array( $post_id, $tagid);


		$this->db_insert_rows('posts_tags', array('post_id', 'tag_id'), $insert_post_tags);

	}

	public function validate_post_form($value, $input){

		switch( $input )
		{

			case 'sources':

				if( $this->input->post('own_content') )
					return TRUE;
				
				$sources = (array) $this->input->post('sources');

				array_walk($sources, 'trim');

				foreach( $sources as $key => $source )
				{

					if( !preg_match('~(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?~i', $source) ){
						unset($sources[$key]);
						continue;
					}

					$sources[$key] = preg_replace('~#[^$]*$~', '', $source);

				}

				$this->fields_postdata['sources'] = array();

				if( empty($sources) )
					return $this->_set_error_post_form($this->lang->line('error_empty_sources'), $input);

				$sources_check = $this->db->select('url, title')->where_in('url', $sources)->where('valid != ', 0)->get('url_sources');
				$unchecked_sources = $sources_check->result_array();
				$total_sources = $sources_check->num_rows();
				$sources_check->free_result();

				$this->fields_postdata['sources'] = $unchecked_sources;

				if( $total_sources == 0 )
					return $this->_set_error_post_form($this->lang->line('error_unchecked_sources'), $input);
				
				return TRUE;

			break;

			case 'tags':

				$tags = explode(',', $value);
				$deleted_tags = array();

				// check each url before
				foreach( $tags as $key => $tag )
				{
					$tags[$key] = $tag = strtolower(htmlspecialchars(trim($tag), NULL, FALSE));
					
					if( in_array($tag, $this->settings->get('censored_words', 'posts')) ){
						
						$deleted_tags[] = $tags[$key];
						unset($tags[$key]);

					}

				}

				$count_tags = count(array_unique($tags));

				$this->fields_postdata['tags'] = implode(', ', array_unique($tags));

				if( $count_tags < count($tags) )
					return $this->_set_error_post_form(sprintf($this->lang->line('error_distinct_tags'), $this->settings->get('tags_min_length', 'posts')), $input);

				// check tags min length
				if( $count_tags < $this->settings->get('tags_min_length', 'posts') )
				{

					if( count($deleted_tags) > 0 )
						return $this->_set_error_post_form(sprintf(count($deleted_tags) > 1 ? 
								$this->lang->line('error_tags_not_allowed') : 
								$this->lang->line('error_tag_not_allowed'), implode(', ', $deleted_tags)), $input);
					else
						return $this->_set_error_post_form(sprintf($this->lang->line('error_tags_min_length'), $this->settings->get('tags_min_length', 'posts')), $input);

				}
					
				if( $count_tags >  $this->settings->get('tags_max_length', 'posts'))
					return $this->_set_error_post_form(sprintf($this->lang->line('error_tags_max_length'), $this->settings->get('tags_min_length', 'posts')), $input);

				return TRUE;

			break;

			case 'category':

				$value = (int) $value;

				if( empty($value) )
					return $this->_set_error_post_form($this->lang->line('error_empty_category'), $input);

				$category = $this->db->select('ID')->where('ID = ', $value)->get('categories');

				$exists = $category->num_rows() > 0;

				if( !$exists )
					return $this->_set_error_post_form($this->lang->line('error_incorrect_category'), $input);

				return TRUE;
			break;

			case 'visibility':

				if( $this->input->post('all_groups') )
					return TRUE;

				$allowed_groups = (array) $this->input->post('allowed_groups');

				array_walk($allowed_groups, 'intval');

				$this->fields_postdata['allowed_groups'] = array();

				if( empty($allowed_groups) )
					return $this->_set_error_post_form($this->lang->line('error_allowed_groups'), 'allowed_groups');

				$groups = $this->ion_auth->groups()->result_array();
				$id_groups = array();

				foreach( $groups as $group )
					$id_groups[] = $group['id'];

				foreach( $allowed_groups as $key => $group )
				{

					if( !in_array($group, $id_groups) )
						unset($allowed_groups[$key]);

				}

				$this->fields_postdata['allowed_groups'] = $allowed_groups;

				if( empty($allowed_groups) )
					return $this->_set_error_post_form($this->lang->line('error_allowed_groups'), 'allowed_groups');

				return TRUE;

			break;


		}

		
		return TRUE;
	}

	private function sanitize_post_input($value, $type = 'detect'){

		if( is_array($value) ){

			foreach($value as $k => $v)
				$value[$k] = $this->sanitize_post_input($v, $type);

			return;

		}

		if ( $type == 'detect' )
		{

			if( gettype($value) == 'integer' || preg_match('~[0-9]{1,}~', $value) )
				$type = 'int';

			else if ( gettype($value) == 'string' )
				$type = 'str';

			else if ( gettype($value) == 'bool' )
				$type = 'bool';

		}

		if( $type == 'str' )
			$value = htmlspecialchars($value, NULL, FALSE);

		if( $type == 'int' )
			$value = (int) $value;

		if( $type == 'bool' )
			$type = (bool) $value;

		return $value;

	}

	private function _set_error_post_form($message, $input){
		
		$this->form_validation->set_message('validate_post_form', $message, $input);

		return FALSE;
	}

	private function _set_posts_href($args = null){

		list($id, $title, $slug) = is_array($args) && count($args) >= 3 ? $args : func_get_args();

		if( !isset($id, $title, $slug) )
			return base_url('/posts');

		return semantic_url('posts/' . $slug . '/' . $id . '/' . $title);

	}

	private function _format_score($points = 0, $times = 1){

			if( empty($points) )
				return FALSE;

			$score = ($points/$times);
			$format = is_float($score) ? sprintf ("%.1f", $score) : $score;

			return $format;

	}
	
}
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
	
class Smileys_model extends  MY_Model{

	public $current_smileys_url;
	private $_smileys_url;
	private $_smileys_collection;
	private $_cache = FALSE;

	public function __construct(){

		$this->_smileys_url = $this->settings->get('smileys_url', 'smileys');
		$this->_smileys_collection = $this->settings->get('smileys_collection', 'smileys');
		$this->current_smileys_url = $this->_smileys_url . $this->_smileys_collection . '/';

	}

	public function get_smileys_data(){

		if( !$this->_cache )
		{
			$result = $this->db->select('ID, filename, codes')->get('smileys');
			$rows = $result->result_array();
			$result->free_result();
			$this->_cache = $rows;
		}
		else
			$rows = $this->_cache;

		$smileys = array();

		foreach($rows as $smiley)
		{

			$smileys[] = array(
				'id' => $smiley['ID'],
				'filename' => $smiley['filename'],
				'codes' => (array) json_decode($smiley['codes'])
			);

		}

		return $smileys;

	}

	public function get_smileys_list( $only_src = FALSE, $include_root = TRUE ){

		$smileys = $this->get_smileys_data();
		$images = array();

		foreach( $smileys as $smiley )
		{

			$smiley_path = ($include_root ?  $this->current_smileys_url . $smiley['filename'] : '');

			foreach( $smiley['codes'] as $code )
			{
				
				if( !$only_src )
					$images[$code] = sprintf(
						'<img alt="%s" src="%s" class="smiley" />', $code, $smiley_path);
				else
					$images[$code] = ($include_root ? $this->current_smileys_url : '') . $smiley['filename'];

			}

		}

		return $images;

	}

}
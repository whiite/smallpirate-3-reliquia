<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class posts extends SP_controller {
	
	private $section_newbies = FALSE;
	private $newbies_enabled = FALSE;
	private $category;
	private $posts_page = 1;
	public $data = array();
	
	function __construct(){
		
		parent::__contruct();
		
		// cargamos el lenguaje
		$this->lang->load('posts/posts');
		
		// cargamos los modelos
		$this->load->model('posts/posts_model');
		
		// cargamos ayudantes
		$this->load->helper(array(
			'array',
			'form',
			'number'
		));

		// cargamos assets para este modulo
		$this->template->add_asset('posts.css');
		$this->template->add_asset('posts.js');
		
		// la seccion newbies esta habilitada?
		$this->newbies_enabled = $this->settings->get('newbies_enabled', 'posts');
		
	}
	
	function _remap($method, $params){

		$this->section_newbies = $method == 'newbies' && $this->newbies_enabled;
		
		if( $this->section_newbies )
			$method = 'index';
		
		if( $this->section_newbies && !empty($params) )
			$this->category = $params[0];
		
		// possible category selected
		else if( !method_exists($this, $method) && !$this->section_newbies ){
			
			$params_page = isset($params[0]) ? $params[0] : '';
			
			// bad request, only /category/ || /category/page/
			if( count($params) > 0 && $method != 'page' && $params_page != 'page' )
				return show_404();
			
			// method is a possible category
			$this->category = $method;
			
		}
		
		// category exists
		if( $this->category && ($this->category = $this->posts_model->category_by_slug($this->category)) ){
			
			if( $this->section_newbies ){
				
				if( isset($params[1]) && !empty($params[1]) && $params[1] != 'page' )
					show_404();
				
			}
			
			$method = 'index';
		}
		
		if( $method == 'page' || (in_array('page', $params) && $method == 'index') ){
				
				$page_index = 1;
				
				if( $this->category )
					$page_index = $this->section_newbies ? (isset($params[2]) ? $params[2] : false) : $params[1];
				else
					$page_index = $this->section_newbies ? (isset($params[1]) ? $params[1] : false) : (isset($params[0]) ? $params[0] : false);
				
				$page_index = (int) $page_index;
				
				if( !$page_index || empty($page_index) || $page_index < 1)
					return show_404();
				
				$this->posts_page = $page_index;
				
				return $this->index($params);	
		}
		
		// call method if exists
		if( method_exists($this, $method) )
			return call_user_func_array(array(__CLASS__, $method), $params);
		else
			show_404();
		
	}
	
	function index($params = array()){
			
			$params = (array)$params;

			// show breadcrumb in this method
			$this->show_breadcrumb();
			
			// obtenemos la cantidad de posts configurada
			$this->posts_model->limit_home_posts = (int) $this->settings->get('limit_posts', 'posts');
			
			// set data posts
			$this->data['last_posts'] = $this->posts_model->get_latest_posts($this->category, $this->posts_page, $this->section_newbies);
			
			// creamos la paginacion
			$base_url_paginate = base_url('/posts/page/');
			
			if( $this->category )
			{
				
				if( $this->section_newbies )
					$base_url_paginate = base_url('/posts/newbies/' . $this->category['slug'] . '/page/');
				else
					$base_url_paginate = base_url('/posts/' . $this->category['slug'] . '/page/');
				
			}
			
			else if( $this->section_newbies )
				$base_url_paginate = base_url('/posts/newbies/page/');
			
			$pagination_links = $this->posts_model
			->set_pagination(array('base_url' => $base_url_paginate))
			->make_pagination();
			
			$this->data['pagination'] =  $pagination_links;

			// *? get stats

			// set total posts
			$this->data['total_posts'] = $this->db->select('value')->where('statistic', 'posts')->get('stats')->row('value');

			// obtenemos los usuarios registrados recientemente
			$this->data['newbie_users'] = $this->posts_model->get_newbie_users();

			// get popular tags
			$this->data['popular_tags'] = $this->posts_model->get_popular_tags();
			
			// get last comments
			$this->data['last_comments'] = $this->posts_model->get_last_comments();
			
			/* filters vars */
			$filters_posts = array(
				'current_page' => in_array('page', array_values($params)) ? $this->posts_page : false,
				'on_newbies_section' => ($this->section_newbies),
				'current_category' => !empty($this->category) ? $this->category : false
			);

			// set javascript vars
			$this->template->set_js_var($filters_posts, 'posts', TRUE);

			// set filter posts vars
			$this->data += $filters_posts;

			// set categories list
			$this->data['categories'] = $this->posts_model->get_categories();

			/* set meta keywords */
			$meta_keywords = array();

			foreach( $this->data['popular_tags'] as $tag )
				$meta_keywords[] = $tag['name'];

			$this->template->append_metadata('meta', array('name' => 'keywords', 'content' => implode(', ', $meta_keywords)));
			
			/* display template */
			$this->template->set($this->data)->display('home_blocks');
			
	}
	
	function _navigate($post_id, $direction = ''){

		$post_id = (int) $post_id;

		if( empty($post_id) )
			redirect();

		$url = $this->posts_model->get_post_from_nav($post_id, $direction);

		if( $url === FALSE )
			redirect();

		redirect($url);

	}

	function ver($post_id = 0, $goto = ''){

		$post_id = (int) $post_id;

		if( in_array($goto, array('anterior', 'aleatorio', 'siguiente')) )
			return call_user_func_array(array(__CLASS__, '_navigate'), func_get_args());
		
		if(empty($post_id))
			show_error($this->lang->line('empty_post_id'));

		// load date helper
		$this->load->helper('date');
		
		// get post data
		$post_data = $this->posts_model->get_post_data($post_id);

		// update visits
		$lasts_posts_visited = $this->session->userdata('posts_visited');
		
		if( !is_array($lasts_posts_visited) )
			$lasts_posts_visited = array();

		if( !in_array($post_id, $lasts_posts_visited) )
		{
			
			// add one visit more
			$this->posts_model->update_post_stats(array('visits' => '+'), $post_id);

			// one pirate more reading this! :)
			++$post_data['post']['stats']['visits'];

			// add to list
			$lasts_posts_visited[] = $post_id;
			$this->session->set_userdata('posts_visited', $lasts_posts_visited);

		}

		$remaining_points = FALSE;
		$hidden_points = FALSE;

		if( $this->current_user && !empty($this->current_user['remaining_points']) )
		{

			$remaining_points = range(1, min(10, $this->current_user['remaining_points']));

			if( $this->current_user['remaining_points'] > 10 )
				$hidden_points = range(11, max(11, $this->current_user['remaining_points']));

		}
		else if (!$this->current_user)
			$remaining_points = range(1, 10);

		// get extra data
		$post_data += array(
			'tags' => $this->posts_model->get_post_tags($post_data['post']['id']),
			'related_posts' => $this->posts_model->get_related_posts($post_data['post']['id'], $post_data['post']['title']),
			'spectator' => array(
				'points' => $remaining_points,
				'hidden_points' => $hidden_points
			)
		);

		// show breadcrumb in this method
		$this->show_breadcrumb();

		// set meta keywords
		$meta_keywords = array();

		foreach( $post_data['tags'] as $tag )
			$meta_keywords[] = $tag['name'];

		$this->template->append_metadata('meta', array('name' => 'keywords', 'content' => implode(', ', $meta_keywords)));

		//prepare all data and display template
		$this->template
		->append_metadata(array(
			array('type' => 'og', 'property' => 'type', 'content' => 'article'),
			array('type' => 'og', 'property' => 'url', 'content' => $post_data['post']['href']),
			array('type' => 'og', 'property' => 'title', 'content' => character_limiter($post_data['post']['title'], 50)),
			array('type' => 'og', 'property' => 'image', 'content' => $post_data['author']['avatar']),
			array('type' => 'og', 'property' => 'description', 'content' => $this->posts_model->prepare_post_ogdescription($post_data['post']['raw_msg'])),
			array('type' => 'og', 'property' => 'article:tag', 'content' => implode(', ', $meta_keywords)),
			array('type' => 'og', 'property' => 'article:published_time', 'content' => standard_date('DATE_ISO8601', $post_data['post']['time']['timestamp'])),
			array('type' => 'dc', 'property' => 'date', 'content' => mdate('%Y-%m-%d %g:%i:%s', $post_data['post']['time']['timestamp'])),
			array('type' => 'dc', 'property' => 'creator', 'content' => $post_data['author']['name']),
		));

		if( !empty($post_data['post']['modified']['time']) )
			$this->template->append_metadata('og', array(
				'property' => 'article:modified_time',
				'content' => standard_date('DATE_ISO8601', $post_data['post']['modified']['time'])
			));

		// *? TODO: article:author, article:publisher ( facebook connect integration )

		$this->template
		->set_title(character_limiter($post_data['post']['title'], 50), 1)
		->set($post_data)
		->add_asset('sceditor.js')
		->add_asset('SCEditor_language.js')
		->set_js_var(array(
			'SCEditor_assets_url' => $this->template->theme['default_theme_url'] . 'assets/SCEditor/'
		), 'global')
		->set_js_var(array(
			'comments_limit_chars' => (int) $this->settings->get('comments_limit_chars', 'posts'),
		), 'post', TRUE)
		->display('post_view');
		
	}

	function image (){
		
		header('Content-type: image/jpeg');
		header('Content-Disposition: filename="large.jpg"');
		
		$this->load->library('image_workshop');
		
		$image = $this->image_workshop->initFromUrl('http://i.imgur.com/2r1IRJe.png');
		$image->cropInPixel(400, 400, 495, 223, 'LT');
		$image->resizeInPixel('32', NULL, TRUE, '0', '0', 'MM');

		imagejpeg($image->getResult(), null, 100); // We choose to show a JPEG (quality of 95%)

	}
	
	function agregar(){

		// el usuario esta logeado?
		if( !$this->ion_auth->logged_in() )
			show_error('Debes ingresar para poder agregar un nuevo post');

		// tiene permisos para realizar esta accion?
		if( !$this->permissions->has_permission('add_new_post') )
			show_error($this->lang->line('error_not_have_permissions'));
		
		// cargamos la libreria bbcode
		$this->load->library('bbcode_parser');

		// generamos un token para este formulario
		$this->form_validation->generate_token();

		// incluimos Assets SCEditor
		$this->template
		->add_asset('sceditor.js')
		->add_asset('SCEditor_language.js')
		->set_js_var(array(
			'SCEditor_assets_url' => $this->template->theme['default_theme_url'] . 'assets/SCEditor/'
		), 'global');

		// definimos los smileys para el sceditor
		$this->data['smileys'] = $this->smileys_model->get_smileys_list( TRUE, TRUE );
		$this->data['smileys_root'] = $this->smileys_model->current_smileys_url;

		// show breadcrumb
		$this->show_breadcrumb();

		// set page title
		$this->template->set_title('Agregar un nuevo post', 1);

		// definimos las categorias
		$this->data['categories'] = $this->posts_model->get_categories();

		// definimos los grupos
		$groups = $this->ion_auth->groups()->result_array();

		foreach( $groups as $group )
			$this->data['users_groups'][$group['id']] = $group['name'];

		// set data to template
		$this->template->set($this->data);
		
		// display view
		$this->template->display('agregar_form');
		
	}

	private function publicar(){

		if( !$this->permissions->has_permission('add_new_post') )
			redirect();

		// submit form?
		if ( $this->input->server('REQUEST_METHOD') !== 'POST' )
			redirect('posts/agregar');

		if( !$this->form_validation->validate_token() )
			show_error('Ya has enviado este formulario previamente.');

		// definimos el modelo para los callbacks
		$this->form_validation->set_model('posts_model');
		
		/* definimos las reglas de validacion */
		
		// obtenemos la longitud del titulo configurada
		$title_min_length = $this->settings->get('title_min_length', 'posts');
		$title_max_length = $this->settings->get('title_max_length', 'posts');

		// obtenemos la longitud del cuerpo del post configurada
		$body_min_length = $this->settings->get('body_min_length', 'posts');
		$body_max_length = $this->settings->get('body_max_length', 'posts');

		$rules = array(
			array(
				'field' => 'title',
				'label' => 'titulo del post',
				'rules' => sprintf('trim|required|min_length[%d]|max_length[%d]', $title_min_length, $title_max_length)
			),
			array(
				'field' => 'body',
				'label' => 'cuerpo del post',
				'rules' => sprintf('trim|required|min_length[%d]|max_length[%d]', $body_min_length, $body_max_length)
			),
			array(
				'field' => 'sources',
				'label' => 'fuentes del post',
				'rules' => 'callback_validate_post_form[sources]'
			),
			array(
				'field' => 'own_content',
				'label' => 'contenido propio',
				'rules' => ''
			),
			array(
				'field' => 'tags',
				'label' => 'tags del post',
				'rules' => 'required|callback_validate_post_form[tags]'
			),
			array(
				'field' => 'category',
				'label' => 'categoria del post',
				'rules' => 'required|callback_validate_post_form[category]'
			),
			array(
				'field' => 'all_groups',
				'label' => 'Todos pueden ver tu post',
				'rules' => ''
			),
			array(
				'field' => 'allowed_groups',
				'label' => 'visibilidad del post',
				'rules' => 'callback_validate_post_form[visibility]'
			),
			array(
				'field' => 'allowed_comments',
				'label' => 'comentarios',
				'rules' => ''
			),
			array(
				'field' => 'sticky',
				'label' => 'sticky',
				'rules' => ''
			)
		);
		
		$this->form_validation->set_rules($rules);

		// agregamos el post a la base de datos si el post fue validado
		if ($this->form_validation->run())
		{

			$postdata = $this->input->post();

			foreach( $this->posts_model->fields_postdata as $field => $value )
				$postdata[$field] = $value;

			// agregamos el nuevo post
			$post_id = $this->posts_model->add_new_post($postdata);
			
			unset($_POST, $postdata);
			
			// cargamos la plantilla post_added
			return $this->template->set(array('post_id' => $post_id))->display('post_added');

		}
		
		foreach( $this->posts_model->fields_postdata as $field => $data )
			$this->form_validation->set_field_postdata($field, $data);

		$this->data['postdata'] = $this->posts_model->fields_postdata;

		// cargamos el formulario
		$this->agregar();
	}

	public function historial()
	{

		$this->index();

	}
	
	function editar(){
		
		
	}
	
	function eliminar($id){
		
		if( $this->input->is_ajax_request() )
			die(to_json(array('id' => $id)));
		else
			redirect();
		
	}
	
	private function show_breadcrumb(){
		
		// set breadcrumb
		$this->template->set_breadcrumb(array(
			'newbies' => array(
				'text' => 'Newbies',
				'href' => base_url('/posts/newbies'),
				'title' => 'Seccion novatos',
				'show_if' => $this->newbies_enabled
			),
			'agregar' => array(
				'text' => 'Agregar nuevo post',
				'href' => base_url('/posts/agregar'),
				'title' => 'Agregar un nuevo post',
				'show_if' => $this->ion_auth->logged_in(),
				'condition' => $this->method == 'publicar'
			),
			'historial' => array(
				'text' => 'Historial de moderaci&oacute;n',
				'href' => base_url('/posts/historial'),
				'title' => 'Historial de moderacion'
			),
			'destacados' => array(
				'text' => 'Posts destacados',
				'href' => base_url('/posts/destacados'),
				'title' => 'Posts destacados'
			)
		));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
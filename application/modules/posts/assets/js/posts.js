var 
posts = posts || {},
post = post || {},
comments = comments || {};

posts = {
	
	init : function(){

		// filter by [newbies, category, page]
		$('#categories').change(function(){
			
			posts.filter_posts(this);
			
		});

	},
	
	filter_posts : function(category){
		
		category = ($(category)
				.find('option:selected').attr('slug') ? $(category).find('option:selected').attr('slug') :
				false);
		category = category ? '/' + category : '';
		
		var location = global.base_url + sprintf('posts%s%s', ( posts.on_newbies_section ? '/newbies' : '' ), category);
		
		if( posts.current_page  )
			location += '/page/' + posts.current_page;
		
		return document.location = location;
	},
	
};

post = {
	
	init : function(){

		if( $("textarea#comment_textarea").length > 0 )
		{
			
			$("textarea#comment_textarea").sceditor({
				plugins: "bbcode",
				style: global.SCEditor_assets_url + "jquery.sceditor.default.min.css",
				locale: 'es',
				width: 'auto',
				height: 200,
				resizeWidth: false,
				resizeHeight: false,
				toolbar: "bold,italic,underline,strike|left,center,right,justify|code,link,image,quote|maximize",
			}).sceditor('instance').toggleSourceMode();

			// count and limit characters
			$('#comments_left_chars').appendTo('.sceditor-container');

			var skip_tags = ['b','i','u','s','left','center','right','justify','code','img','quote', 'url'];
			var regexp_tags = new RegExp("\\\[\\/?(" + implode('|', skip_tags) + ")[^\\\]]*\\\]", 'gi');
			
			window.setInterval(function(){

				var instance = $('textarea#comment_textarea').sceditor('instance'),
		        	value = $('textarea#comment_textarea').data("sceditor").val(),
		        	length = value.replace(regexp_tags, '').length,
		        	limit = post.comments_limit_chars,
		        	left_chars = (limit - length) < 0 ? 0 : limit - length;

		   		$('#comments_left_chars span').html(left_chars);

		   		if(length > limit)
	    			instance.val(value.substring(0, limit));

        	}, 500);

		}

		post.form.init();


	},

	form : {
		sources : {},
		own_content : false,

		init : function(){

			// set SCEditor
			$("textarea#post_body").val('').sceditor({
				plugins: "bbcode",
				style: global.SCEditor_assets_url + "jquery.sceditor.default.min.css",
				locale: 'es',
				width: 600,
				height: 400,
				resizeWidth: false,
				emoticons : {
					hidden : post.form.smileys
				},
				toolbar: "bold,italic,underline,strike|left,center,right,justify|font,size,color,removeformat|code,bulletlist,horizontalrule|youtube,image,link,quote|maximize,source",
			});

			$('a.sceditor-button').tipsy({'gravity' : 's'});

			// check post visibility option
			this.toggle_post_visibility();

			// check sources option
			this.toggle_sources();

			// vista previa del post
			$('#preview_post').click(function(){
				post.form.preview();
			});

			// enviar el formulario
			$('#submit_post').click(function(){
				post.form.submit();
			});

		},

		submit : function(){

			if( !this.check_all_inputs() )
				return false;
			
			$('form#add_post_form').submit();

		},

		preview : function(){

			if( this.check_all_inputs() )
			{
				sp.modal.set({
					'title' : 'Vista previa',
					'height' : 300,
					'position' : 'fixed',
					'buttons' : [
						{
							'label' : 'Publicar',
							'class' : 'green',
							'callback' : function(){
								post.form.submit();
							}
						},
						{
							'label' : 'Cerrar',
							'class' : 'clear',
							'callback' : 'close'
						}
					]
				}).load();

				$.scrollTo(0);

				$.ajax({
					'type' : 'POST',
					'url' : global.base_url + 'ajax/preview_post',
					'dataType' : 'json',
					'data' : 'title=' + encodeURIComponent($('form#add_post_form input#post_title').val()) + '&body=' + encodeURIComponent($('form#add_post_form #post_body').sceditor('instance').val()),
					'success' : function(data){

						sp.modal
						.stop_load()
						.set_width(800, false, true)
						.set_body(data.body);

					}
				})

			}

		},

		check_all_inputs : function(){

			var valid = true,
				first_issue_input = false;

			sp.tooltip.destroy('all');

			$('.add_post_form :input').each(function(){

				if( post.form.check_input(this) === false ){
					
					if( !first_issue_input )
						first_issue_input = this;

					valid = false;

				}

			});

			if( first_issue_input )
			{

				$.scrollTo($(this.tooltips_positions[$(first_issue_input).attr('name')][0]).offset().top-70, 500);
				
			}

			return valid;

		},

		check_input : function(input){

			var value = $(input).val(),
				name = $(input).attr('name');
			
			switch( name ){

				case 'title':

					if( empty(value) || $.trim(value) == '' )
						return this.show_error('title', 'Este campo es necesario.');

					if( value.length < this.settings.title_length )
						return this.show_error('title', 'El titulo debe tener mas de ' + (this.settings.title_length-1).toString() + ' caracteres' );

					if( value.match(/[A-Z]{5,}/) )
						return this.show_error('title', 'El titulo no debe tener demasiadas mayusculas.');

					if( value.match(/[\!]{3,}/) )
						return this.show_error('title', 'El titulo no debe ser exagerado.');

					return true;

				break;

				case 'body':

					value = $('#post_body').sceditor('instance').val();

					if( empty(value) || $.trim(value) == '' )
						return this.show_error('body', 'Este campo es necesario.');
					
					if( value.length < this.settings.body_length )
						return this.show_error('body', 'El contenido debe tener mas de ' + (this.settings.title_length-1).toString() + ' caracteres' );

					return true;

				break;

				case 'sources':

					if( !$('input#own_content').is(':checked') )
					{

						if( empty(this.sources) )
							return this.show_error('sources', 'Ingresa las fuentes de tu post');

					}
					else
						return true;

				break;

				case 'tags':

					var split_tags = value.split(','),
						unique_tags = new Array(),
						count = 0,
						tags = new Array();

					if( empty(value) )
						return this.show_error('tags', 'Este campo es necesario.');

					for ( var tag in split_tags ){

						if( (split_tags[tag].replace(/(\s|\t|\r|\n)*/, '')).length == 0 )
							continue;

						tags.push($.trim(split_tags[tag].toLowerCase()));

					}

					count = $.unique($(tags)).length;

					console.log(count, tags.length);
					
					if( count != tags.length )
						return this.show_error('tags', sprintf('Debes ingresar mas de %d tags distintos.', this.settings.tags_length));

					if( count < this.settings.tags_length )
						return this.show_error('tags', sprintf('Debes ingresar mas de %d tags.', this.settings.tags_length));

					return true;

				break;

				case 'category':

					value = parseInt(value);

					if( empty(value) || value < 0)
						return this.show_error('category', 'Este campo es necesario.');

					return true;

				break;

				case 'allowed_groups[]':
					
					if( !$('#all_groups').is(':checked') && empty($('#allowed_groups').val()) )
						return this.show_error('visibility', 'Debes seleccionar almenos un grupo.');

					return true;

				break;

			}

		},

		tooltips_positions : {
			'title' : ['#post_title', 'north right'],
			'body' : ['.sceditor-container', 'north right'],
			'sources' : ['.sub_section.sources > label', 'north right'],
			'tags' : ['#post_tags', 'south left'],
			'category' : ['#post_category', 'north right'],
			'visibility' : ['select#allowed_groups', 'east top']
		},

		show_error : function(input, error_msg){

			if( $.inArray(input, array_keys(this.tooltips_positions)) === -1 )
				return false;
			
			sp.tooltip.set('error', error_msg, {position : this.tooltips_positions[input][1]}).to(this.tooltips_positions[input][0]).show();

			return false;

		},

		toggle_post_visibility : function(){

			var checkbox = $('.sub_section.post_option input[name=all_groups]');

			if( $(checkbox).is(':checked') )
			{
				$('select#allowed_groups').stop().animate({'opacity' : 0.5}).attr('disabled', 'disabled');
				post.form.own_content = true;
			}
			else{
				$('select#allowed_groups').stop().animate({'opacity' : 1.0}).removeAttr('disabled');
				post.form.own_content = false;
			}

			return post.form.own_content;

		},

		toggle_sources : function(){

			var checkbox = $('.sub_section.own_content input[name=own_content]');

			if( $(checkbox).is(':checked') )
			{
				$('.sub_section.sources').stop().animate({'opacity' : 0.5});
				$('input#add_source').attr('disabled', 'disabled');
				this.own_content = true;
			}
			else{
				$('.sub_section.sources').stop().animate({'opacity' : 1.0});
				$('input#add_source').removeAttr('disabled');
				this.own_content = false;
			}

		},

		add_source : function(){

			if( this.own_content || $('.sub_section.own_content input[name=own_content]').is(':checked') )
				return false;

			var

			source_url = $.trim($('.sub_section.sources input#add_source').val()),
			sources_list = $('.sub_section.sources .sources_list'),
			_tmp = '<li><i class="icon" style="background:url({{favicon_src}}) no-repeat"></i>\
					<a class="text" title="{{title}}" href="{{url}}" target="_blank">{{title}}</a></li>',
			new_item,
			hidden_input = $('<input />').attr('type', 'hidden').attr('name', 'sources[]');

			if( !source_url.match(sp._regexp_utils['url']) )
				return false;

			source_url = source_url.replace(/#[^$]*$/, '');

			if( $.inArray(source_url, array_values(post.form.sources)) !== -1 )
				return false;

			if( $('input[name=sources]') && $('input[name=sources]').val() == 'SOURCES_PRESENCE' )
				$('input[name=sources]').remove();
			
			$('input#add_source').addClass('loading-icon').attr('disabled', 'disabled');
			
			$.ajax({
				type : 'POST',
           		 async: false,
           		 cache: false, 
				data : 'url=' + encodeURIComponent(source_url),
				dataType : 'json',
				url : global.base_url + 'ajax/check_post_source/',
				success : function(data){

					if( data.status == 'error' )
						return sp.modal.alert('Ocurrio un error', data.msg);

					$('input#add_source').val('').removeClass('loading-icon').removeAttr('disabled');

					new_item = $('<li class="ui_icon clearfix"></li>').html(Mustache.render(_tmp, {'favicon_src' : data.favicon, 'title' : data.title, 'url' : source_url}));
								
					$(sources_list).append(new_item);

					$('form#add_post_form ').append(hidden_input);

					$(hidden_input).val(source_url);
					
					post.form.sources[source_url] = source_url;

				}
			});

		}
	},
	remove : function(id){

		id = parseInt(id);

		if( empty(id) )
			return false;

		sp.modal.confirm('Confirmar', 'Estas seguro que deseas eliminar este post?', function(){
		
			$.ajax({
				type : 'post',
				url : global.base_url + 'posts/eliminar/1',
				dataType : 'json',
				data : 'id=' + id,
				success : function(){

					sp.modal.alert('Post eliminado', 'El post fue eliminado', function(){
						sp.modal.close(function(){
							
							document.location = global.base_url;
							
						})
					}).set_class('success');

				}
			})

		}).set_class('error');
	}

};

comments = {};

$(document).ready(function(){
	
	posts.init();
	post.init();

});
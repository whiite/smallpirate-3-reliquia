<?php

class Settings_model extends MY_Model{
		
	protected $_settings;
	protected $inheritances = array(
		'global',
		'users',
		'stream',
		'posts',
		'smileys'
	);
	protected $_cache = array();
	
	public function __counstruct(){
		
		parent::__construct();
		
		return $this;
	}
	
	public function get_all($inheritance = null){
		
		$real_inheritance = !empty($inheritance) && in_array($inheritance, $this->inheritances);
		$inheritance = $real_inheritance ? $inheritance : 'global';
		
		$settings = $this->db
		->select('setting, value, type')
		->where('inheritance', $inheritance)
		->get('settings');
		
		foreach( $settings->result_array() as $row )
		{
			// make output
			$this->_settings[$row['setting']] = $this->_set_type_setting($row['value'], $row['type']);

			// set settings cache
			$this->_cache[$inheritance][$row['setting']] = $row['value'];
		}

		$settings->free_result();
		
		return !empty($this->_settings) ? $this->_settings : false;
	}
	
	public function get($setting = null, $inheritance = null){
		
		if( empty($setting) )
			return false;
		
		// set inheritance
		$real_inheritance = !empty($inheritance) && in_array($inheritance, $this->inheritances);
		$inheritance = $real_inheritance ? $inheritance : 'global';

		// this setting was already called
		if( isset($this->_cache[$inheritance][$setting]) )
			return $this->_cache[$inheritance][$setting];
		
		// match setting
		$where_settings = array(
			'setting' => $setting,
			'inheritance' => $inheritance
		);
		
		$settings = $this->db
		->select('setting, value, type')
		->where($where_settings)
		->limit(1)
		->get('settings');
		
		// uhmm, setting exists?
		if( $settings->num_rows() == 0 )
			return false;
		
		$setting = $settings->row();

		$settings->free_result();

		$this->_set_type_setting($setting);
		
		return $setting->value;
		
	}

	private function _set_type_setting(&$setting, $type = NULL){

		if( is_object($setting) ){
			$value = $setting->value;
			$type = $setting->type;
		}
		else
			$value = $setting;

		if( $type == 'int' )
			$value = (int) $value;

		if( $type == 'list' )
			$value = explode(',', $value);

		if( is_object($setting) )
			$setting->value = $value;
		else
			$setting = $value;

	}
	
}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class users extends SP_controller {

	public $data = array();
	public $current_user = null;
	private $_current_user_id = 0;

	public function __construct(){
		
		parent::__contruct();
		
		$this->load->model('users/login_model');
		$this->load->model('users/users_model');
		
	}
	
	public function _remap($method, $params = array()){

		// possible user from vanity url
		if( !method_exists($this, $method) )
		{

			// check if user exists
			if( ($this->_current_user_id = $this->users_model->user_exists($method)) )
				$method = 'profile';
			else
				show_404();
		}
		
		// call method
		return call_user_func_array(array($this, $method), $params);
	}

	public function index(){

		show_error('indice de usuarios');

	}
	
	public function profile(){
		
		// set title, assets
		$this->template
		->set_title('perfil de Jonathan', 1)
		->add_asset('profile.css')
		->add_asset('profile.js');
		
		if( empty($this->_current_user_id) && $this->ion_auth->logged_in() == FALSE )
			return $this->ingresar();

		//$this->user_data = $this->users_model->get_user_data($this->_current_user_id);
		
		// get profile info
		$profile = $this->db
		->select('profiles.*')
		->where('profiles.user_id', $this->user_data['ID'])
		->limit(1)
		->get('profiles');
		
		$this->user_data += $profile->row_array();

		$profile->free_result();

		$this->template
		->set('info', print_r($this->user_data, 1))
		->set('profile', $this->user_data)
		->display('profile');
		
	}
	
	public function ingresar(){

		$this->template->display('login_view');

	}

	public function autentificar(){
		
		$this->login_model->login();
		
	}

	public function salir(){
		
		$redirect = '/';

		if( $this->input->get('redirect') )
			$redirect = urldecode($this->input->get('redirect'));

		$this->ion_auth->logout();

		$this->session->set_flashdata('message', $this->ion_auth->messages());
		
		redirect($redirect, 'refresh');

	}
}
/* End of file users.php */
/* Location: ./application/modules/users/controllers/users.php */
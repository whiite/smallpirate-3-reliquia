<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Users_model extends MY_model{

	public $username_regexp = '~[a-z0-9_\-]{4,30}~i';
	private $_user_data = array();
	private $_cache_user_searches = array();
	private $_cache_users_data = array(
		'data' => array(),
		'profile' => array(),
		'avatars' => array(),
		'remaining_points' => array()
	);
	
	public function user_exists( $identity = NULL, $check_active = TRUE )
	{

		if( isset($this->_cache_user_searches[$identity]) )
			return $this->_cache_user_searches[$identity];
		
		$identity_type = is_int($identity) ? 'id' : (is_string($identity) ? 'username' : false);

		if( !$identity_type )
			return false;
		
		// find user by name
		if( $identity_type == 'username' )
		{

			if( !preg_match($this->username_regexp, $identity) )
				return false;

			$identity = strtolower($identity);

		}

		// find user by id
		else if( empty($identity) )
				return false;

		$find_condition = array($identity_type => $identity);

		if( $check_active )
			$find_condition += array('active' => '1');

		$result = $this->db
		->select('id')
		->where($find_condition)
		->limit(1)
		->get('users');

		$found = ($result->num_rows() > 0) ? $result->row('id') : false;
		$result->free_result();
		
		if( $found )
			$this->_cache_user_searches[$identity] = $found;

		return $found;
	}

	public function get_user_data($id = NULL){
		
		if( !$this->ion_auth->logged_in() )
			return FALSE;

		$id || $id = $this->session->userdata('user_id');

		if( empty($id) )
			return FALSE;

		if( isset($this->_cache_users_data['data'][$id]) )
			return $this->_cache_users_data['data'][$id];
		
		$result = $this->ion_auth->user($id);
		$this->_user_data = $result->row_array();
		
		$this->_user_data += array(
			'href' => base_url('/' . $this->_user_data['username']),
			'avatar' => $this->make_avatars_src($this->_user_data['folder'], $this->_user_data['hash']),
			'is_logged_in' => $this->ion_auth->logged_in(),
			'is_admin' => $this->ion_auth->is_admin(),
			'group_info' => $this->ion_auth->get_users_groups($this->_user_data['group'])->result(),
			'remaining_points' => $this->get_remaining_points($this->_user_data['ID'])
		);
		
		$this->_cache_users_data['data'][$id] = $this->_user_data;
		
		return $this->_user_data;

	}

	public function get_user_profile($id = NULL){

		

	}

	public function get_user_avatars($user_id = NULL, $size = FALSE)
	{

		if( empty($user_id) )
			return $this->make_avatars_src(FALSE, $size);

		if( isset($this->_cache_users_data['avatars'][$user_id]) )
			return $this->_cache_users_data['avatars'][$user_id];

		$result = $this->db
		->select('avatars.folder, avatars.hash')
		->from('avatars as avatars')
		->from('users as users')
		->where('users.ID', $user_id)
		->where('users.avatar_id = avatars.ID')
		->limit(1)
		->get();

		$avatar_exists = $result->num_rows() > 0;
		$avatar_info = $result->row();
		$result->free_result();

		if( !$avatar_exists )
			return $this->make_avatars_src(FALSE, $size);
		
		if( !isset($this->_cache_user_searches['avatars'][$user_id]) )
			$this->_cache_user_searches['avatars'][$user_id] = $this->make_avatars_src((array)$avatar_info, $size);

		return $this->make_avatars_src((array)$avatar_info, $size);

	}

	public function make_avatars_src(){

		$arguments = func_get_args();

		$default_avatar_src = base_url('/application/assets/images/avatar') . '/';
		$defaul_avatars = array(
			'large' => $default_avatar_src . 'default_large.jpg',
			'medium' => $default_avatar_src . 'default_medium.jpg',
			'small' => $default_avatar_src . 'default_small.jpg'
		);
		
		if( is_array($arguments[0]) && count(array_diff(array_keys($arguments[0]), array('folder', 'hash')))  == 0 )
		{

			$size = isset($arguments[1]) && in_array($arguments[1], array('medium', 'large', 'small')) ? $arguments[1] : FALSE;
			$vars = array($arguments[0]['folder'], $arguments[0]['hash'], $size);

		}
		
		else if ( count($arguments) == 1 && is_array($arguments[0]) && count($arguments[0]) >= 2 )
			$vars = array($arguments[0][0], $arguments[0][1], isset($arguments[0][2]) ? $arguments[0][2] : FALSE); 
		
		else if (count($arguments) >= 2 )
			$vars = array($arguments[0], $arguments[1], isset($arguments[2]) ? $arguments[2] : FALSE);
		
		else
			$vars = 'default';
		
		if( $arguments[0] === FALSE || $vars == 'default' )
		{

			$size = $arguments[1] ? $arguments[1] : FALSE;
			return $size && in_array($size, array_keys($defaul_avatars)) ? $defaul_avatars[$size] : $defaul_avatars;
		
		}

		$vars = (array) $vars;	
		list($folder, $hash, $size) = $vars;
		
		if( !$folder || !$hash )
			return $size && in_array($size, array_keys($defaul_avatars)) ? $defaul_avatars[$size] : $defaul_avatars;
		
		// set avatar src
		$avatars_src = $this->config->item('avatars_url');

		// add folder name and hash to path
		$avatars_src .= sprintf('%s/%s_', $folder, $hash);
		
		// output array or string
		$avatars = array();
		$sizes = array('large', 'medium', 'small');

		foreach( $sizes as $s )
		{

			if( $size && $size == $s )
				return $avatars_src . $s . '.jpg';

			$avatars[$s] = $avatars_src . $s . '.jpg';

		}

		return $avatars;
	}

	public function update_user_stats($stats = array(), $user_id = NULL)
	{

		$user_id = empty($user_id) && !isset($this->current_user) ? 0 : (isset($this->current_user['ID']) ? $this->current_user['ID'] : $user_id);
		
		if( $this->current_user['ID'] && $user_id != $this->current_user['ID'] )
			if( !$this->user_exists($user_id) )
				return FALSE;

		$stats_update = array();

		foreach( $stats as $stat => $value )
		{

			if( in_array($value, array('+', '-')) )
				$value = strtr($value, array(
					'+' => $stat . '+1',
					'-' => $stat . '-1'
				));
			else
				$value = (int) $stat;

			$stats_update[$stat] = $value;

		}

		$this->db->set($stats_update, NULL, FALSE)->where('user_id', $user_id)->update('users_stats');
		
		if( $this->db->affected_rows() > 0 )
			return TRUE;

		return FALSE;
	}

	public function get_remaining_points($user_id = 0)
	{

		$user_id = empty($user_id) && !isset($this->current_user) ? 0 : (!empty($user_id) ? (int) $user_id : $this->current_user['ID']);
		
		if( isset($this->_cache_users_data['remaining_points'][$user_id]) )
			return $this->_cache_users_data['remaining_points'][$user_id];

		$result = $this->db->select('points')->where('user_id', $user_id)->get('users_points');
		$points = $result->row('points');
		$result->free_result();

		$this->_cache_users_data['remaining_points'][$user_id] = (int) $points;

		return (int) $points;

	}

}


?>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends MY_Model
{
	
	public function __construct(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('users/auth');
		$this->load->helper('language');
		
	}
	
	public function login(){
		
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == true)
		{
			
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				
				$this->session->set_flashdata('message', $this->ion_auth->messages());

				$redirect = '/';

				if( $this->input->get('redirect') )
					$redirect = urldecode($this->input->get('redirect'));

				redirect($redirect, 'refresh');

			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('users/ingresar', 'refresh');
			}
		}
		else
		{
			$data = array();
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);
			
			$this->template->set_data($data);
			$this->template->display('users/login_view', $data);
		}
		
	}
	
}
?>
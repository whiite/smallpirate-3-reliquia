<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Errors extends SP_controller {

	var $heading;
	var $message;
	var $view;
	var $status_code = 500;
	var $_data = array();

	public function _handle_error(){

		$arguments = func_get_arg(0);
		
		foreach( $arguments as $key => $arg )
			$this->{$key} = (isset($arg) ? $arg : NULL);
		
		if( $this->view && method_exists(__CLASS__, $this->view) )
			return call_user_func(array(__CLASS__, $this->view));
		else
			return $this->error_general();


	}

	public function error_general(){
		
		$this->_display_error();

	}

	public function error_404(){

		$this->_display_error();
		
	}

	private function _display_error(){
		
		set_status_header($this->status_code);

		if( is_array($this->message) )
			$this->message = implode('<br />', $this->message);

		$this->template
			 ->set(array_merge(array('heading' => $this->heading, 'message' => $this->message), $this->_data))
			 ->display($this->view);

	}

}
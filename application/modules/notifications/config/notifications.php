<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// update user total notifications probability
$config['notifications']['update_unseen_probability'] = 5; // 10 %

// lang link wrappers
$config['notifications']['link_wrappers'] = array(
	'post' => '<a href="{{href}}" title="{{title}}">{{name}}</a>',
	'user' => '<a href="{{href}}" title="{{name}}" data-user="">{{name|capitalize}}</a>'
);
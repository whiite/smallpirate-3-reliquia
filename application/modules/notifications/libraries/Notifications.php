<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Spirate
 *
 * Social linksharing script
 *
 * @package		Spirate
 * @author		Spirate team
 * @license		http://spirate.net/license.txt
 * @link		http://spirate.net
 * @version		3.0
 */

/**
 * Spirate notifications library
 *
 * Esta clase te permite controlar las notificaciones
 *
 * @package		Spirate
 * @subpackage	Libraries
 * @category  	Libraries
 * @author		Spirate Team
 * @version		1.0
 */

class Notifications{

	var $CI;
	public $count = 0;
	private $_config = array();
	private $_lang = array();
	private $_applied_filters = array();
	private $_types = array(
		'comment_post',	'new_post',
		'follow_post', 'share_post',
		'vote_post', 'comment_reply',
		'follow_user', 'add_friend'
	);
	private $_subjects_cache = array();
	private $_objects_cache = array();

	public function __construct()
	{

		// get ci instance
		$this->CI =& get_instance();

		// cargamos la libreria parser
		$this->CI->load->library('parser');

		// cargamos la configuracion de las notificaciones
		$this->CI->load->config('notifications/notifications');
		$this->_config = $this->CI->config->item('notifications');
		
		// load language
		$this->CI->lang->load('notifications');
		
		// set language items
		$this->_lang = $this->CI->lang->line('notifications');
		
		// update unseen notifications and get total users notifications
		if( !($this->count = $this->_update_unseen_notifications(TRUE)) )
			$this->count = (int) $this->CI->current_user['notifications'];

	}

	public function get_notifications()
	{

		if( !$this->CI->current_user )
			return array();

		// set default limit
		//if( !isset($this->_applied_filters['limit']) )
			//$this->_applied_filters['limit'] = array(0, 50);

		$data = $this->_get_notifications_data();

		foreach($data as $data)
			echo $data['action'] . '<br>';

	}

	public function apply_filters(Array $filters)
	{

		$conditions = array();
		$filter_dates = array(
			'yesterday' => time()-86400,
			'weekly' => time()-604800,
			'monthly' => time()-18748800
		);

		foreach( $filters as $filter => $value )
		{

			if( $filter == 'disabled_types' && $this->_is_a_type($value))
			{

				$conditions['where_not_in']['activities.type'] = $value;

			}

			if($filter == 'date' && in_array($value, array_keys($filter_dates)))
			{

				$conditions['where']['activities.time < '] = $filter_dates[$value];

			}

			if($filter == 'limit')
			{

				$value = (array)$value;

				array_map('intval', $value);
				$conditions['limit'] = $value;

			}

		}

		$this->_applied_filters = $conditions;

		return $this;

	}

	private function _get_notifications_data()
	{

		$result = $this->CI->db
		->select('activities.type, activities.subject, activities.subject_type, activities.object, activities.object_type')
		->select('activities.date, notifications.ID, notifications.user_id AS user_id, notifications.params, notifications.time')
		->where('notifications.user_id', $this->CI->current_user['ID'])
		->join('activities', 'activities.ID = notifications.activity_id')
		->order_by('notifications.ID','DESC');

		if( count($this->_applied_filters) > 0 )
		{

			foreach( $this->_applied_filters as $condition => $condition_values )
			{

				if ($condition == 'where_not_in' || $condition == 'where')
				{
					foreach($condition_values as $column => $values)
					{

						$result->{$condition}($column, $values);

					}
				}

				if($condition == 'limit')
					call_user_func_array(array($result, 'limit'), $values);

			}

		}

		$result = $result->from('notifications')->get();

		$rows = $result->result_array();
		$result->free_result();

		$data = array();
		foreach( $rows as $key => $row )
		{

			switch( $row['subject_type'] )
			{

				case 'user':

					if( isset($this->_subjects_cache[$row['subject_type']][$row['subject']]) )
					{
						$subject_data = $this->_subjects_cache[$row['subject_type']][$row['subject']];
						continue;
					}
					
					$result = $this->CI->db->select('username')->where('ID', $row['subject'])->get('users');
					$subject_data = $result->row_array();
					$result->free_result();

					$subject_data += array('avatar' => $this->CI->users_model->get_user_avatars());

					$this->_subjects_cache[$row['subject_type']][$row['subject']] = $subject_data;

				break;

			}

			switch( $row['object_type'] )
			{

				case 'post':

					if( isset($this->_objects_cache[$row['object_type']][$row['object']]) )
					{
						$object_data = $this->_objects_cache[$row['object_type']][$row['object']];
						continue;
					}

					$result = $this->CI->db->select('user_id AS author, title, category, slug AS category_slug')
					->join('categories', 'categories.ID = posts.category')
					->where('posts.ID', $row['object'])
					->get('posts');

					$object_data = $result->row_array();
					$result->free_result();

					$this->_objects_cache[$row['object_type']][$row['object']] = $object_data;

				break;

			}

			$data[$row['ID']] = array(

				'id' => $row['ID'],
				'type' => $row['type'],
				'user_id' => $row['user_id'],
				'time' => $row['time'],
				'date' => $row['date'],
				'params' => json_decode($row['params']),
				'subject' => array(
					'id' => $row['subject'],
					'type' => $row['subject_type'],
					'data' => $subject_data
				),
				'object' => array(
					'id' => $row['object'],
					'type' => $row['object_type'],
					'data' => $object_data
				),

			);

			$data[$row['ID']]['action'] = $this->_make_singular_text_actions($data[$row['ID']]);

		}

		$this->_merge_notifications($data);

		return $data;

	}

	private function _merge_notifications(&$data)
	{

		foreach( $data as $ID => $activity )
		{

			$last_id = 0;

			switch( $activity['type'] )
			{

				case 'post_comment':

					

				break;

			}

		}

		return $data;
	}

	private function _make_singular_text_actions($data)
	{

		$action = '';
		$subject_data = $data['subject']['data'];
		$object_data = $data['object']['data'];
		$to = 'other';
		
		switch($data['type'])
		{

			case 'post_comment':
			case 'new_post':
			case 'vote_post':

				$to = 'other';

				if( in_array($data['type'], array('post_comment')) )
				{

					if( $data['user_id'] == $object_data['author'] )
						$to = 'own';

					if( $data['subject']['id'] == $object_data['author'] )
						$to = 'author';

				}

				$replaces = array(
					'post' => array(
						'href' => semantic_url('posts/' . $object_data['category_slug'] . '/' . $data['object']['id'] . '/' . $object_data['title']),
						'title' => $object_data['title'],
						'name' => $this->_lang['post']
					),
					'user' => array(
						'href' => base_url('users/' . $subject_data['username']),
						'name' => $subject_data['username']
					)
				);

			break;

		}

		$action = $this->_lang['templates']['singular'][$data['type']][$to];

		if( strpos($action, '{{params') !== FALSE )
			$action_data['params'] = $data['params'];
		
		foreach( $this->_config['link_wrappers'] as $type => $link )
			$action_data["link_{$type}"] = $this->CI->parser->parse_string($link, $replaces[$type]);

		$action = $this->CI->parser->parse_string($action, $action_data);

		return $action;

	}

	private function _update_unseen_notifications($check_probability = FALSE, $user_id = 0)
	{

		// check probability

		if( $check_probability && !((rand() % 100) < $this->_config['update_unseen_probability']) )
			return FALSE;

		// set user id
		$user_id = (int) $user_id;
		$user_id = empty($user_id) ? ($this->CI->current_user ? $this->CI->current_user['ID'] : 0) : $user_id;

		// user is necessary
		if( empty($user_id) )
			return FALSE;

		// get total unseen notifications
		$result = $this->CI->db->select('COUNT(ID) as total_unseen')->where(array('user_id' => $user_id, 'seen' => 0))->get('notifications');
		$total_unseen = $result->row('total_unseen');
		$result->free_result();

		// update user notifications
		$this->CI->db->set('notifications', $total_unseen)->where('ID', $user_id)->update('users');

		// return unseen notifications
		return $total_unseen;

	}

	private function _is_a_type($types = FALSE)
	{

		if(!is_array($types))
			return in_array($types, $this->_types);
		else
			return (count(array_intersect($types, $this->_types)) > 0);

	}

}
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
	
class Permissions_model extends  MY_Model{

	protected $_cache_permissions = array();

	public function has_permission($permission, $user_id = NULL){

		$user_id = (int) $user_id;
		$user_id = empty($user_id) ? ( $this->current_user ? $this->current_user['ID'] : FALSE ) : $user_id;
		$is_current_user = FALSE;

		$permissions = array();

		if( preg_match('~([a-z0-9_]+\,)+~', $permission) )
			$permissions = explode(',', $permission);
		else
			$permissions = (array) $permission;

		array_walk($permissions, 'trim');
		$permissions = array_unique($permissions);
		
		if( $user_id !== FALSE && isset($this->_cache_permissions[$user_id]) )
		{

			foreach ( $permissions as $inheritance => $permission )
				if( isset($this->_cache_permissions[$user_id][$permission]) )
				{
					
					if( $this->_cache_permissions[$user_id][$permission] === 0 )
						return FALSE;

				}

			// Todos los permisos han sido establecidos en cache y no hay ninguno con problemas
			if ( array_diff(array_keys($this->_cache_permissions[$user_id]), $permissions) == 0 )
				return TRUE;

		}

		if( empty($user_id) && $this->current_user )
			$is_current_user = TRUE;

		// Buen intento! mejor suerte la proxima
		if( empty($user_id) && !$is_current_user )
			return FALSE;

		// Welcome again Sir! do you want a beer?
		else if( $is_current_user && $this->current_user['is_admin'] )
			return TRUE;

		else if( $is_current_user )
			$group_id = $this->current_user['group'];

		else
		{
			
			// obtenemos el grupo del usuario
			$result = $this->db->select('group')->where('ID', $user_id)->get('users');
			
			$user_exists = $result->num_rows() > 0;
			$group_id = $result->row('group');
			$result->free_result();

			// user exists?
			if( !$user_exists )
				return FALSE;

			// wait sir! you can pass!, here please.
			if( $group_id == 1 )
				return TRUE;

		}
			
		if( !isset($this->_cache_permissions[$user_id]) )
			$this->_cache_permissions[$user_id] = array();
		
		// find permission/permissions
		$result = $this->db
		->select('permission, allow')
		->where_in('permission', $permissions)
		->where('group', $group_id)
		->get('permissions');
		
		$rows = $result->result_array();
		$num_rows = $result->num_rows();
		$result->free_result();

		$has_permission = TRUE;

		// si no hay resultados o la cantidad de resultados es diferente a la cantidad de permisos dada devolvemos FALSE
		if( $num_rows == 0 || count($permissions) != $num_rows )
		{

			// si hay resultados guardamos en cache
			if( $num_rows > 0 )
			{
				
				$permissions_fetch = array();

				foreach( $rows as $row )
					$permissions_fetch[$row['permission']] = $row['allow'];

				foreach( $permissions as $permission )
				{
					
					$permission_result = isset($permissions_fetch[$permission]) ? !empty($permissions_fetch[$permission]) : 0;
					$this->_cache_permissions[$user_id][$permission] = $permission_result;
				
				}

			}

			return FALSE;

		}
		
		// buscamos si hay algun permiso denegado en los resultados
		foreach( $rows as $row )
		{
			
			$has_permission &= !empty($row['allow']);
			$this->_cache_permissions[$user_id][$row['permission']] = $has_permission;

		}

		// can or can't ?
		return $has_permission;

	}

}
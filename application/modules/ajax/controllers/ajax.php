<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajax extends SP_controller {

	var $_config = array();

	public function __construct(){

		parent::__contruct();

		// cargar lenguajes
		$this->lang->load('ajax/ajax');

		// cargar modelo
		$this->load->model('ajax/ajax_model');

		// cargar libreria form_validation
		$this->load->library('form_validation');

		// cargar configuracion
		$this->load->config('ajax/config');
		$this->_config = $this->config->item('ajax');

		// definimos el tipo contenido como json
		$this->output->set_content_type('application/json');

	}

	public function _remap($method, $params){

		header('Content-Type: application/json; charset=utf8');

		if( !method_exists($this, $method) || !$this->input->is_ajax_request() )
			return $this->ajax_model->output_data(array('msg' => 'bad request'), 'error');

		else
			return call_user_func_array(array(__CLASS__, $method), $params);

	}
	
	public function preview_post(){

		// cargamos la libreria bbcode
		$this->load->model('posts/posts_model');

		// definimos el titulo
		$title = $this->input->post('title', TRUE);

		// definimos el cuerpo del post
		$body = $this->input->post('body', TRUE);

		// formateamos el cuerpo del post
		$body = $this->posts_model->parse_msg($body);

		// devolvemos la informacion en json
		$this->ajax_model->output_data(array('title' => $title, 'body' => $body), 'success');

	}
	
	public function check_post_source(){

		// definimos la url
		$url = trim($this->input->post('url'));
		
		// si esta vacia o no existe devolvemos error
		if( empty($url) )
			$this->ajax_model->output_data('No ingresaste la url de la fuente.', 'error');

		// si no es una url valida devolvemos error
		if( !preg_match('~(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?~i', $url) )
			$this->ajax_model->output_data('La url ingresada es incorrecta.', 'error');

		// removemos anchor
		$url = preg_replace('~#[^$]*$~', '', $url);

		// cargamos la libreria scraper para una verificacion
		$this->load->library('scraper');

		// comprobamos urls verificadas anteriormente
		$log_sources = $this->db->select('title, valid')->where('url', $url)->get('url_sources');

		if( $log_sources->num_rows() > 0 )
		{

			$source_data = $log_sources->row();

			$log_sources->free_result();

			// la fuente previamente verificada fue invalida.
			if( !$source_data->valid )
				$this->ajax_model->output_data('Ocurrio un error al verificar tu url.', 'error');
			
			// devolvemos la informacion de la url previamente verificada.
			$this->ajax_model->output_data(array(
				'title' => $source_data->title,
				'favicon' => sprintf($this->_config['favicon_api_url'], $url)
			), 'success');

		}

		else{

			$scraper = $this->scraper->process($url);

			$this->db->insert('url_sources', array(
				'url' => $url,
				'title' => $scraper->get_title(),
				'valid' => $scraper === FALSE ? 0 : 1,
				'verified_time' => time()
			));

			// si la url ingresada no existe devolvemos error
			if( $scraper === FALSE )
				$this->ajax_model->output_data('Ocurrio un error al verificar tu url.', 'error');

			// devolvemos la informacion en json
			$this->ajax_model->output_data(array(
				'title' => $scraper->get_title(),
				'favicon' => sprintf($this->_config['favicon_api_url'], $url)
			), 'success');

		}

	}

	public function follow_post()
	{

		

	}

	public function bookmark_post()
	{



	}

	public function denounce_form_post()
	{



	}

	public function denounce_post()
	{



	}

	public function vote_post()
	{



	}

	public function add_comment()
	{



	}

	public function remove_comment()
	{



	}

	public function vote_comment()
	{



	}

	public function edit_comment()
	{

		
		
	}

}
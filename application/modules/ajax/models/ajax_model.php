<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
	
class Ajax_model extends  MY_Model{

	public function output_data($msg = '', $status = 'success'){

		$data = array();

		if( is_array($msg) )
			foreach( $msg as $key => $value )
				$data[$key] = $value;
		else
			$data['msg'] = $msg;

		$data['status'] = $status;

		die(json_encode($data));
	}

}
<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// general lines
$lang['notifications']['title'] = 'notificaciones';
$lang['notifications']['see_more'] = 'ver m&aacute;s';
$lang['notifications']['post'] = 'post';

// templates
$lang['notifications']['templates'] = array(

	'singular' => array(
		// comment post
		'post_comment' => array(
			'other' => '{{link_user|raw}} ha comentado el {{link_post|raw} de {{link_user|raw}}',
			'author' => '{{link_user|raw}} ha comentado su {{link_post|raw}}',
			'own' => '{{link_user|raw}} ha comentado tu {{link_post|raw}}'
		),
		'new_post' => array(
			'other' => '{{link_user|raw}} ha creado un nuevo {{link_post|raw}}'
		),
		'vote_post' => array(
			'other' => '{{link_user|raw}} ha dado {{params.points}} {{params.points > 1 ? "puntos" : "punto"}} a tu {{link_post|raw}}'
		)
	),
	
	'plural' => array(

		// comment post
		'post_comment' => array(
			'other' => '{{link_user|raw}} ha comentado el {{link_post|raw} de {{link_user|raw}}',
			'author' => '{{link_user|raw}} ha comentado su {{link_post|raw}}',
			'own' => '{{link_user|raw}} ha comentado tu {{link_post|raw}}'
		),
		'new_post' => array(
			'other' => '{{link_user|raw}} ha creado un nuevo {{link_post|raw}}'
		),
		'vote_post' => array(
			'other' => '{{link_user|raw}} ha dado {{params.points}} {{params.points > 1 ? "puntos" : "punto"}} a tu {{link_post|raw}}'
		)

	)

);

// nexus
$lang['notifications']['nexus'] = array(
	'and' => 'y'
);
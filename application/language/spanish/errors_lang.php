<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Error 404
$lang['error_404_title']	= 'Página no encontrada';
$lang['error_404_message']	= 'No pudimos encontrar la página que estabas buscando. <a href="%s">Ir a la página principal</a>.';

// Database
$lang['error_invalid_db_group']	= 'El grupo de configuración de la base de datos es incorrecto "%s".';

// misc
$lang['error_view_not_found'] = 'El archivo vista <strong>%s</strong> no ha sido encontrado.';
$lang['error_csrf_invalid_token'] = 'Error al enviar el formulario. El id del formulario no coincidi&oacute;.';
$lang['error_not_have_permissions'] = 'No dispones de permisos para realizar esta acci&oacute;n';

/* End of file errors_lang.php */
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Spirate
 *
 * Social linksharing script
 *
 * @package		Spirate
 * @author		Spirate team
 * @license		http://spirate.net/license.txt
 * @link		http://spirate.net
 * @version		3.0
 */

/**
 * ImageWorkshop class
 * 
 * wrapper class
 *
 * @version 2.0.3
 * @link http://phpimageworkshop.com
 * @author Sybio (Clément Guillemain / @Sybio01)
 * @license http://en.wikipedia.org/wiki/MIT_License
 * @copyright Clément Guillemain
 */

require_once(dirname(__FILE__).'/PHPImageWorkshop/ImageWorkshop.php');

/* ------------------------------------------------------------------------ */

class Image extends ImageWorkshop{
	
	public function initFromUrl($filename){

		switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
	        case 'jpeg':
	        case 'jpg':
	            $image = imagecreatefromjpeg($filename);
	        break;

	        case 'png':
	            $image = imagecreatefrompng($filename);
	        break;

	        case 'gif':
	            $image = imagecreatefromgif($filename);
	        break;

	        default:
	            throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
	        break;
	    }
        
        return new ImageWorkshopLayer($image);

	}

}
<?php
	
/**
 * Spirate
 *
 * Social linksharing script
 *
 * @package		Spirate
 * @author		Spirate team
 * @license		http://spirate.net/license.txt
 * @link		http://spirate.net
 * @version		3.0
 */

/**
 * Spirate BBCODE library
 *
 * This class enables you to parse bbcode to html or html to bbcode
 *
 * @package		Spirate
 * @subpackage	Libraries
 * @subpackage	jbbcode
 * @category  	Libraries
 * @author		Spirate Team
 * @version		1.0
 */


/* ------------------------------------------------------------------------ */

require_once APPPATH . '/libraries/jbbcode/Parser.php';
 
class Bbcode{

	var $parser = NULL;

	public function __construct(){

		$this->parser = new JBBCode\Parser();
		$this->parser->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());

	}

	public function bbcode_to_html( $text = '' ){

		$this->parser->parse($text);

		return $this->parser->getAsHtml();

	}
	
	public function html_to_bbcode( $html = '' ){

		$this->parser->parse($html);

		return $this->parser->getAsBBCode();
	}

}
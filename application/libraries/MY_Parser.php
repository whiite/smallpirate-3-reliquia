<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Spirate parser class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Parser
 * @license	 http://philsturgeon.co.uk/code/dbad-license
 * @link		http://philsturgeon.co.uk/code/codeigniter-dwoo
 */



class MY_Parser extends CI_Parser {

	private $CI;
	private $parser;

	public function __construct()
	{
		
		$this->CI = & get_instance();

		if( !class_exists('Twig_Autoloader') )
		{
			
			require dirname(__FILE__) . '/Twig/Autoloader.php';
			Twig_Autoloader::register();

		}

		$this->parser = new Twig_Environment(new Twig_Loader_String());
		
	}

	public function parse($template, $data = array(), $return = false)
	{
		$string = $this->CI->load->view($template, $data, true);

		return $this->_parse($string, $data, $return);
	}

	public function parse_string($string, $data = array(), $return = TRUE)
	{
		return $this->_parse($string, $data, $return);
	}

	// --------------------------------------------------------------------

	/**
	 *  Parse
	 *
	 * Parses pseudo-variables contained in the specified template,
	 * replacing them with the data in the second param
	 *
	 * @access	protected
	 * @param	string
	 * @param	array
	 * @param	bool
	 * @return	string
	 */
	public function _parse($string, $data, $return = false)
	{
		
		$data = (array) $data;

		$parsed = $this->parser->render($string, $data);
		
		// Return results or not ?
		if ( !$return)
		{
			$this->CI->output->append_output($parsed);
			return;
		}

		return $parsed;
	}
}

// END MY_Parser Class

/* End of file MY_Parser.php */
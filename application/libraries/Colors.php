<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require dirname(__FILE__) . '/Color/src/color.php';

class Colors {

	public function set( $hex ){

		return new Color( $hex );

	}

}
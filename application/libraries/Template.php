<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Spirate Template Class
 *
 * Build your CodeIgniter pages much easier with partials, breadcrumbs, layouts and themes
 *
 * @package			Spirate
 * @subpackage		Libraries
 * @subpackage		Twig
 * @category		Libraries
 * @author			Jonathan Andres J.
 */

if( !class_exists('Twig_Autoloader') )
{
	
	require dirname(__FILE__) . '/Twig/Autoloader.php';
	Twig_Autoloader::register();

}

class Template
{
	
	private $CI; // CI instance
	private $_twig; // twig environment
	private $_module = ''; // current module
	private $_controller = ''; // current controller
	private $_method = ''; // current method
	private $_proccessed = FALSE; // template was processed?
	private $_theme; // current theme
	private $_theme_seted = FALSE;
	private $_data = array(); // template data vars
	private $_global_vars = array();
	private $_title = array(); // page title
	private $_metadata = array(); // metadata on head tag
	private $_regions = array(); // regions to process
	private $_breadcrumb = array(); // breadcrumb
	private $_assets = array(); // assets to process
	private $js_vars = array(); // javascript vars
	private $js_extended_objects = array(); // javascript vars to extend Jquery.extend(object, new_object)
	public $theme = array(); // theme info
	public $ie_cache = FALSE; // enable ie cache?

	/**
	 * Constructor
	 */
	public function __construct()
	{
		// get instance
		$this->CI =& get_instance();

		// set default template configs
		$this->init();

		// class intialized!
		log_message('debug', 'Spirate Template Class Initialized');
	}

	/**
	 * intialize library
	 *
	 * @access private
	 * @param array @configs
	 * @return void
	 */
	private function init(){

		// load library user_agent
		$this->CI->load->library('user_agent');

		// load javascript helper
		$this->CI->load->helper('javascript');

		// load template config vars
		$this->CI->load->config('template');

		// set default template settings
		$this->configs = $this->CI->config->item('template');

		// set default theme var
		$this->theme['default_theme_url'] = base_url('/application/themes/' . $this->configs['default_theme']);
		
		// set module, method and controller
		$this->_controller = $this->CI->router->fetch_class();
		$this->_method = $this->CI->router->fetch_method();
		$this->_module = $this->CI->router->fetch_module();

		// set twig environment
		$this->setup_twig();

	}

	/**
	 * Set a theme
	 *
	 * @access public
	 * @param string $theme (default=NULL)
	 * @return object $this
	 */
	public function set_theme($theme = NULL){

		// get theme or set default theme
		$this->_theme = $theme ? $theme : $this->configs['default_theme'];

		// get current theme location
		$location = $this->current_theme_dir();

		// if not exists...
		if( !is_dir(realpath($location)) )
			return show_error(
				array(
					sprintf('theme <strong>%s</strong> not exists', $theme),
					sprintf('<strong>looked into:</strong> %s', $location)
				)
			);

		// add theme assets
		$this
		->add_dir_assets($this->current_theme_dir() . 'assets/')
		->add_asset('theme.css');

		// set theme url
		$this->theme['theme_url'] = base_url('/application/themes/' . $this->current_theme());

		// el theme fue configurado
		$this->_theme_seted = TRUE;

		// chaining method
		return $this;

	}

	/**
	 * Set page title
	 *
	 * @access public
	 * @param string $title
	 * @param int $pos
	 * @return object $this
	 */
	public function set_title($title, $pos = -1){

		// add other title on list
		array_push($this->_title, $title);

		// chaining method
		return $this;

	}

	/**
	 * append a tag to metadata
	 *
	 * @access public
	 * @param string $type
	 * @param array $attributes
	 * @return object $this
	 */
	public function append_metadata($type, $attributes = array()){

		if( is_array($type) )
		{

			foreach($type as $data)
			{
				
				$type = $data['type'];
				unset($data['type']);

				$this->append_metadata($type, $data);
			
			}
			
			return $this;

		}

		// add other tag to list
		if( !isset($this->_metadata[$type]) )
			$this->_metadata[$type] = array();

		array_push($this->_metadata[$type], $attributes);

		return $this;

	}

	/**
	 * Set a template region
	 *
	 * @access public
	 * @param mixed $var
	 * @param array $var (default=array())
	 * @param bool $parse (default=FALSE)
	 * @return object $this
	 */
	public function set_region($name, $vars = array(), $parse = FALSE){

		// add new region
		$this->_regions[$name] = array(
			'view' => isset($vars['view']) ? $vars['view'] : FALSE,
			'string' => isset($vars['string']) ? $vars['string'] : FALSE,
			'data' => !isset($vars['data']) ? array() : (array) $vars['data'],
			'parse' => (bool) $parse,
		);

		// chaining method
		return $this;
	}
	
	/**
	 * Set a template var
	 *
	 * @access public
	 * @param mixed $var (default=NULL)
	 * @param mixed $value (default=NULL)
	 * @param bool $global (default=FALSE)
	 * @return object $this
	 */
	public function set($key = NULL, $value = NULL, $global = FALSE){
		
		if( empty($key) )
			return;
		
		if( is_array($key) )
		{

			$global = (bool) $value;

			foreach( $key as $k => $v )
				$this->set($k, $v, $global);

			return $this;
		
		}
		else
		{
			
			if( in_array($key, $this->_global_vars) )
				return $this;

			if( $global )
				$this->_global_vars[] = $key;

			$this->_data[$key] = $value;
		}
		
		return $this;

	}

	/**
	 * set base template
	 * 
	 * @access public
	 * @param string $template (default=NULL)
	 * @return object $this
	 */

	public function base_template($template = NULL){

		$this->_template = $template;

		return $this;

	}

	/**
	 * Display template
	 * 
	 * @access public
	 * @param string $view
	 * @param bool $parse
	 * @return object $this
	 */

	public function display( $view = NULL, $parse = FALSE ){

		// template was processed? avoid
		if( $this->_proccessed )
			return FALSE;

		if( !$this->_theme_seted )
			$this->set_theme();

		// process template
		$this->build_template($view, $parse);

		// display template
		$this->_twig->display($this->get_base_template(), $this->_data);

		// template was proccessed
		$this->_proccessed = TRUE;

		// chaining
		return $this;

	}

	/**
	 * set template breadcrumb
	 * 
	 * @access public
	 * @param array $data
	 * @param bool $return
	 * @return object $this
	 */

	public function set_breadcrumb($data, $return = FALSE){
		
		// attributes order
		$sort_attributes = array('class','href','title','text', 'selected');

		// add keys to attributes
		$sort_attributes = array_combine($sort_attributes, range(0, count($sort_attributes)-1));

		// define breadcrumb list
		$breadcrumbs = array();

		// define output
		$output = array();
		
		// process all data
		foreach( $data as $method => $item ){

			// show or no this item?
			if( isset($item['show_if']) && ((bool)$item['show_if']) === FALSE )
				continue;
			
			// define item data
			$item_data = array();
			
			// process all items
			foreach($item as $k => $v )
			{
				
				if( !in_array($k, array_keys($sort_attributes)) )
					continue;
				
				$item_data[$sort_attributes[$k]] = $v;
			}
			
			// condition to show item
			$custom_condition = (in_array('condition', array_keys($item)) && ((bool) $item['condition']) === TRUE);
			
			// current method or custom condition
			if( $this->_method == $method ||  $custom_condition ){
				
				if( !isset($item_data[$sort_attributes['class']]) )
					$item_data[$sort_attributes['class']] = '';
				
				$item_data[$sort_attributes['class']] .= ' selected';
				
			}
			
			// sort all items properly
			ksort($item_data);
			
			$last_item_key = count(array_keys($item_data));
			$item_keys = array_keys($item_data);
			$first_item_key = array_splice($item_keys, 0, 1);
			$first_item_key = $first_item_key[0];
			
			$item_data = array_combine(
				array_keys(array_slice($sort_attributes, $first_item_key, $last_item_key)),
				array_values($item_data)
			);
			
			if( $this->_method == $method || $custom_condition)
				$item_data['selected'] = true;
			
			array_push($breadcrumbs, $item_data);
			
		}
		
		
		$this->_breadcrumb = $breadcrumbs;

		return $this;
	}

	public function add_module_assets($module = FALSE){

		if( !$module )
			$module = $this->_module;

		if( is_dir(($module_theme_dir = $this->current_theme_dir() . 'views/modules/' . $module . '/assets/')) )
			$this->add_dir_assets($module_theme_dir);

		else
			$this->add_dir_assets(APPPATH . 'modules/' . $module . '/assets/');

		return $this;
	}

	public function add_asset($name, $minify = false, $combine = false, $media = 'screen'){
		
		$asset_ext = substr($name, (strrpos($name, '.') + 1));
		$asset_type = in_array($asset_ext, array('js', 'css')) ? $asset_ext : false;
		$asset_name = substr($name, 0, strrpos($name, '.'));
		$src = $name;
		
		if( !$asset_type )
			return false;
		
		$asset_info = array(
			'asset_name' => $asset_name,
			'type' => $asset_type,
			'src' => $src,
			'minify' => (bool) $minify,
			'combine' => (bool) $combine,
			'media' => $media
		);
		
		$this->_assets[] = $asset_info;

		return $this;		
	}

	public function add_dir_assets($directory){
		
		if ( is_array($directory) )
		{
			foreach( $directory as $loc )
				$this->add_dir_assets($loc);

			return $this;
		}

		// add assets path to sprinkle config
		$directories = glob($directory . '*');

		if( is_array($directories) )
			foreach( $directories as $dir ){

				if( is_dir($dir) ){

					if( !preg_match('~(/css|/js)$~', $dir) )
						continue;

					$this->CI->sprinkle->add_assets_dir($dir[strlen($dir)-1] != '/' ? $dir . '/' : $dir);

				}
				else
					$this->CI->sprinkle->add_assets_dir($directory);


			}
		
		return $this;		
	}

	/**
	 * add a function in twig environments
	 * 
	 * @access public
	 * @param mixed $function
	 * @return object $this
	 */

	public function register_function($function){

		if( !function_exists($function) )
			show_error(sprintf('Function %s not exists', $function));

		$this->_twig->addFunction(new Twig_SimpleFunction($function, $function));

		return $this;

	}

	/**
	 * add a filter in twig environments
	 * 
	 * @access public
	 * @param mixed $filer
	 * @return object $this
	 */

	public function register_filter($filter){

		if( !function_exists($filter) )
			show_error(sprintf('Filter %s not exists', $filter));

		$this->_twig->addFilter(new Twig_SimpleFilter($filter, $filter));

		return $this;

	}

	/**
	 * output inline javascript
	 * 
	 * @access public
	 * @param string $code
	 * @param bool $return (default=TRUE)
	 * @param bool $format_code (default=FALSE)
	 * @return string
	 */

	public function inline_js($code, $return = true, $format_code = false){
		
		$tag = output_script_tag($code, $format_code);
		
		if( $return )
			$this->_inline_js[] = $tag;
		else
			return $tag;
	}

	/**
	 * add a var on javascript vars
	 * 
	 * @access public
	 * @param mixed $key
	 * @param string $var
	 * @param bool $object (default=FALSE)
	 * @param bool $extend (default=FALSE)
	 * @return object $this
	 */
	
	public function set_js_var($key, $var, $object = FALSE, $extend = FALSE){

		if( is_array($key) ){

			$args = func_get_args();

			$object = isset($args[1]) ? $args[1] : FALSE;
			$merge = isset($args[2]) ? $args[2] : FALSE;
			
			foreach($key as $k => $v )
				$this->set_js_var($k, $v, $object, $merge);
			
			return $this;
		}
		
		if( is_array($var) && isset($var['type']) )
		{
			
			$data_var = $var;

			$data_var['type'] = isset($data_var['type']) ? $data_var['type'] : 'AUTO';

			if( $data_var['type'] == 'function' ){

				$var = "function(" . $data_var['args'] . "){\n";
				$var .= format_var($data_var['context'], 'function');
				$var .= "\n}";

			}

			else if( $data_var['type'] == 'object' )
				$var = "{" . $data_var['context'] . "}";

			else
				$var = $data_var['context'];

			$type = $data_var['type'] == 'AUTO' ? false : $data_var['type'];
			
		}
		else
			$type = js_var_type($var);

		$formatted_var = format_var($var, $type);

		if( $object )
		{
			
			if( !isset($this->js_vars[$object]) )
				$this->js_vars[$object] = array();

			if( $extend )
				$this->js_extended_objects[] = $object;

			if( isset($this->js_vars[$object][$key]) )
				$this->js_vars[$object][$key] = $formatted_var;

			else
				$this->js_vars[$object] += array($key => $formatted_var);

		}
		else
			$this->js_vars[$key] = $formatted_var;

		return $this;		
	}

	/**
	 * build javascript vars and output
	 * 
	 * @access public
	 * @return string
	 */

	private function make_js_vars(){
		
		$output = '';
		
		foreach( $this->js_vars as $key => $value){

			$extend = FALSE;

			if( in_array($key, $this->js_extended_objects) )
				$extend = TRUE;
			
			if( is_array($value) ){

				$output .=  ($extend ? 'jQuery.extend(' . $key . ', {' : "$key = {") . "\n";
				$keys = array_keys($value);
				$last_item = end($keys);
				
				foreach($value as $m => $v)
					$output .= "\t$m : $v" . ( $last_item != $m ? ',' : '') . "\n";

				$output .= "}" . ($extend ? ')' : '') . ";\n";
				
			}
			else
				$output .= "$key = $value;\n";
			
		}

		return $output;
		
	}

	/**
	 * process all data and build the template
	 * 
	 * @access public
	 * @param string $view
	 * @param bool $parse
	 * @return string
	 */

	private function build_template($view, $parse){

		$template = array();

		$template['regions'] = array();
		$template['metadata'] = $this->_get_metadata();
		$template['title'] = $this->_get_segmented_title();

		$this->_data['_template'] =& $template;

		if( !$this->ie_cache && $this->CI->agent->browser() == 'Internet Explorer' )
		{
			$this->CI->output->set_header('Expires: Sat, 01 Jan 2000 00:00:01 GMT');
			$this->CI->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->CI->output->set_header('Cache-Control: post-check=0, pre-check=0, max-age=0');
			$this->CI->output->set_header('Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
			$this->CI->output->set_header('Pragma: no-cache');
		}

		// set favicon
		if( file_exists($this->current_theme_dir() . 'assets/images/favicon.ico') )
			$template['favicon'] = $this->_theme()->theme_url . 'assets/images/favicon.ico';
		else
			$template['favicon'] = base_url('application/assets/images/favicon.ico');
		
		// load custom assets
		foreach( $this->_assets as $asset_info )
			$this->CI->sprinkle->add_asset($asset_info);

		// load IE css support
		if( $this->CI->agent->browser() == 'Internet Explorer' && $this->CI->agent->version() < 8 )
			$this->CI->sprinkle->load('ie-css');

		// load assets
		foreach( $this->_assets as $asset_info )
			$this->CI->sprinkle->add_asset($asset_info);

		$_scripts = $this->CI->sprinkle->output('js');

		// set inline javascript
		$this->inline_js($this->make_js_vars());

		if( !empty($this->_inline_js) )
			foreach($this->_inline_js as $tag)
				$_scripts .= $tag;

		$template['stylesheets'] = $this->CI->sprinkle->output('css');
		$template['scripts'] = $_scripts;

		$template['stylesheets'] = preg_replace('~\t([^\n]+)~', '$1', $template['stylesheets']);
		$template['scripts'] = preg_replace('~\t([^\n]+)~', '$1', $template['scripts']);

		// set template breadcrumb
		$template['breadcrumb'] = $this->_breadcrumb;

		// set template body content
		$template['body'] = $this->_load_view($view, $parse, $this->_data);

		// build all regions
		$this->build_regions();

	}

	/**
	 * Prepare all regions
	 *
	 * @access private
	 * @return object $this
	 */
	private function build_regions(){

		if( !isset($this->_regions) )
			$this->_regions = array();

		foreach( $this->_regions as $region => $vars )
		{

			if( isset($vars['view']) )
				$this->_data['_template']['regions'][$region] = $this->_load_view($vars['view'], $vars['parse'], $this->_data + $vars['data']);

		}

		// set theme regions
		foreach( $this->configs['default_regions'] as $default_region )
			$this->_data['_template']['regions'][$default_region] = $this->_render($this->_find_theme_region($default_region), $this->_data);

		return $this;
	}

	/**
	 * render template file
	 * 
	 * @access public
	 * @param string $template
	 * @param array $data
	 * @return object
	 */

	private function _render($template, $data = array()){

		return $this->_twig->render($template, $data);

	}

	/**
	 * find a view in modules or theme folder
	 * 
	 * @access public
	 * @param string $view
	 * @param bool $is_template (default=FALSE)
	 * @return string
	 */

	private function _find_view($view, $is_template = FALSE){

		list($module, $view) = (strpos($view, '/') !== FALSE) ? explode('/', $view) : array($this->_module, $view);

		$theme_views_path = $this->current_theme_dir() . implode('/', array(
			'views',
			'modules',
			$module,
		));

		$module_views_path = APPPATH . implode('/', array(
			'modules',
			$module,
			'views'
		));

		if( file_exists($theme_views_path . '/' . $view . $this->configs['templates_extension']) )
			return implode('/', array('views', 'modules', $module, $view . $this->configs['templates_extension']));
		
		if( $is_template )
		{
			
			if( file_exists($module_views_path . '/' . $view . $this->configs['templates_extension']) )
				return implode('/', array($module, 'views', $view . $this->configs['templates_extension']));
			else
				show_error(array(
					sprintf('Unable to find view: "%s"', $view),
					sprintf('Looked into: [%s], [%s]', $module_views_path, $theme_views_path)
				));
		}
		else
		{

			$find_view = Modules::find($view, $module, 'views/');

			if( !empty($find_view[0]) )
				return implode('/', array($module, $view));
			else
				show_error(array(
					sprintf('Unable to find view: "%s"', $view),
					sprintf('Looked into: [%s], [%s]', $module_views_path, $theme_views_path)
				));

		}

	}

	/**
	 * Load view and get content
	 * 
	 * @access public
	 * @param mixed $view
	 * @param bool $parse (default=FALSE)
	 * @param array $data
	 * @return string
	 */

	private function _load_view($view, $parse = FALSE, array $data){
		
		if( $this->CI->router->fetch_module() == 'errors' )
			return $this->CI->load->view('errors/error_general' , $this->_data, true);

		$view_path = $this->_find_view($view, $parse);
		$is_template = pathinfo($view_path, PATHINFO_EXTENSION) == pathinfo($this->configs['templates_extension'], PATHINFO_EXTENSION);

		if( !$is_template )
			$output = $this->CI->load->view($view_path, $data, TRUE );
		else
			$output = $this->_render($view_path, $data);

		return $output;

	}

	/**
	 * find a region
	 * 
	 * @access public
	 * @param string $region
	 * @return string
	 */

	private function _find_theme_region($region){

		if( !file_exists($this->current_theme_dir() . 'views/regions/' . $region . $this->configs['templates_extension']))
				show_error('unable to find region: ' . $region . '<br />looked into: ' . $this->current_theme_dir() . 'regions/');

		return 'views/regions/' . $region . $this->configs['templates_extension'];

	}

	private function is_theme_region($region){

		return file_exists($this->current_theme_dir() . 'views/regions/' . $region . $this->configs['templates_extension']);

	}

	/**
	 * load twig environment
	 * 
	 * @access private
	 * @return void
	 */
	private function setup_twig(){

		// register twig class
		if( !class_exists('Twig_Autoloader') )
		{
			
			require dirname(__FILE__) . '/Twig/Autoloader.php';
			Twig_Autoloader::register();

		}
		
		// set twig Twig_Loader_Filesystem
		$twig_filesystem = new Twig_Loader_Filesystem(array(
			$this->current_theme_dir(),
			APPPATH . 'modules/'
		));

		// set twig environment
		$this->_twig = new Twig_Environment($twig_filesystem, $this->configs['twig']['environment']);

		// register default functions/filters
		foreach( $this->configs['twig']['autoload'] as $type => $register )
		{

			if( $type == 'functions' )
				foreach( $register as $function )
					$this->register_function($function);

			if( $type == 'filters' )
				foreach( $register as $filter )
					$this->register_filter($filter);

		}

	}

	/**
	 * get current theme dir
	 * 
	 * @access private
	 * @return string
	 */
	private function current_theme_dir(){

		return $this->configs['themes_dir'] .
			   (strpos(strrev($this->configs['themes_dir']), '/') === 0 ? '' : '/') .
			   $this->current_theme() .
			   '/';

	}

	/**
	 * get current theme
	 * 
	 * @access private
	 * @return string
	 */
	private function current_theme(){

		return (isset($this->_theme) ? $this->_theme : $this->configs['default_theme']);

	}

	/**
	 *  Prepare metadata tags
	 *
	 * @access private
	 * @return string
	 */
	private function _get_metadata(){

		// define proccesed tags
		$processed_metadata = array();

		// define open graph/rdfa metatags
		$rdfa_tags = array();

		// define link tags
		$link_tags = array();
		
		// define allowed tags
		$type_tags = array(
			'meta', 'link', 'og', 'dc'
		);
		
		// process all tags
		foreach( $this->_metadata as $type => $metadata )
		{

			// allowed tag?
			if( !in_array($type, $type_tags) )
				continue;

			foreach( $metadata as $data )
			{	

				// process open graph/rdfa metatags before
				if( $type == 'og' || $type == 'dc' )
				{
					
					if( !isset($data['property']) )
						continue;

					if( strpos($data['property'], 'og:') !== FALSE ||  strpos($data['property'], 'dc:') !== FALSE)
						$data['property'] = substr($data['property'], 3);

					if ( !isset($rdfa_tags[$type]) )
						$rdfa_tags[$type] = array();

					$rdfa_tags[$type][] = array('property' => $data['property'], 'content' => $data['content']);
					continue;

				}

				// also process link tags before
				if( $type == 'link' )
				{
					$link_tags[] = $data;
					continue;
				}

				// set tag attributes
				$attributes = array();
				foreach( $data as $attr => $content )
					$attributes[] = $attr . '="' . $content . '"';

				// add tag to list
				$processed_metadata[] = '<' . $type . ' ' . implode(' ', $attributes) . ' />';
			}

		}

		// process og metatags now
		foreach( $rdfa_tags as $type => $node )
		{

			foreach($node as $attr)
				$processed_metadata[] = '<meta property="' . $type . ':' . $attr['property'] . '" ' . 'content="' . $attr['content'] . '" />';

		}

		// process link tags now
		foreach( $link_tags as $data )
		{

			$link_attributes = array();
			
			foreach( $data as $attr => $content )
				$link_attributes[] = $attr . '="' . $content . '"';
			
			$processed_metadata[] = '<link ' . implode(' ', $link_attributes) . ' />';

		}

		// output all tags as string
		return "\t" . implode("\n\t", $processed_metadata);

	}

	/**
	 *  Prepare page title
	 *
	 * @access private
	 * @return string
	 */
	private function _get_segmented_title(){

		$this->CI->load->helper('inflector');

		return 'Spirate 3.0';

	}

	/**
	 * get current base template
	 * 
	 * @access private
	 * @return string
	 */

	private function get_base_template(){

		$template = isset($this->_template) ? $this->_template : $this->configs['default_base_template'];

		return 'views/templates/' . $template . $this->configs['templates_extension'];

	}

	/**
	 * get file extension
	 *
	 * @access private
	 * @param string $file
	 * @return string
	 */
	private function _ext($file){
		return pathinfo($file, PATHINFO_EXTENSION) ? '' : EXT;
	}

}

// END Template class
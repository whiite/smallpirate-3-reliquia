<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Spirate
 *
 * Social linksharing script
 *
 * @package		Spirate
 * @author		Spirate team
 * @license		http://spirate.net/license.txt
 * @link		http://spirate.net
 * @version		3.0
 */

/**
 * Spirate Scraper library
 *
 * This class enables you to scrap webpages
 *
 * @package		Spirate
 * @subpackage	Libraries
 * @subpackage	WebBrowser 
 * @subpackage	simple_html_dom
 * @category  	Libraries
 * @author		Spirate Team
 * @author		CubicleSoft
 * @version		1.0
 */

define('CACHE_PAGES_DIR', dirname(__FILE__) . '/scraper/cache');

require_once(dirname(__FILE__) . '/scraper/http.php' );
require_once(dirname(__FILE__) . '/scraper/web_browser.php' );
require_once(dirname(__FILE__) . '/scraper/emulate_curl.php' );
require_once(dirname(__FILE__) . '/scraper/simple_html_dom.php' );

/* ------------------------------------------------------------------------ */

class Scraper{

	protected $html;
	protected $htmldomloaded = FALSE;
	protected $browser;
	protected $result = FALSE;
	protected $is_processed = FALSE;
	private $CI = NULL;

	public function __construct(){

		// get instace
		$this->CI =& get_instance();

		// load cache driver
		$this->CI->load->driver('cache');

		// set simple_html_dom instance
		$this->html = new simple_html_dom();

		// set WebBrowser instance
		$this->browser = new WebBrowser();

	}

	public function process($url = '', $cache = FALSE){

		if( $cache && ($cache_data = $this->CI->cache->file->get('scraper-cache-' . md5($url))) === FALSE )
		{

			// process url
			$this->result = $cache_data = $this->browser->Process($url);

			// escape html
			$cache_data['body'] = $this->_html_to_cache_string($this->result['body']);

			// create new cache file
			$this->CI->cache->file->save('scraper-cache-' . md5($url), $cache_data, 3600);

		}
		else
		{

			// unescape cached html
			$cache_data['body'] = $this->_prepare_cached_html($cache_data['body']);
			
			// set cached result
			$this->result = $cache_data;

		}

		// error occurs? return false
		if( isset( $this->result['error'] ) )
			return FALSE;

		// url was processed
		$this->is_processed = TRUE;

		// return object, chaining!
		return $this;

	}

	public function get_content(){

		if( !$this->is_processed )
			return FALSE;

		return $this->result['body'];

	}

	public function get_metatags(){

		if( !$this->is_processed )
			return FALSE;

		$this->_load_html_dom();

		$metatags = array();
		$DOM_metatags = $this->html->find('meta');

		foreach($DOM_metatags as $tag )
		{

			if( !isset($tag->name) )
				continue;

			$metatags[$tag->name] = $tag->content;

		}

		return $metatags;

	}

	public function get_title(){

		$this->_load_html_dom();

		$title = $this->html->find('title', 0);

		return $title->innertext;

	}

	public function get_response_code(){

		if( !$this->is_processed )
			return FALSE;

		return $this->result['response']['code'];

	}

	private function _load_html_dom(){

		if( $this->htmldomloaded )
			return FALSE;

		$this->html->load($this->result['body']);

	}

	private function _prepare_cached_html($html){

		$html = stripslashes($html);

		$html = strtr($html, array(
			'&lquo;' => '<',
			'&rquo;' => '>'
		));

		return $html;

	}
	private function _html_to_cache_string($html){

		$html = preg_replace(array('~\n*~', '~\s+~'), array('', ' '), trim($html));

		$html = strtr($html, array('"' => '\"', '<' => '&lquo;', '>' => '&rquo;'));

		return $html;

	}

}
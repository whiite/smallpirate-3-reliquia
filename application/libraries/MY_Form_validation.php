<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation{

	protected $CI;
	private static $token_name = 'form_token';
	private static $token;
	public $_field_data = array();
	public $token_generated = FALSE;

	/**
	 * The model class to call with callbacks
	 */
	private $_model;
	
	public function __construct($rules = array())
	{
		parent::__construct($rules);
		$this->CI =& get_instance();
	}
	
	public function generate_token(){
		
		// generate a token
		self::$token = md5(uniqid() . microtime() . rand());

		// store token in session
		$this->CI->session->set_userdata(self::$token_name, self::$token);

		// token now generated
		$this->token_generated = TRUE;

	}

	public function validate_token(){

		// get token post data
		$token_input = $this->CI->input->post(self::$token_name);

		if( $token_input === FALSE || empty($token_input) || $token_input != $this->CI->session->userdata(self::$token_name) )
			return FALSE;

		return TRUE;

	}

	public function destroy_token(){

		$this->CI->session->set_userdata(self::$token_name, NULL);
		$this->token_generated = FALSE;

	}

	public function get_token_name(){
		return self::$token_name;
	}

	public function get_token_hash(){
		return self::$token;
	}

	public function set_field_postdata($field, $data)
	{

		$this->_field_data[$field]['postdata'] = $data;

	}

	
	
	// --------------------------------------------------------------------

	/**
	 * Alpha-numeric with underscores dots and dashes
	 *
	 * @param	string
	 * @return	bool
	 */
	public function alpha_dot_dash($str)
	{
		return (bool) preg_match("/^([-a-z0-9_\-\.])+$/i", $str);
	}

	// --------------------------------------------------------------------

	/**
	 * Sneaky function to get field data from
	 * the form validation library
	 *
	 * @param	string
	 * @return	bool
	 */
	public function field_data($field)
	{
		return (isset($this->_field_data[$field])) ? $this->_field_data[$field] : null;
	}
	// --------------------------------------------------------------------

	/**
	 * Formats an UTF-8 string and removes potential harmful characters
	 *
	 * @param	string
	 * @return	string
	 * @todo	Find decent regex to check utf-8 strings for harmful characters
	 */
	public function utf8($str)
	{
		// If they don't have mbstring enabled (suckers) then we'll have to do with what we got
		if ( ! function_exists('mb_convert_encoding'))
		{
			return $str;
		}

		$str = mb_convert_encoding($str, 'UTF-8', 'UTF-8');

		return htmlentities($str, ENT_QUOTES, 'UTF-8');
	}

	// --------------------------------------------------------------------

	/**
	 * Sets the model to be used for validation callbacks. It's set dynamically in MY_Model
	 *
	 * @param	string	The model class name
	 * @return	void
	 */
	public function set_model($model)
	{
		if ($model)
		{
			$this->_model = strtolower($model);
		}
	}

    /**
     * Set Error Message
     *
     * Lets users set their own error messages on the fly.  Note:  The key
     * name has to match the  function name that it corresponds to.
     *
     * @access      public
     * @param       string
     * @param       string
     * @param       string
     * @return      string
     */
    function set_message($lang, $val = '', $field = 'default')
    {
            $this->_error_messages[$lang][$field] = array('message' => $val);
    }

    /**
	 * Executes the Validation routines
	 *
	 * Modified to work with HMVC -- Phil Sturgeon
	 * Modified to work with callbacks in the calling model -- Jerel Unruh
	 *
	 * @param	array
	 * @param	array
	 * @param	mixed
	 * @param	integer
	 * @return	mixed
	 */
	protected function _execute($row, $rules, $postdata = null, $cycles = 0)
	{
		// If the $_POST data is an array we will run a recursive call
		if (is_array($postdata))
		{
			foreach ($postdata as $key => $val)
			{
				$this->_execute($row, $rules, $val, $cycles);
				$cycles++;
			}

			return;
		}

		// --------------------------------------------------------------------

		// If the field is blank, but NOT required, no further tests are necessary
		$callback = false;
		if ( ! in_array('required', $rules) and is_null($postdata))
		{
			// Before we bail out, does the rule contain a callback?
			if (preg_match("/(callback_\w+(\[.*?\])?)/", implode(' ', $rules), $match))
			{
				$callback = true;
				$rules = (array('1' => $match[1]));
			}
			else
			{
				return;
			}
		}

		// --------------------------------------------------------------------

		// Isset Test. Typically this rule will only apply to checkboxes.
		if (is_null($postdata) and $callback == false)
		{
			if (in_array('isset', $rules, true) or in_array('required', $rules))
			{
				// Set the message type
				$type = (in_array('required', $rules)) ? 'required' : 'isset';

				if ( ! isset($this->_error_messages[$type][$row['field']]))
				{
					if (isset($this->_error_messages[$type]['default'])) {

						$line = $this->_error_messages[$type]['default']['message'];

					}

					elseif (FALSE === ($line = $this->CI->lang->line($type)))
					{
						$line = 'The field was not set';
					}
				}
				else
				{
					$line = $this->_error_messages[$type][$row['field']]['message'];
				}

				// Build the error message
				$message = sprintf($line, $this->_translate_fieldname($row['label']));

				// Save the error message
				$this->_field_data[$row['field']]['error'] = $message;

				if ( ! isset($this->_error_array[$row['field']]))
				{
					$this->_error_array[$row['field']] = $message;
				}
			}

			return;
		}

		// --------------------------------------------------------------------

		// Cycle through each rule and run it
		foreach ($rules as $rule)
		{
			$_in_array = false;

			// We set the $postdata variable with the current data in our master array so that
			// each cycle of the loop is dealing with the processed data from the last cycle
			if ($row['is_array'] == true and is_array($this->_field_data[$row['field']]['postdata']))
			{
				// We shouldn't need this safety, but just in case there isn't an array index
				// associated with this cycle we'll bail out
				if ( ! isset($this->_field_data[$row['field']]['postdata'][$cycles]))
				{
					continue;
				}

				$postdata = $this->_field_data[$row['field']]['postdata'][$cycles];
				$_in_array = true;
			}
			else
			{
				$postdata = $this->_field_data[$row['field']]['postdata'];
			}

			// --------------------------------------------------------------------

			// Is the rule a callback?
			$callback = false;
			if (substr($rule, 0, 9) == 'callback_')
			{
				$rule = substr($rule, 9);
				$callback = true;
			}

			// Strip the parameter (if exists) from the rule
			// Rules can contain a parameter: max_length[5]
			$param = false;
			if (preg_match("/(.*?)\[(.*)\]/", $rule, $match))
			{
				$rule	= $match[1];
				$param	= $match[2];
			}

			// Call the function that corresponds to the rule
			if ($callback === true)
			{
				// first check in the controller scope
				if (method_exists(CI::$APP->controller, $rule))
				{
					$result = call_user_func(array(new CI::$APP->controller, $rule), $postdata, $param);
				}
				// it wasn't in the controller. Did MY_Model specify a valid model in use?
				elseif ($this->_model)
				{
					// moment of truth. Does the callback itself exist?
					if (method_exists(CI::$APP->{$this->_model}, $rule))
					{
						$result = call_user_func(array(CI::$APP->{$this->_model}, $rule), $postdata, $param);
					}
					else
					{
						throw new Exception('Undefined callback '.$rule.' Not found in '.$this->_model);
					}
				}
				else
				{
					throw new Exception('Undefined callback "'.$rule.'" in '.CI::$APP->controller);
				}

				// Re-assign the result to the master data array
				if ($_in_array == true)
				{
					$this->_field_data[$row['field']]['postdata'][$cycles] = (is_bool($result)) ? $postdata : $result;
				}
				else
				{
					$this->_field_data[$row['field']]['postdata'] = (is_bool($result)) ? $postdata : $result;
				}

				// If the field isn't required and we just processed a callback we'll move on...
				if ( ! in_array('required', $rules, true) and $result !== false)
				{
					continue;
				}
			}
			else
			{
				if ( ! method_exists($this, $rule))
				{
					// If our own wrapper function doesn't exist we see if a native PHP function does.
					// Users can use any native PHP function call that has one param.
					if (function_exists($rule))
					{
						$result = $rule($postdata);

						if ($_in_array == true)
						{
							$this->_field_data[$row['field']]['postdata'][$cycles] = (is_bool($result)) ? $postdata : $result;
						}
						else
						{
							$this->_field_data[$row['field']]['postdata'] = (is_bool($result)) ? $postdata : $result;
						}
					}
					else
					{
						log_message('debug', "Unable to find validation rule: ".$rule);
					}

					continue;
				}

				$result = $this->$rule($postdata, $param);

				if ($_in_array == true)
				{
					$this->_field_data[$row['field']]['postdata'][$cycles] = (is_bool($result)) ? $postdata : $result;
				}
				else
				{
					$this->_field_data[$row['field']]['postdata'] = (is_bool($result)) ? $postdata : $result;
				}
			}

			// Did the rule test negatively?  If so, grab the error.
			if ($result === false)
			{
				if ( ! isset($this->_error_messages[$rule][$row['field']]))
				{
					if (isset($this->_error_messages[$rule]['default']))
					{
						$line = $this->_error_messages[$rule]['default']['message'];
					}
					elseif (FALSE === ($line = $this->CI->lang->line($rule)))
					{
						$line = 'Unable to access an error message corresponding to your field name.';
					}
				}
				else
				{
					$line = $this->_error_messages[$rule][$row['field']]['message'];
				}

				// Is the parameter we are inserting into the error message the name
				// of another field?  If so we need to grab its "field label"
				if (isset($this->_field_data[$param]) and isset($this->_field_data[$param]['label']))
				{
					$param = $this->_translate_fieldname($this->_field_data[$param]['label']);
				}

				// Build the error message
				$message = sprintf($line, $this->_translate_fieldname($row['label']), $param);

				// Save the error message
				$this->_field_data[$row['field']]['error'] = $message;

				if ( ! isset($this->_error_array[$row['field']]))
				{
					$this->_error_array[$row['field']] = $message;
				}

				return;
			}
		}
	}

}
/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */
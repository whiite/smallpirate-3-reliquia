<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function js_var_type($var, $output_match = false){
	
	$types = array('string', 'integer', 'boolean', 'array');
	$type = in_array(gettype($var), $types) ? gettype($var) : 'undefined';

	if( $type !== 'array' )
	{
		
		$check_var = trim(preg_replace(array('~\s+~', '/\r|\n|\t/'), array(' ', ''), $var));

		// is function
		if(	preg_match('#((function|[\w\$]{1,})\(((\s?[\w\$]{1,}[\, ]?)*)\))#i', $check_var, $match) )
			$type = 'function';

		// is Jquery or Object function
		else if ( preg_match('#([\$\w]{1,}\s?\([^\)]+\)(\.[\w\$]+\s?\([^$.]+)*\;?$)#i', $check_var, $match))
			$type = 'function';
		
		else if( preg_match(
			'#\{' . // match first "{"
			'(\s?[a-z0-9_"\']+\s?:' . // match method
			'\s?(["\'][^"\']+["\']|true|false|[0-9\.]+|function\(((\s?[\w\$]{1,}[\, ]?)*)\)\{[^\}]*\})[\, ]?)' . // match value
			'*\s?\}#i', // match last "}"
		$check_var, $match) )
			$type = 'object';

	}
	
	if( $output_match && !empty($match) )
		return array('type' => $type, 'match' => $match);
	else
		return $type;
}

function format_var($var, $type = false){

	$type = $type ? $type : js_var_type($var);

	if( $type == 'undefined' )
		return 'false';

	$format = '';

	switch($type){

		case 'string':
			$format = "'%s'";
		break;

		case 'function':
		case 'object':

			$var = preg_replace('~^\s+|\s+$~', '', $var);
			$lines = preg_split('~\n~', $var);
			$var = '';

			foreach( $lines as $line )
			{
				
				if( $line != $lines[count($lines)-1] && $lines[0] != $line )
					$line = preg_replace('~\t{5,}~', "", $line);

				if( $lines[0] != $line && end($lines) != $line )
					$var .= "\t";

				$var .= $line;

				if( end($lines) != $line )
					$var .= "\n \t";
			
			}

			$format = '%s';

		break;

		case 'boolean':
			$format = '%s';
		break;

		case 'integer':
			$format = '%d';
		break;

		case 'array':
			$var = json_encode($var);
			$format = '%s';
		break;

	}

	if( $type == 'boolean' )
		$var = $var ? 'true' : 'false';

	if( $type == 'integer' )
		$var = (int) $var;

	$var = sprintf($format, $var);

	return $var;
}

function output_script_tag($code, $format = false){

	$script_tag = "\t \t<script type=\"text/javascript\">\n";
	$lines = preg_split('~\n~', $format ? format_var($code) : $code);
	
	foreach( $lines as $line )
	{

		$script_tag .= "\t \t \t$line";

		if( end($lines) != $line )
			$script_tag .= "\n ";
		else
			$script_tag .= " \t \n";
	}
	
	$script_tag = preg_replace('~\s*$~', '', $script_tag);

	$script_tag .= "\n \t \t</script>\n";

	return $script_tag;
}


?>
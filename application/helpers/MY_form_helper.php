<?php

function form_open($action = '', $attributes = '', $hidden = array())
{
	$CI =& get_instance();

	if ($attributes == '')
	{
		$attributes = 'method="post"';
	}

	// If an action is not a full URL then turn it into one
	if ($action && strpos($action, '://') === FALSE)
	{
		$action = $CI->config->site_url($action);
	}

	// If no action is provided then set to the current url
	$action OR $action = $CI->config->site_url($CI->uri->uri_string());

	$form = '<form action="'.$action.'"';

	$form .= _attributes_to_string($attributes, TRUE);

	$form .= '>';

	// Add CSRF field if enabled, but leave it out for GET requests and requests to external websites	
	if ($CI->form_validation->token_generated === TRUE AND ! (strpos($action, $CI->config->base_url()) === FALSE OR strpos($form, 'method="get"')))	
	{
		$hidden[$CI->form_validation->get_token_name()] = $CI->form_validation->get_token_hash();
	}
	
	if (is_array($hidden) AND count($hidden) > 0)
	{
		$form .= sprintf("<div style=\"display:none\">%s</div>", form_hidden($hidden));
	}

	return $form;
}
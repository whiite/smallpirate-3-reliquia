<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @package semantic_url
 * @subpackage helpers
 * @author Spirate dev team
 * @since 3.0
 */

function semantic_url($string, $suffix = '.html'){
	
	// get original accents and chars from string
	$string = strtr($string, array_flip (get_html_translation_table ( HTML_SPECIALCHARS | HTML_ENTITIES)));
	$output = '';
	
	// if is path, convert each segment
	if(strpos($string, '/') !== false)
	{
		
		$path = explode('/', $string);
		
		foreach($path as $key => $segment ){
			
			if( empty($segment) )
				continue;
			$keys = array_keys($path);
			$output .= semantic_url($segment, false) . (end($keys) != $key ? '/' : '');
			
		}
		
		return base_url($output . ($suffix ? $suffix : ''));
	}
	
	// set allowed characters <http://tools.ietf.org/html/rfc3986>
	$allowed = array_merge(
		range('0', '9'),
		range('a', 'z'),
		range('A', 'Z'),
		array('-', '_', '~', '.', ',')
	);
	
	// preg_match need a string
	$allowed = implode('', $allowed);
	
	// remove unicode letters accents ( thanks to wordpress )
	$output = remove_accents($string);
	
	// char by char
	$chars = str_split($string);
	foreach($chars as $char)
	{
		if( !preg_match('~' . preg_quote($char) . '~', $allowed) )
			$output = preg_replace('~' .preg_quote($char) . '~', '-', $output);
	}
	
	// replace spaces and excess characters
	$output = preg_replace(
		array('~\s~', '~-+~', '~^-*|-*$~', '~\.*$~'),
		array('-', '-', '', ''),
		$output
	);
	
	// add suffix
	$output = $output . ($suffix ? $suffix : '');
	
	// output
	return $output;
	
}

?>
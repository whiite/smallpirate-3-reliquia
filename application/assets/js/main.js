/* Main.js */

// !? TODO:
// - amd system
// - integrate require.js

/* set main functions/objects */

// lang object
var lang = lang || {};

// main object
var sp = {
	
	init : function(){
		
		// hover icons
		$('.icon.hover-state').parent().hover(function(){
			$(this).find('.icon').addClass('active');
		}, function(){
			$(this).find('.icon').removeClass('active');
		});

		// load placeholders
		$('input, textarea').placeholder();
		
		// load tipsy context
		$('.tt-tipsy').tipsy({'gravity' : 's'});
		
		// load dropdowns
		$('.dropdown-area').each(function(){
		
			sp.dialog({
				'target' : this,
				'bind' : 'hover',
				'body' : $($(this).attr('target-dropdown')).html(),
				'dropdown' : true
			});
		
		});
		
		// set register button onclick event
		$('a#register').click(function(){
			
			sp.modal.set({
				'title' : 'Registro',
				'body' : $('#register-form').html(),
				'buttons' : [
					{
						'label' : 'Siguiente',
						'callback' : function(){
							
							sp.modal.alert('Bienvenido!', 'Has sido registrado exitosamente').set_class('success');
							
							
						}
					},
					{
						'label' : 'Cancelar',
						'class' : 'clear',
						'callback' : 'close'
					}
				]
			}).show();

			return false;
			
		});
		
		// set user functions
		
		if( user.logged_in )
		{
			
			var
			user_options = [
				{
					'target' : '#uicon_notifications',
					'body' : ' ',
					'bind' : 'click',
					'width' : 150,
					'position' : 'center'
				},				
				{
					'target' : '#uicon_messages',
					'body' : ' ',
					'bind' : 'click',
					'width' : 150,
					'position' : 'center'
				},
				{
					'target' : '#uicon_bookmarks',
					'body' : ' ',
					'bind' : 'click',
					'width' : 150,
					'position' : 'center'
				},
				{
					'target' : '#menu_username',
					'obj' : '#user-dropdown',
					'bind' : 'hover',
					'dropdown' : true,
					'width' : 150,
					'padding-y' : 17,
					'position' : 'center'
				}
			];

			for( var option in user_options)
				sp.dialog(user_options[option]);

			/*
			sp.notifications.settings({
				'cookie' : true,
				'favicon-alert' : true
			}).start([
				notifications : {
					'receive_url' : global.base_url + 'notifications/get',
					'post_data' : {},
					'alert_bubble' : ['#uicon_notifications', true, true],
					'count' : 10
				},
				messages : {
					'receive_url' : global.base_url + 'messages/get',
					'post_data' : {},
					'count' : 3
				}
			]).
			*/
			// !!! calculcar posicion para las burbujas
			sp.bubble_alerts.init({
			'notifications' : {
			    'parent' : '#uicon_notifications',
			    'count' : user.notifications,
			    'limit' : 99,
			    'animate' : true,
			    'anim-counter' : true
			},
			'messages' : {
			    'parent' : '#uicon_messages',
			    'count' : user.messages,
			    'limit' : 30,
			    'animate' : true,
			    'anim-counter' : true
			}}).show();

		}
		else
			sp.dialog({
				'target' : 'a#login',
				'body' : $('#login-form').html(),
				'position' : 'center'
			});
		
	}, /* end sp.__ */

	tooltip : {

		id_count : 0,
		tooltips : {},
		tooltips_by_target : {},
		types : ['info', 'error', 'success', 'warning'],
		template : '<div class="validation-tooltip {{class}}" tt-id="{{id}}"><i></i><span><strong><em>{{msg}}</em></strong></span></div>',

		set : function(){

			var
			type = arguments[0],
			msg = arguments[1],
			settings = typeof arguments[2] != 'object' ? {} : arguments[2],
			tooltip_key = (new Array('tooltip', type, this.id_count+1)).join('-'),
			tt_obj = null,
			tt_class = [],
			default_settings = {
				'position' : 'north left',
				'fade' : false,
				'animate' : false,
				'exact' : false
			};

			if( $.inArray(type, this.types) === -1 )
				type = 'info';

			tt_class.push(type);

			for ( var setting in default_settings ){

				var value;

				if( typeof settings[setting] == 'undefined' )
					value = default_settings[setting];
				else
					value = settings[setting];

				if( setting == 'position' ){

					value = value.split(' ');

					var pos1 = value[0];
					var pos2 = typeof value[1] == 'undefined' ? 'left' : value[1];
					
					if( $.inArray(pos1, new Array('south', 'east', 'west', 'north')) === -1 ){
						pos1 = 'north';
					}

					if( pos1 == 'east' || pos1 == 'west' ){
						
						if( $.inArray(pos2, new Array('top', 'bottom', 'middle')) === -1 )
							pos2 = 'top';

					}

					else if( $.inArray(pos2, new Array('left', 'right', 'middle')) === -1 )
						pos2 = 'left';

					settings[setting] = new Array();

					settings[setting][0] = pos1;
					settings[setting][1] = pos2;

					if( pos1 == 'north' )
						continue;

					tt_class.push(pos1);

				}

				if( setting == 'fade' || setting == 'animate' )
					settings[setting] = value ? true : false;

			}

			tt_class = tt_class.join(' ');

			tt_obj = $(Mustache.render(this.template, {'msg' : msg, 'class' : tt_class, 'id' : tooltip_key}));

			this.id_count++;

			// add tooltip to list
			this.tooltips[tooltip_key] = {
				'tmpl' : tt_obj,
				'key' : tooltip_key,
				'msg' : msg,
				'type' : type,
				'settings' : settings
			};

			return array_merge(this, {'actual_tt' : this.tooltips[tooltip_key]});
		},

		show : function(){

			var tooltip;

			// directly called
			if( $.inArray('actual_tt', array_keys(this)) === -1 )
			{

				if (arguments.length === 0)
				{
					return console.warn('debes ingresar el id del tooltip');
				}

				if ( typeof arguments[0] == 'object' ){

					if( $(arguments[0]).hasAttr('tt-id') && $.inArray($(arguments[0]).hasAttr('tt-id'), array_keys(this.tooltips)) )
						tooltip = this.tooltips[$(arguments[0]).hasAttr('tt-id')];

				}

				else if( typeof this.tooltips_by_target[arguments[0]] != 'undefined' )
					tooltip = this.tooltips_by_target[arguments[0]];
				else
					return console.warn('No proporcionaste el id del tooltip o no existe');
				

			}
			else
				tooltip = this.actual_tt;

			$(tooltip.obj).show();

			var top_calc, left_calc;

			// left position
			left_calc = $(tooltip.target).offset().left ;
				
			// calculate top position default NORTH
			top_calc = ($(tooltip.target).offset().top - ($(tooltip.obj)[0].offsetHeight+$(tooltip.obj).find('i')[0].offsetHeight));
			
			// south position invert all
			if( tooltip.settings['position'][0] == 'south' )
			{
				top_calc = ($(tooltip.target).offset().top + $(tooltip.obj)[0].offsetHeight + $(tooltip.obj).find('i')[0].offsetHeight);
				top_calc += $(tooltip.target).height();
			}

			if( tooltip.settings['position'][0] == 'north' || tooltip.settings['position'][0] == 'south')
			{

				// middle position?
				if ( tooltip.settings['position'][1] == 'middle' )
					left_calc = left_calc + $(tooltip.target)[0].offsetWidth/2;

				// right position?
				else if ( tooltip.settings['position'][1] == 'right' ){
					
					left_calc = $(tooltip.target).offset().left + $(tooltip.target)[0].offsetWidth;
					
					if ( !tooltip.settings['exact'] )
						left_calc -=  ($(tooltip.obj).find('i').position().left + $(tooltip.obj).find('i')[0].offsetWidth);

				}

				// align to arrow position
				if (tooltip.settings['exact'])
					left_calc = left_calc - $(tooltip.obj).find('i')[0].offsetWidth - $(tooltip.obj).find('i').position().left/2

			}

			if( tooltip.settings['position'][0] == 'west' || tooltip.settings['position'][0] == 'east' ){

				if( tooltip.settings['position'][1] == 'top' )
					top_calc = $(tooltip.target).offset().top - (tooltip.settings['exact'] ? $(tooltip.obj)[0].offsetHeight/2 : 0)

				if( tooltip.settings['position'][1] == 'bottom' )
					top_calc = $(tooltip.target).offset().top + $(tooltip.target)[0].offsetHeight - (tooltip.settings['exact'] ? $(tooltip.obj)[0].offsetHeight/2 : $(tooltip.obj)[0].offsetHeight)

				if( tooltip.settings['position'][1] == 'middle' )
					top_calc = $(tooltip.target).offset().top + $(tooltip.target)[0].offsetHeight/2 - $(tooltip.obj)[0].offsetHeight/2;
				
				if( tooltip.settings['position'][0] == 'west' )
					left_calc -= $(tooltip.obj)[0].offsetWidth - $(tooltip.obj).find('i')[0].offsetWidth;

				if( tooltip.settings['position'][0] == 'east' )
					left_calc += $(tooltip.target)[0].offsetWidth + $(tooltip.obj).find('i')[0].offsetWidth;

			}

			$(tooltip.obj).css({
				'top' : top_calc,
				'left' : left_calc
			});

			var _this = array_merge(this, {'actual_tt' : tooltip});

			return _this;
		},
		
		hide : function(){

			if( arguments[0] == 'all' ){
				$('.validation-tooltip').hide();
				return this;
			}

			var tooltip;

			// directly called
			if( $.inArray('actual_tt', array_keys(this)) === -1 )
			{

				if (arguments.length === 0)
				{
					return console.warn('debes ingresar el id del tooltip');
				}

				if ( typeof arguments[0] == 'object' ){

					if( $(arguments[0]).hasAttr('tt-id') && $.inArray($(arguments[0]).hasAttr('tt-id'), array_keys(this.tooltips)) )
						tooltip = this.tooltips[$(arguments[0]).hasAttr('tt-id')];

				}

				else if( typeof this.tooltips_by_target[arguments[0]] != 'undefined' )
					tooltip = this.tooltips_by_target[arguments[0]];
				else
					return console.warn('No proporcionaste el id del tooltip o no existe');
				

			}
			else
				tooltip = this.actual_tt;

			$(tooltip.obj).hide();

		},

		destroy : function(){

			if( arguments[0] == 'all' )
			{

				$('.validation-tooltip').remove();

				this.tooltips = {};
				this.tooltips_by_target = {};
				this.id_count = 0;

				return this;
			}

			var tooltip;

			// directly called
			if( $.inArray('actual_tt', array_keys(this)) === -1 )
			{

				if (arguments.length === 0)
				{
					return console.warn('debes ingresar el id del tooltip');
				}

				if ( typeof arguments[0] == 'object' ){

					if( $(arguments[0]).hasAttr('tt-id') && $.inArray($(arguments[0]).hasAttr('tt-id'), array_keys(this.tooltips)) )
						tooltip = this.tooltips[$(arguments[0]).hasAttr('tt-id')];

				}

				else if( typeof this.tooltips_by_target[arguments[0]] != 'undefined' )
					tooltip = this.tooltips_by_target[arguments[0]];
				else
					return console.warn('No proporcionaste el id del tooltip o no existe');
				

			}
			else
				tooltip = this.actual_tt;

			$(tooltip.obj).remove();
			delete this.tooltips[tooltip.key];
			delete this.tooltips_by_target[tooltip.target];

		},

		to : function(target){

			if( $(target).length == 0 )
				return this;

			$('body').append(this.actual_tt.tmpl);

			var tt_obj = $('body').find('.validation-tooltip[tt-id=' + this.actual_tt.key + ']');
			
			this.tooltips[this.actual_tt.key]['obj'] = tt_obj;
			this.tooltips[this.actual_tt.key]['target'] = target;
			this.tooltips_by_target[target] = this.tooltips[this.actual_tt.key];

			return array_merge(this, {'actual_tt' : this.actual_tt});

		}

	}, /* end sp.tooltip */
	
	modal : {
		
		dialog			: false,
		opened			: false,
		loading			: null,
		configured		: false,
		settings		: {},
		parent_dialog	: {},
		dialog_settings	: {},
		
		set : function(settings){
			
			var dialog_settings = {
					'modal' : true,
					'buttons' : [],
					'draggable' : false,
					'resizable' : false,
					'width' : 370,
					'dialogClass' : 'sp-modal',
					'position' : 'absolute'
				},
				settings_callbacks = {
					'body' : 'set_body',
					'title' : 'set_title',
					'height' : 'set_height',
					'width' : 'set_width'
				},
				previous_callbacks = {};
			
			// set default settings
			this.settings = {
				'no-close' : false,
				'animate-close' : false,
				'buttons-class' : 'sp-button'
			}
			
			if( sp.modal.configured === true )
				return this;
			
			this.dialog = $('<div />').attr('id', 'sp-dialog');
				
			for( var key in settings ){
				
				var value = settings[key];
				
				/* modal settings */
				
				// booleans
				if( $.inArray(key, new Array('no-close', 'animate-close')) !== -1 )
				{
					
					sp.modal.settings[key] = Boolean(value);
					
					continue;
				}
				
				// callbacks
				if( $.inArray(key, new Array('after-close', 'before-close', 'before-load', 'after-load')) !== -1 )
				{
					
					if(typeof val == 'string' && sp.modal._is_modal_function(value)){
						
						var internal_callback = value;
						
						value = function(){
							
							sp.modal[internal_callback]();
							
						}
						
					}
					
					dialog_settings[key] = value;
					continue;
				}
				
				/* dialog settings */
				
				// overlay
				if( key == 'overlay' )
				{
					dialog_settings['modal'] = Boolean(value);
					continue;
				}

				// position
				if( key == 'position' )
				{

					if( $.inArray(value, new Array('fixed', 'absolute')) === -1 )
						sp.modal.settings['position'] = 'absolute';
					else
						sp.modal.settings['position'] = value;

				}
				
				// make buttons
				if( key == 'buttons' )
				{
					
					var button_settings = new Array();
					
					for( var button in value ){
						
						var settings_length = array_keys(value[button]).length,
							index = 0;
						
						for( var setting in value[button]){
							
							var val = value[button][setting];
							
							if( setting == 'label' )
								button_settings['label'] = val;
							
							if( setting == 'callback' ){
								
								if( typeof val == 'string' ){
									
									// match function
									var function_regexp, func_match, func, args, arguments = new Array();
									
									function_regexp = new RegExp(/([a-zA-Z_]+)(\(([\,]?[a-zA-Z0-9]+[^\)]+)\))?/);
									func_match = (val.replace(/\s+/g, '')).match(function_regexp);
									
									if( func_match !== null )
									{
										
										func = func_match[1];

										/*
										args_source = typeof func_match[3] !== 'undefined' ? explode(',', func_match[3]) : '';
										
										if (args_source != '')
											for(var arg in args_source){ 
												
												if( !isNaN(parseInt(args_source[arg]))  || (args_source[arg] == 'true' || args_source[arg] == 'false') )
													arguments.push(args_source[arg]);
												else
													arguments.push('"' + args_source[arg] + '"');
												
											}
									
										arguments = implode(',', arguments);
										// IE BUG ( argggh )
										*/
										
										if( sp.modal._is_modal_function(func) )
											val = sp.modal[func];

									}
								}
								
								button_settings['callback'] = val;
								
							}
							
							if( setting == 'class' )
								button_settings['class'] = val;
							
							index++;
							
							if( index >= settings_length )
							{
								
								dialog_settings['buttons'].push({
									
									text : button_settings['label'],
									'class' : implode(' ', new Array(
											sp.modal.settings['buttons-class'],
											button_settings['class']
										)),
									click : button_settings['callback']
								});
								
								index = 0;
							}
						}
					}
				}
				
				if( $.inArray(key, array_keys(settings_callbacks)) !== -1 )
				{
					
					if( $.inArray(key, sp.modal.settings) !== -1 )
						sp.modal.settings[key] = value;
					
					// call this functions when dialog is called
					previous_callbacks[settings_callbacks[key]] = value;
					
				}
				else
					continue;
					
			}
			
			// make $.dialog settings
			this.dialog_settings = dialog_settings;
			
			// initialize modal
			$(this.dialog ).dialog(this.dialog_settings).dialog('close');
			
			
			// set and get .ui-dialog
			this.parent_dialog = $(this.dialog).parents().filter('.ui-dialog');
			
			// set close event
			$(sp.modal.dialog).dialog({
				close : function(){
					sp.modal.close();
				}
			});
			
			// wrap close button
			$('<div />')
			.addClass('close-button clearfix rounded-top-right')
			.appendTo($(this.parent_dialog).find('.ui-dialog-titlebar'));
			
			$(this.parent_dialog)
			.find('.ui-dialog-titlebar-close')
			.remove();
			
			$('<a />')
			.addClass('ui-dialog-titlebar-close')
			.bind('click', function(){
				
				$(sp.modal.dialog).dialog('close');
				
			})
			.appendTo($(this.parent_dialog).find('.ui-dialog-titlebar > .close-button'));
			
			// change panel buttons
			$(this.parent_dialog).find('.ui-dialog-buttonpane button').each(function(i, event){
				
				var event_onclick = jQuery._data($(this)[0]).events.click[0].handler;
				
				var new_button = $(this).setElementType('a');
					new_button = new_button[0][0];
				
				$(new_button).bind('click', function(){
					
					event_onclick();
					
				});
				
			});
			
			// previous callbacks
			for( var func in previous_callbacks ){
				
				var arg = previous_callbacks[func];
				
				if( func == 'set_width' || func == 'set_height' )
					sp.modal[func](arg, true);
				else
					sp.modal[func](arg);
				
			}
			
			// okay, modal is configured
			sp.modal.configured = true;
			
			// chaining object
			return this;
			
		},
		
		alert : function(title, body, callback){
			
			if( sp.modal.opened )
				sp.modal.close();
			
			sp.modal.set({
				buttons :[
					{
						'label' : lang.buttons.ok,
						'class' : '',
						'callback' : typeof callback == 'function' ? callback : 'close'
					}
				],
				'title' : title,
				'body' : body,
			}).show();
			
			// chaining object
			return sp.modal;
		},
		
		confirm : function(title, body, callback){
			
			if( sp.modal.opened )
				sp.modal.close();
			
			if( typeof callback != 'function' )
				return sp.modal;
			
			sp.modal.set({
				buttons :[
					{
						'label' : lang.buttons.ok,
						'class' : '',
						'callback' : callback
					},
					{
						'label' : lang.buttons.cancel,
						'class' : 'clear',
						'callback' : 'close'
					}
				],
				'title' : title,
				'body' : body,
			}).show();
			
			// chaining object
			return sp.modal;
			
		},
		
		show : function(){
			
			if( !sp.modal.configured )
			{
				console.log("modal wasn't configured")	
				return false;
			}
			
			// dialog is hidden
			if( sp.modal.opened === true )
			{
				$(sp.modal.dialog ).dialog('open');
				return sp.modal;
			}
			
			/* settings */
			
			// close button
			if( sp.modal.settings['no-close'] )
				$(sp.modal.dialog ).parents().filter('.ui-dialog').find('.close-button').remove();
			
			// titlebar
			if( sp.modal.settings['title'] === false )
				$(sp.modal.parent_dialog).find('.ui-dialog-titlebar').hide();
			
			// show dialog
			$(sp.modal.dialog ).dialog('open');

			// set position
			$(sp.modal.parent_dialog).css('position', sp.modal.settings['position']);
			
			sp.modal.opened = true;
			
			sp.modal.center();
			
			// chaining object
			return sp.modal;
		},
		
		close : function(callback){
			
			// animate on close?
			if( sp.modal.settings['animate-close'] )
				$(sp.modal.dialog).dialog('option', 'hide', 'fade');
			
			// destroy dialog after close
			$(sp.modal.dialog).dialog({
				close : function(){
					
					$(sp.modal.dialog).dialog('destroy');
					$(sp.modal.parent_dialog ).remove();
					
					if( typeof callback == 'function' )
						setTimeout(function(){
							callback();
						}, 100);
				}
			});
			
			$(sp.modal.dialog).dialog('close');
			
			// set settings to default
			sp.modal.configured = false;
			sp.modal.opened = false;
			sp.modal.loading = null;
			sp.modal.settings = {};
			sp.modal.dialog_settings = {};
			
			// chaining object
			return sp.modal;
		},
		
		center : function(){
			
			$(sp.modal.dialog).dialog("option", "position", "center");

			var topPosition = ($(sp.modal.parent_dialog).css('top')).replace(/px$/, '')
			
			if(  parseFloat(topPosition) < 21 )
				$(sp.modal.parent_dialog).css('top', 30);
			
			// chaining object
			return sp.modal;
		},
		
		set_title : function(title){
			
			$(sp.modal.dialog).dialog('option', 'title', title)
			
			// chaining object
			return sp.modal;
		},
		
		set_body : function(body){
			
			$(sp.modal.dialog).html(body);
			
			// chaining object
			return sp.modal;
			
		},
		
		set_class : function(_class){
			
			var classes = new Array('info', 'error', 'success');
			
			if( $.inArray(_class, classes) === -1 )
				return sp.modal;
			
			for( var CLASS in classes )
				$(sp.modal.parent_dialog).removeClass(classes[CLASS]);
			
			$(sp.modal.parent_dialog).addClass(_class);
				
			// chaining object
			return sp.modal;
		},
		
		set_width : function(width, fromCore, animate){

			if(width == '100%')
				width = $('body').width()-100;

			if ( width > $(document).width() )
				width = $(document).width()-100
			
			var animate_left = $(document).width()/2 - width/2;

			$(sp.modal.parent_dialog).find('.load-overlay').css({
				'width' : width,
				'height' : $(sp.modal.parent_dialog).find('.load-wrap')[0].offsetHeight
			});
						
			if( !fromCore && animate)
			{
				$(sp.modal.parent_dialog).animate({'width' : width, 'left' : animate_left}, 500, function(){ sp.modal.center(); });
			}
			else
				$(sp.modal.parent_dialog).css('width', width);

			this.center();

			// chaining object
			return sp.modal;

		},

		set_height : function(height, fromCore, animate){

			if( !fromCore && animate)
				$(sp.modal.dialog).animate({'height' : height}, 500);
			else
				$(sp.modal.dialog).css('height', height);

			this.center();

			// chaining object
			return sp.modal;

		},
		
		load : function(callback){
			
			if( !sp.modal.opened )
				sp.modal.show();
			
			if( sp.modal.loading )
				return false;
			
			if( sp.modal.loading === false )
			{
				
				$(sp.modal.parent_dialog).find('.load-overlay').fadeIn(function(){
					
					if( typeof callback == 'function')
						callback();
						
					sp.modal.loading = true;
					
				});
				
				return sp.modal;
			}
			
			// create wrap
			var load_wrap = $('<div />').addClass('load-wrap'),
				overlay = $('<div />').addClass('load-overlay rounded').hide();
			
			// insert wrap
			$(load_wrap).insertBefore($(sp.modal.parent_dialog).find('.ui-dialog-content'));
			
			// move dialog title
			$(sp.modal.parent_dialog).find('.ui-dialog-titlebar').appendTo(load_wrap);
			
			// move dialog content
			$(sp.modal.parent_dialog).find('.ui-dialog-content').appendTo(load_wrap);
			
			// move dialog buttons
			$(sp.modal.parent_dialog).find('.ui-dialog-buttonpane').appendTo(load_wrap);
			
			// insert overlay
			var w_wrap = $(load_wrap)[0].offsetWidth,
				h_wrap = $(load_wrap)[0].offsetHeight;
			
			$(overlay).appendTo(load_wrap).css({
				'width' : w_wrap,
				'height' : h_wrap,
			}).fadeIn(function(){
			
				if( typeof callback == 'function')
					callback();
					
			});

			sp.modal.loading = true;
			
			// chaining object
			return sp.modal;			
		},
		
		stop_load : function(callback){
			
			if( !sp.modal.loading )
				return false;

			$(sp.modal.parent_dialog).find('.load-overlay').fadeOut(function(){
				
				if( typeof callback == 'function')
					callback();
				
			});

			sp.modal.loading = false;
			
			// chaining object
			return sp.modal;
		},
		
		shake : function(callback){
			
			$(sp.modal.parent_dialog).effect( "shake", 'fast', function(){
				
				if( typeof callback == 'function' )
					callback();
				
			});
			
			// chaining object
			return sp.modal;
		},
		
		/* internals */
		_is_modal_function : function(func){
			
			var modal_methods = array_keys(sp.modal),
				modal_functions = new Array();
			
			for( var method in modal_methods )
			{
				
				// private functions
				if( modal_methods[method].substr(0, 1) == '_' )
					continue;
				
				if( typeof sp.modal[modal_methods[method]] == 'function')
					modal_functions.push(modal_methods[method]);
				
				else continue;
				
			}
			
			return ($.inArray(func, modal_functions) !== -1);
			
		}

	}, /* end sp.modal */

	notifications : {

		get_notifications : function(){



		}

	}, /* end sp.notifications */

	/*
	
		sp.bubble_alerts.init({
		'notifications' : {
		    'parent' : '#uicon_notifications',
		    'count' : 1,
		    'animate' : true
		},
		'messages' : {
		    'parent' : '#uicon_messages',
		    'count' : 3,
		    'animate' : true
		}}).show()

	 */

	bubble_alerts : {

		bubbles : {},
		configs : {},
		bubble_template : '<div class="bubble_notifications"{{bubble_attr}}><span><strong>{{count}}</strong></span></div>',
		enable_tinycon : true,
		bubbles_count : 0,
		
		init : function(items){

			for( var item in items )
			{

				var 
				
				settings = {
					'parent' : [true, null],
					'count' : [false, 0],
					'animate' : [false, false],
					'anim-counter' : [false, false],
					'limit' : [false, 99],
					'position' : [false, 'NE'],
					'distance' : [false, [-6, -9]]
				},
				bubble_coords = {'NE' : ['top', 'right'], 'NO' : ['top', 'left'], 'SE' : ['bottom', 'right'], 'SO' : ['bottom', 'left']},
				bubble_id = item,
				bubble_configs = items[item];

				if( typeof this.bubbles[bubble_id] != 'undefined' )
					continue;

				/* welcome dear bubble :3 */
				this.bubbles[bubble_id] = {};

				// make settings for each bubble
				for( var config in settings )
				{

					var is_necessary = settings[config][0],
						default_value = settings[config][1],
						value = typeof bubble_configs[config] == 'undefined' ? default_value : bubble_configs[config];
					
					if( config == 'parent' )
					{

						if( $(value).length > 1 )
							value = $(value).get(0);

						else if ($(value).length == 0)
							value = null;

						else
							value = $(value);

						// bubble need a good place
						if( !empty(value) )
							$(value).css('position', 'relative');

					}

					if( config == 'position' )
					{

						value = value.toUpperCase().substring(0, 2);

						if( $.inArray(value, bubble_coords) === -1 )
							value = bubble_coords[default_value];
						else
							value = bubble_coords[value];

					}
					
					if( config == 'distance' )
					{
						
						var distances;

						if( typeof value == 'string' )
							distances = explode(' ', value.replace(/\s+/g, ' ').replace(/^\s*|\s*$/g, ''));
						else
							distances = value;

						if( distances.length == 0 )
							distances = explode(' ', default_value);
						else if( distances.length < 2 )
							distances.push(0);

						distances = array_slice(distances, 0, 2);
						distances = $.map(distances, function(n){ return parseInt(n) });

						value = distances;
						
					}
					
					// required setting is empty?
					if( empty(bubble_configs[config]) && is_necessary )
						continue;
					
					// check for defaults settings
					else if ( !bubble_configs[config] && empty(value) )
						bubble_configs[config] = default_value;
					
					// or... maybe custom setting
					else
						bubble_configs[config] = value;
					
				}

				this.bubbles_count += bubble_configs['count'];
				
				var bubble_tmp = Mustache.render(this.bubble_template, {
					'count' : bubble_configs['anim-counter'] ? 0 : bubble_configs['count'],
					'bubble_attr' : ' bubble_id="' + bubble_id + '"'
				});
				
				this.bubbles[bubble_id]['options'] = bubble_configs;
				this.bubbles[bubble_id]['obj'] = $(bubble_configs['parent'])
					.append($(bubble_tmp))
					.find('[bubble_id]');
				
				this.set_position();
				
				$(bubble_configs['parent']).find('[bubble_id]').hide();

				if( this.enable_tinycon )
					Tinycon.setBubble(this.bubbles_count > 99 ? '99' : this.bubbles_count);
				
			}
			
			return this;
			
		},
		
		set_position : function(bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			$(bubbles).each(function(k, b){

				var bubble = sp.bubble_alerts.bubbles[b],
					make_positions = {};

				$.each([0, 1], function(i){

					var position = bubble.options.position[i];
					var distance = bubble.options.distance[i];

					// !? position absolute, calc offset top and left from bubble.obj

					make_positions[position] = distance
					
				});
				
				$(bubble.obj).css(make_positions);

			});

			return this;

		},

		show : function (bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			for ( var b in bubbles )
			{

				if( typeof this.bubbles[bubbles[b]] == 'undefined' )
				{
					console.warn('bubble notification "' + bubbles[b] + '" not defined');
					continue;
				}

				var bubble = this.bubbles[bubbles[b]];

				$(bubble.obj).show();

				this.set_position(bubbles[b]);

				if( bubble.options.count == 0 || bubble.options.count === false)
				{
					$(bubble.obj).hide();
					continue;
				}

				if( bubble.options.animate )
				{
					
					var anim_dir = bubble.options.position[0] == 'top' ? '+=' : '-=',
						anim_dir2 = anim_dir.substr(0, 1) == '+' ? '-=' : '+=',
						anim_options = {'before' : {}, 'after' : {}},
						current_bubble_key = bubbles[b];

					anim_options['before'][bubble.options.position[0]] = anim_dir + '5px';
					anim_options['after'][bubble.options.position[0]] = anim_dir2 + '5px';

					$(bubble.obj).animate(anim_options['before'], 100, function(){

						$(this).animate(anim_options['after'], 100);

					});

				}

				if( bubble.options['anim-counter'] )
					sp.bubble_alerts.animate_counter(current_bubble_key);

			}

			return this;

		},

		hide : function(bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			for ( var b in bubbles )
			{

				if( typeof this.bubbles[bubbles[b]] == 'undefined' )
				{
					console.warn('bubble notification "' + bubbles[b] + '" not defined');
					continue;
				}

				var bubble = this.bubbles[bubbles[b]];

				if( bubble.options.animate )
				{
					
					var anim_dir = bubble.options.position[0] == 'top' ? '+=' : '-=',
						anim_dir2 = anim_dir.substr(0, 1) == '+' ? '-=' : '+=',
						anim_options = {'before' : {}, 'after' : {}};

					anim_options['before'][bubble.options.position[0]] = anim_dir2 + '5px';
					anim_options['after'][bubble.options.position[0]] = anim_dir + '5px';

					$(bubble.obj).animate(anim_options['before'], 100, function(){
						$(this).animate(anim_options['after'], 100, function(){
							$(this).hide();
						});
					});

				}
				else
					$(bubble.obj).hide();

			}

			return this;

		},

		animate_counter : function(bubbles){

			bubbles = this.__get_bubbles_ids(bubbles);

			if( !bubbles )
				return;

			for ( var b in bubbles )
			{

				var bubble = this.bubbles[bubbles[b]],
					ul_counter = $('<ul />').css('position', 'relative'),
					current_pos = 0,
					total_numbers = 0,
					duration;
				
				if( bubble.options.count == 0 )
					continue;
				
				for ( var i = 0; i <= bubble.options.count; i++)
				{
					
					var number = i.toString(),
						li;

					if( number.length < (bubble.options.count).toString().length )
						number = (str_repeat('0', (bubble.options.count > bubble.options.limit ? bubble.options.limit : bubble.options.count).toString().length - number.length) + i);

					li = $('<li />').html(number);
					
					$(ul_counter).append(li)

					if( i == bubble.options.limit )
						break;

				}
				
				$(bubble.obj)
				.find('span > strong')
				.empty()
				.css('overflow-y', 'hidden')
				.append(ul_counter);

				total_numbers = $(bubble.obj).find('span strong ul li').length;
				
				$(bubble.obj).find('span strong ul li').each(function(i, obj){
			    	
			    	if( i == ($(bubble.obj).find('span strong ul li').length)-1 )
						return false;

			    	current_pos -= ($(this)[0].offsetHeight);
			    	duration = total_numbers-i <= 5 ? 300 : 30;
			    	
			    	// animate counter
				    $(bubble.obj).find('span ul').animate({
				        'top' : current_pos
				    }, duration, function(){
				    	
				    	// define last element
						var last_number = $(bubble.obj).find('span ul li').last();
						
						// set last element effects
						if ( i == (total_numbers-2) )
						{
							
							// remove all numbers except the last.
							$(bubble.obj).find('span strong').empty().html($('<i />').html($(last_number).html()));
							
							if( bubble.options.count > bubble.options.limit )
							{
								$(bubble.obj).find('span strong')
								.append('<i class="plus" />')
								.find('i.plus')
								.html('+')
								.hide()
								.fadeIn('fast');
							}

						}

				    });
				
				});

			}

			return this;

		},

		__get_bubbles_ids : function(bubbles, from){
			
			if( empty(bubbles) && !empty(this.bubbles) )
				return array_keys(this.bubbles);

			else if ( empty(bubbles) )
				return false;
			
			bubbles = explode(',', bubbles.replace(/\,[\s*]/g, ',').replace(/^\s*|\s*$/g, ''));

			return bubbles;

		}
	
	}, /* end sp.bubble_alerts */

	_regexp_utils : {
		'url' : /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	}
	
	/* core internals */

	/* end core internals */
};

(function(sp){

sp.dialog = function( options ) {
	
	options = options || {};
	
	if( !options.target || $(options.target).length == 0 )
	{
		console.warn('dialog:', 'target not defined.');
		return false;
	}
	
	var dialog = {
		
		settings : {},
		dialog : {},
		target : {},
		positions: {},
		
		show : function(callback){
			
			$('body > .ui-dialog-context').hide();
			
			if( this.settings['animate'] ){
				
				$(this._dialog).fadeIn(500, function(){
					if( typeof callback == 'function' )
						callback.apply(sp.dialog);
				});

				this.set_position();
				
			}
			else
			{
				$(this._dialog).show();
				
				this.set_position();
				
				if( typeof callback == 'function' )
						callback.apply(sp.dialog);
			}
			
			$(this._dialog).show();
			
			return this;
		},
		
		close : function(){
			
			$(this._dialog).hide();
			
			return this;
		},
		
		bind_dialog : function(){
			
			var dialog = this,
				_dialog = this._dialog,
				target = this.target,
				custom_events = (this.settings['bind'][0] == 'dialog::show');
			
			if( this.settings['bind'] == 'none' )
				return false;
			
			$(custom_events ? this.target.add(this._dialog) : target).bind(this.settings['bind'][0], function(){
				
				dialog.show.apply(dialog);

				if( this.nodeName == 'A' )
					return false;
				
			});
			
			$(custom_events ? this.target.add(this._dialog) : _dialog).bind(this.settings['bind'][1], function(_event){
				
				if( dialog.settings['bind'][0] != 'dialog::show' )
				{
					if( $(_event.target)[0] === $(dialog.target)[0] )
						return;
					
					if( $(this).css('display') == 'none' )
						return;
					
					if( $(_event.target).parents().filter(dialog.target)[0] === $(dialog.target)[0] )
						return;
				}
				
				dialog.close.apply(dialog);
				
			});
			
		},
		
		set_position : function(){
			
			var make_positions = {'my' : [], 'at' : []},
				positions = this.positions,
				target = this.target,
				dialog_borders = (
					parseInt($(this._dialog).find('.sp-dialog .dialog').css('border-left-width'))
					+
					parseInt($(this._dialog).find('.sp-dialog .dialog').css('border-right-width'))
				),
				center_of_target = (
					($(target)[0].offsetWidth/2) // target width
					-20 // arrow dialog width
					-dialog_borders // dialog borders width
					+this.settings['padding-x'] // custom padding-x
				),
				padding_y = this.settings['padding-y'].toString(),
				pos_x = (padding_y.substr(1) == '-' ? '-' + padding_y : '+' + padding_y);
			
			center_of_target = center_of_target.toString();
			center_of_target = center_of_target.substring(0, 1) == '-' ? '+' + center_of_target.substr(1) : '-' + center_of_target;
			
			for( var pos in positions )
			{
				if( pos == 'my' ){
					
					//positions[pos][1] = (positions[pos][1] + pos_x);
					if( positions['my'][1] == 'bottom' || positions['my'][1] == 'top' )
						$(this._dialog).css('padding-top', this.settings['padding-y']);

					if( positions[pos][0] == 'center' ){
						positions[pos][0] = 'right' + center_of_target;

					}

				}
				
				make_positions[pos] = positions[pos];
			}

			$(this._dialog).position({
				my : implode(' ', make_positions['my']),
				at : implode(' ', make_positions['at']),
				of : $(target)
			})


		}
	};
		
		var default_template = '<div class="ui-dialog-context">\
		<div class="sp-dialog">\
			<i class="arrow"></i>\
		<div class="dialog rounded">\
			{{#dropdown}}\
			<ul class="ui-dropdown dialog-content">\
			{{{body}}}\
			</ul>\
			{{/dropdown}}\
			{{^dropdown}}\
			<div class="dialog-body dialog-content">\
			{{{body}}}\
			</div>\
			{{/dropdown}}\
		</div>\
		</div>\
		</div>';

		var defaults = {
			'bind' : 'click',
			'animate' : false,
			'padding-y' : 10,
			'padding-x' : 0,
			'position' : 'right top right bottom',
			'dropdown' : false,
			'obj' : false,
			'template' : default_template
		},
		_settings = {
			'body' : options['obj'] ? false : true,
			'template' : default_template,
			'target' : true,
			'bind' : false,
			'animate' : false,
			'padding-x' : false,
			'padding-y' : false,
			'position' : false,
			'dropdown' : false,
			'obj' : false,
			'width' : false,
			'height' : false
		},
		bind_events = {
			'click' : ['click', 'clickoutside'],
			'hover' : ['mouseover', 'mouseoveroutside'],
			'custom': ['dialog::show', 'dialog::close']
		};
	
	// prepare settings
	for ( var s in _settings )
	{
		
		var setting = s;
		
		if( !empty(options[setting]) )
			dialog.settings[setting] = options[setting];
			
		else if( $.inArray(setting, array_keys(defaults)) !== -1 )
			dialog.settings[setting] = defaults[setting];
			
		else
			dialog.settings[setting] = null;
		
	}
	
	// check settings
	for( setting in dialog.settings)
	{
		
		var value = dialog.settings[setting];
		
		if( value === null && _settings[setting] === true ){
			console.warn('dialog:', setting + ' not defined.');
			return false;
		}
		
		if( setting == 'target' )
			dialog.target = $(value);
		
		// set body dialog
		if( setting == 'body' )
		{
			
			if( dialog.settings['obj'] )
				value = $(dialog.settings.obj).html();
			console.log(dialog.settings);
			// template to html object
			var wrapper = $('<div />')
			.attr('id', 'dialog-wrapper')
			.append(Mustache.to_html(dialog.settings.template, {'body' : value, 'dropdown' : dialog.settings['dropdown']})).prependTo('body');
			
			dialog._dialog = $(wrapper).find('.ui-dialog-context').css({
				'position' : 'absolute',
				'left' : -99999,
				'top' : -99999
			}).clone();
			
			$('body').prepend(dialog._dialog);
			$(wrapper).remove();
				
		}
		
		if( setting == 'position' )
		{
			var coords = explode(' ', value),
				default_coords = explode(' ', defaults['position']);
			
			for( var coord in default_coords)
			{
				var coord_option = (coord < 2 ? 'my' : 'at');
				
				if(typeof dialog.positions[coord_option] == 'undefined')
					dialog.positions[coord_option] = new Array();
				
				if( coords[coord] )
					dialog.positions[coord_option].push(coords[coord]);
				else
					dialog.positions[coord_option].push(default_coords[coord]);
			}
		}
		
		if( setting == 'bind' )
		{
			
			if( $.inArray(value, array_keys(bind_events))  !== -1 )
				dialog.settings['bind'] = bind_events[dialog.settings['bind']];
			else if( value === false )
				dialog.settings['bind'] = 'none';
			
		}
		
	}
	
	// set width and height
	if( dialog.settings.width )
		$(dialog._dialog).find('.dialog-content').css('width', dialog.settings.width);
		
	if( dialog.settings.height )
		$(dialog._dialog).find('.dialog-content').css('height', dialog.settings.height);
	
	// position near to target
	dialog.set_position.apply(dialog);
	
	// hide dialog. awaiting event
	$(dialog._dialog).hide();
	
	// bind event to show dialog
	if( dialog.settings['bind'] !== false )
		dialog.bind_dialog.apply(dialog);
	
	return dialog;
	
}
})( sp );

// load core when document is ready
$(document).ready(function(){
	sp.init()
});


/*********************
 * JQuery PLUGINS
 *********************/

/* Jquery Tipsy */
;(function(e){function t(e,t){return typeof e=="function"?e.call(t):e}function n(e){while(e=e.parentNode){if(e==document)return true}return false}function r(t,n){this.$element=e(t);this.options=n;this.enabled=true;this.fixTitle()}r.prototype={show:function(){var n=this.getTitle();if(n&&this.enabled){var r=this.tip();r.find(".tipsy-inner")[this.options.html?"html":"text"](n);r[0].className="tipsy";r.remove().css({top:0,left:0,visibility:"hidden",display:"block"}).prependTo(document.body);var i=e.extend({},this.$element.offset(),{width:this.$element[0].offsetWidth,height:this.$element[0].offsetHeight});var s=r[0].offsetWidth,o=r[0].offsetHeight,u=t(this.options.gravity,this.$element[0]);var a;switch(u.charAt(0)){case"n":a={top:i.top+i.height+this.options.offset,left:i.left+i.width/2-s/2};break;case"s":a={top:i.top-o-this.options.offset,left:i.left+i.width/2-s/2};break;case"e":a={top:i.top+i.height/2-o/2,left:i.left-s-this.options.offset};break;case"w":a={top:i.top+i.height/2-o/2,left:i.left+i.width+this.options.offset};break}if(u.length==2){if(u.charAt(1)=="w"){a.left=i.left+i.width/2-15}else{a.left=i.left+i.width/2-s+15}}r.css(a).addClass("tipsy-"+u);r.find(".tipsy-arrow")[0].className="tipsy-arrow tipsy-arrow-"+u.charAt(0);if(this.options.className){r.addClass(t(this.options.className,this.$element[0]))}if(this.options.fade){r.stop().css({opacity:0,display:"block",visibility:"visible"}).animate({opacity:this.options.opacity})}else{r.css({visibility:"visible",opacity:this.options.opacity})}}},hide:function(){if(this.options.fade){this.tip().stop().fadeOut(function(){e(this).remove()})}else{this.tip().remove()}},fixTitle:function(){var e=this.$element;if(e.attr("title")||typeof e.attr("original-title")!="string"){e.attr("original-title",e.attr("title")||"").removeAttr("title")}},getTitle:function(){var e,t=this.$element,n=this.options;this.fixTitle();var e,n=this.options;if(typeof n.title=="string"){e=t.attr(n.title=="title"?"original-title":n.title)}else if(typeof n.title=="function"){e=n.title.call(t[0])}e=(""+e).replace(/(^\s*|\s*$)/,"");return e||n.fallback},tip:function(){if(!this.$tip){this.$tip=e('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>');this.$tip.data("tipsy-pointee",this.$element[0])}return this.$tip},validate:function(){if(!this.$element[0].parentNode){this.hide();this.$element=null;this.options=null}},enable:function(){this.enabled=true},disable:function(){this.enabled=false},toggleEnabled:function(){this.enabled=!this.enabled}};e.fn.tipsy=function(t){function i(n){var i=e.data(n,"tipsy");if(!i){i=new r(n,e.fn.tipsy.elementOptions(n,t));e.data(n,"tipsy",i)}return i}function s(){var e=i(this);e.hoverState="in";if(t.delayIn==0){e.show()}else{e.fixTitle();setTimeout(function(){if(e.hoverState=="in")e.show()},t.delayIn)}}function o(){var e=i(this);e.hoverState="out";if(t.delayOut==0){e.hide()}else{setTimeout(function(){if(e.hoverState=="out")e.hide()},t.delayOut)}}if(t===true){return this.data("tipsy")}else if(typeof t=="string"){var n=this.data("tipsy");if(n)n[t]();return this}t=e.extend({},e.fn.tipsy.defaults,t);if(!t.live)this.each(function(){i(this)});if(t.trigger!="manual"){var u=t.live?"live":"bind",a=t.trigger=="hover"?"mouseenter":"focus",f=t.trigger=="hover"?"mouseleave":"blur";this[u](a,s)[u](f,o)}return this};e.fn.tipsy.defaults={className:null,delayIn:0,delayOut:0,fade:false,fallback:"",gravity:"n",html:false,live:false,offset:0,opacity:.8,title:"title",trigger:"hover"};e.fn.tipsy.revalidate=function(){e(".tipsy").each(function(){var t=e.data(this,"tipsy-pointee");if(!t||!n(t)){e(this).remove()}})};e.fn.tipsy.elementOptions=function(t,n){return e.metadata?e.extend({},n,e(t).metadata()):n};e.fn.tipsy.autoNS=function(){return e(this).offset().top>e(document).scrollTop()+e(window).height()/2?"s":"n"};e.fn.tipsy.autoWE=function(){return e(this).offset().left>e(document).scrollLeft()+e(window).width()/2?"e":"w"};e.fn.tipsy.autoBounds=function(t,n){return function(){var r={ns:n[0],ew:n.length>1?n[1]:false},i=e(document).scrollTop()+t,s=e(document).scrollLeft()+t,o=e(this);if(o.offset().top<i)r.ns="n";if(o.offset().left<s)r.ew="w";if(e(window).width()+e(document).scrollLeft()-o.offset().left<t)r.ew="e";if(e(window).height()+e(document).scrollTop()-o.offset().top<t)r.ns="s";return r.ns+(r.ew?r.ew:"")}}})(jQuery);

/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
;(function($,c,b){$.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "),function(d){a(d)});a("focusin","focus"+b);a("focusout","blur"+b);$.addOutsideEvent=a;function a(g,e){e=e||g+b;var d=$(),h=g+"."+e+"-special-event";$.event.special[e]={setup:function(){d=d.add(this);if(d.length===1){$(c).bind(h,f)}},teardown:function(){d=d.not(this);if(d.length===0){$(c).unbind(h)}},add:function(i){var j=i.handler;i.handler=function(l,k){l.target=k;j.apply(this,arguments)}}};function f(i){$(d).each(function(){var j=$(this);if(this!==i.target&&!j.has(i.target).length){j.triggerHandler(e,[i.target])}})}}})(jQuery,document,"outside");

/*! jQuery Mustache - v0.2.8 - 2013-06-23
* https://github.com/jonnyreeves/jquery-Mustache
* Copyright (c) 2013 Jonny Reeves; Licensed MIT */

/*global jQuery, window */
;(function(e,t){"use strict";function s(){if(r===null){r=t.Mustache;if(r===void 0){e.error("Failed to locate Mustache instance, are you sure it has been loaded?")}}return r}function o(e){return n[e]!==void 0}function u(t,r){if(!i.allowOverwrite&&o(t)){e.error("TemplateName: "+t+" is already mapped.");return}n[t]=e.trim(r)}function a(){var t;if(arguments.length===0){t=e('script[type="'+i.domTemplateType+'"]').map(function(){return this.id})}else{t=e.makeArray(arguments)}e.each(t,function(){var t=document.getElementById(this);if(t===null){e.error("No such elementId: #"+this)}else{u(this,e(t).html())}})}function f(e){var t=n[e];delete n[e];return t}function l(){n={};s().clearCache()}function c(t,r){if(!o(t)){if(i.warnOnMissingTemplates){e.error("No template registered for: "+t)}return""}return s().to_html(n[t],r,n)}function h(t,n){return e.ajax({url:t,dataType:i.externalTemplateDataType}).done(function(t){e(t).filter("script").each(function(t,n){u(n.id,e(n).html())});if(e.isFunction(n)){n()}})}function p(){return e.map(n,function(e,t){return t})}var n={},r=null,i={warnOnMissingTemplates:false,allowOverwrite:true,domTemplateType:"text/html",externalTemplateDataType:"text"};e.Mustache={options:i,load:h,has:o,add:u,addFromDom:a,remove:f,clear:l,render:c,templates:p,instance:r};e.fn.mustache=function(t,n,r){var i=e.extend({method:"append"},r);var s=function(n,r){e(n)[i.method](c(t,r))};return this.each(function(){var t=this;if(e.isArray(n)){e.each(n,function(){s(t,this)})}else{s(t,n)}})}})(jQuery,window);

;(function(n,t,i){"use strict";function it(n,t){t&&t.onError&&t.onError(n)===!1||(this.name="JsRender Error",this.message=n||"JsRender error")}function o(n,t){var i;n=n||{};for(i in t)n[i]=t[i];return n}function ct(n,t,i){return(!k.rTag||arguments.length)&&(a=n?n.charAt(0):a,v=n?n.charAt(1):v,f=t?t.charAt(0):f,h=t?t.charAt(1):h,w=i||w,n="\\"+a+"(\\"+w+")?\\"+v,t="\\"+f+"\\"+h,l="(?:(?:(\\w+(?=[\\/\\s\\"+f+"]))|(?:(\\w+)?(:)|(>)|!--((?:[^-]|-(?!-))*)--|(\\*)))\\s*((?:[^\\"+f+"]|\\"+f+"(?!\\"+h+"))*?)",k.rTag=l+")",l=new RegExp(n+l+"(\\/)?|(?:\\/(\\w+)))"+t,"g"),et=new RegExp("<.*>|([^\\\\]|^)[{}]|"+n+".*"+t)),[a,v,f,h,w]}function ei(n,t){t||(t=n,n=i);var e,f,o,u,r=this,s=!t||t==="root";if(n){if(u=r.type===t?r:i,!u)if(e=r.views,r._.useKey){for(f in e)if(u=e[f].get(n,t))break}else for(f=0,o=e.length;!u&&f<o;f++)u=e[f].get(n,t)}else if(s)while(r.parent.parent)u=r=r.parent;else while(r&&!u)u=r.type===t?r:i,r=r.parent;return u}function lt(){var n=this.get("item");return n?n.index:i}function oi(n,t){var u,f=this,r=t&&t[n]||(f.ctx||{})[n];return r=r===i?f.getRsc("helpers",n):r,r&&typeof r=="function"&&(u=function(){return r.apply(f,arguments)},o(u,r)),u||r}function si(n,t,u){var h,f,o,e=+u===u&&u,s=t.linkCtx;return e&&(u=(e=t.tmpl.bnds[e-1])(t.data,t,r)),o=u.args[0],(n||e)&&(f=s&&s.tag||{_:{inline:!s},tagName:n+":",flow:!0,_is:"tag"},f._.bnd=e,s&&(s.tag=f,u.ctx=c(u.ctx,s.view.ctx)),f.tagCtx=u,u.view=t,f.ctx=u.ctx||{},delete u.ctx,t._.tag=f,n=n!=="true"&&n,n&&((h=t.getRsc("converters",n))||p("Unknown converter: {{"+n+":"))&&(f.depends=h.depends,o=h.apply(f,u.args)),o=e&&t._.onRender?t._.onRender(o,t,e):o,t._.tag=i),o}function hi(n,t){for(var e=this,u=r[n],f=u&&u[t];f===i&&e;)u=e.tmpl[n],f=u&&u[t],e=e.parent;return f}function ci(n,t,u,f){var ft,s,et,nt,k,l,tt,it,h,d,y,ot,v,ut,w="",g=+f===f&&f,a=t.linkCtx||0,b=t.ctx,st=u||t.tmpl,ht=t._;for(n._is==="tag"&&(s=n,n=s.tagName),g&&(f=(ot=st.bnds[g-1])(t.data,t,r)),tt=f.length,s=s||a.tag,l=0;l<tt;l++)h=f[l],y=h.tmpl,y=h.content=y&&st.tmpls[y-1],u=h.props.tmpl,l||u&&s||(v=t.getRsc("tags",n)||p("Unknown tag: {{"+n+"}}")),u=u||(s?s:v).template||y,u=""+u===u?t.getRsc("templates",u)||e(u):u,o(h,{tmpl:u,render:rt,index:l,view:t,ctx:c(h.ctx,b)}),s||(v._ctr?(s=new v._ctr,ut=!!s.init,s.attr=s.attr||v.attr||i):s={render:v.render},s._={inline:!a},a&&(a.attr=s.attr=a.attr||s.attr,a.tag=s,s.linkCtx=a),(s._.bnd=ot||a)&&(s._.arrVws={}),s.tagName=n,s.parent=k=b&&b.tag,s._is="tag",s._def=v),ht.tag=s,h.tag=s,s.tagCtxs=f,s.flow||(d=h.ctx=h.ctx||{},et=s.parents=d.parentTags=b&&c(d.parentTags,b.parentTags)||{},k&&(et[k.tagName]=k),d.tag=s);for(s.rendering={},l=0;l<tt;l++)h=s.tagCtx=f[l],s.ctx=h.ctx,!l&&ut&&(s.init(h,a,s.ctx),ut=i),(ft=s.render)&&(it=ft.apply(s,h.args)),w+=it!==i?it:h.tmpl?h.render():"";return delete s.rendering,s.tagCtx=s.tagCtxs[0],s.ctx=s.tagCtx.ctx,s._.inline&&(nt=s.attr)&&nt!=="html"&&(w=nt==="text"?wt.html(w):""),g&&t._.onRender?t._.onRender(w,t,g):w}function b(n,t,r,u,f,e,o,s){var c,l,a,y=t==="array",v={key:0,useKey:y?0:1,id:""+fi++,onRender:s,bnds:{}},h={data:u,tmpl:f,content:o,views:y?[]:{},parent:r,ctx:n,type:t,get:ei,getIndex:lt,getRsc:hi,hlp:oi,_:v,_is:"view"};return r&&(c=r.views,l=r._,l.useKey?(c[v.key="_"+l.useKey++]=h,a=l.tag,v.bnd=y&&(!a||!!a._.bnd&&a)):c.splice(v.key=h.index=e!==i?e:c.length,0,h),h.ctx=n||r.ctx),h}function li(n){var t,i,r,u,f;for(t in y)if(u=y[t],(f=u.compile)&&(i=n[t+"s"]))for(r in i)i[r]=f(r,i[r],n,t,u)}function ai(n,t,i){var u,r;return typeof t=="function"?t={depends:t.depends,render:t}:((r=t.template)&&(t.template=""+r===r?e[r]||e(r):r),t.init!==!1&&(u=t._ctr=function(){},(u.prototype=t).constructor=u)),i&&(t._parentTmpl=i),t}function at(r,u,f,o,s,h){function v(i){if(""+i===i||i.nodeType>0){try{a=i.nodeType>0?i:!et.test(i)&&t&&t(n.document).find(i)[0]}catch(u){}return a&&(i=a.getAttribute(ht),r=r||i,i=e[i],i||(r=r||"_"+ui++,a.setAttribute(ht,r),i=e[r]=at(r,a.innerHTML,f,o,s,h))),i}}var l,a;return u=u||"",l=v(u),h=h||(u.markup?u:{}),h.tmplName=r,f&&(h._parentTmpl=f),!l&&u.markup&&(l=v(u.markup))&&l.fn&&(l.debug!==u.debug||l.allowCode!==u.allowCode)&&(l=l.markup),l!==i?(r&&!f&&(tt[r]=function(){return u.render.apply(u,arguments)}),l.fn||u.fn?l.fn&&(u=r&&r!==l.tmplName?c(h,l):l):(u=vt(l,h),ut(l,u)),li(h),u):void 0}function vt(n,t){var i,f=d.wrapMap||{},r=o({markup:n,tmpls:[],links:{},tags:{},bnds:[],_is:"template",render:rt},t);return t.htmlTag||(i=ii.exec(n),r.htmlTag=i?i[1].toLowerCase():""),i=f[r.htmlTag],i&&i!==f.div&&(r.markup=u.trim(r.markup),r._elCnt=!0),r}function vi(n,t){function u(e,o,s){var l,h,a,c;if(e&&""+e!==e&&!e.nodeType&&!e.markup){for(a in e)u(a,e[a],o);return r}return o===i&&(o=e,e=i),e&&""+e!==e&&(s=o,o=e,e=i),c=s?s[f]=s[f]||{}:u,h=t.compile,(l=k.onBeforeStoreItem)&&(h=l(c,e,o,h)||h),e?o===null?delete c[e]:c[e]=h?o=h(e,o,s,n,t):o:o=h(i,o),h&&o&&(o._is=n),(l=k.onStoreItem)&&l(c,e,o,h),o}var f=n+"s";r[f]=u,y[n]=t}function rt(n,t,f,o,s,h){var w,ut,nt,v,tt,it,rt,k,y,ft,d,et,a,l=this,ot=!l.attr||l.attr==="html",g="";if(o===!0&&(rt=!0,o=0),l.tag?(k=l,l=l.tag,ft=l._,et=l.tagName,a=k.tmpl,t=c(t,l.ctx),y=k.content,k.props.link===!1&&(t=t||{},t.link=!1),f=f||k.view,n=n===i?f:n):a=l.jquery&&(l[0]||p('Unknown template: "'+l.selector+'"'))||l,a&&(!f&&n&&n._is==="view"&&(f=n),f&&(y=y||f.content,h=h||f._.onRender,n===f&&(n=f.data,s=!0),t=c(t,f.ctx)),f&&f.data!==i||((t=t||{}).root=n),a.fn||(a=e[a]||e(a)),a)){if(h=(t&&t.link)!==!1&&ot&&h,d=h,h===!0&&(d=i,h=f._.onRender),u.isArray(n)&&!s)for(v=rt?f:o!==i&&f||b(t,"array",f,n,a,o,y,h),w=0,ut=n.length;w<ut;w++)nt=n[w],tt=b(t,"item",v,nt,a,(o||0)+w,y,h),it=a.fn(nt,tt,r),g+=v._.onRender?v._.onRender(it,tt):it;else v=rt?f:b(t,et||"data",f,n,a,o,y,h),ft&&!l.flow&&(v.tag=l),g+=a.fn(n,v,r);return d?d(g,v):g}return""}function p(n){throw new r.sub.Error(n);}function s(n){p("Syntax error\n"+n)}function ut(n,t,i,r){function v(t){t-=f,t&&h.push(n.substr(f,t).replace(nt,"\\n"))}function c(t){t&&s('Unmatched or missing tag: "{{/'+t+'}}" in template:\n'+n)}function y(e,l,y,w,b,k,d,g,tt,it,rt,ut){k&&(b=":",w="html"),it=it||i;var at,st,ht=l&&[],ot="",et="",ct="",lt=!it&&!b&&!d;y=y||b,v(ut),f=ut+e.length,g?p&&h.push(["*","\n"+tt.replace(dt,"$1")+"\n"]):y?(y==="else"&&(ti.test(tt)&&s('for "{{else if expr}}" use "{{else expr}}"'),ht=u[6],u[7]=n.substring(u[7],ut),u=o.pop(),h=u[3],lt=!0),tt&&(tt=tt.replace(nt," "),ot=ft(tt,ht,t).replace(ni,function(n,t,i){return t?ct+=i+",":et+=i+",",""})),et=et.slice(0,-1),ot=ot.slice(0,-1),at=et&&et.indexOf("noerror:true")+1&&et||"",a=[y,w||!!r||"",ot,lt&&[],'params:"'+tt+'",props:{'+et+"}"+(ct?",ctx:{"+ct.slice(0,-1)+"}":""),at,ht||0],h.push(a),lt&&(o.push(u),u=a,u[7]=f)):rt&&(st=u[0],c(rt!==st&&st!=="else"&&rt),u[7]=n.substring(u[7],ut),u=o.pop()),c(!u&&rt),h=u[3]}var a,p=t&&t.allowCode,e=[],f=0,o=[],h=e,u=[,,,e];return n=n.replace(gt,"\\$&"),c(o[0]&&o[0][3].pop()[0]),n.replace(l,y),v(n.length),(f=e[e.length-1])&&c(""+f!==f&&+f[7]===f[7]&&f[0]),yt(e,i?n:t,i)}function yt(n,i,r){var c,f,e,l,a,y,st,ht,ct,lt,ft,p,o,et,v,tt,w,it,at,b,pt,wt,ot,rt,k,h=0,u="",g="",ut={},bt=n.length;for(""+i===i?(v=r?'data-link="'+i.replace(nt," ").slice(1,-1)+'"':i,i=0):(v=i.tmplName||"unnamed",i.allowCode&&(ut.allowCode=!0),i.debug&&(ut.debug=!0),p=i.bnds,et=i.tmpls),c=0;c<bt;c++)if(f=n[c],""+f===f)u+='\nret+="'+f+'";';else if(e=f[0],e==="*")u+=""+f[1];else{if(l=f[1],a=f[2],it=f[3],y=f[4],g=f[5],at=f[7],(wt=e==="else")||(h=0,p&&(o=f[6])&&(h=p.push(o))),(ot=e===":")?(l&&(e=l==="html"?">":l+e),g&&(rt="prm"+c,g="try{var "+rt+"=["+a+"][0];}catch(e){"+rt+'="";}\n',a=rt)):(it&&(tt=vt(at,ut),tt.tmplName=v+"/"+e,yt(it,tt),et.push(tt)),wt||(w=e,pt=u,u=""),b=n[c+1],b=b&&b[0]==="else"),y+=",args:["+a+"]}",ot&&o||l&&e!==">"){if(k=new Function("data,view,j,u"," // "+v+" "+h+" "+e+"\n"+g+"return {"+y+";"),k.paths=o,k._ctxs=e,r)return k;ft=1}if(u+=ot?"\n"+(o?"":g)+(r?"return ":"ret+=")+(ft?(ft=0,lt=!0,'c("'+l+'",view,'+(o?(p[h-1]=k,h):"{"+y)+");"):e===">"?(ht=!0,"h("+a+");"):(ct=!0,"(v="+a+")!="+(r?"=":"")+'u?v:"";')):(st=!0,"{tmpl:"+(it?et.length:"0")+","+y+","),w&&!b){if(u="["+u.slice(0,-1)+"]",(r||o)&&(u=new Function("data,view,j,u"," // "+v+" "+h+" "+w+"\nreturn "+u+";"),o&&((p[h-1]=u).paths=o),u._ctxs=e,r))return u;u=pt+'\nret+=t("'+w+'",view,this,'+(h||u)+");",o=0,w=0}}u="// "+v+"\nvar j=j||"+(t?"jQuery.":"js")+"views"+(ct?",v":"")+(st?",t=j._tag":"")+(lt?",c=j._cnvt":"")+(ht?",h=j.converters.html":"")+(r?";\n":',ret="";\n')+(d.tryCatch?"try{\n":"")+(ut.debug?"debugger;":"")+u+(r?"\n":"\nreturn ret;\n")+(d.tryCatch?"\n}catch(e){return j._err(e);}":"");try{u=new Function("data,view,j,u",u)}catch(kt){s("Compiled template code:\n\n"+u,kt)}return i&&(i.fn=u),u}function ft(n,t,i){function b(b,k,d,g,nt,tt,it,rt,et,ot,st,ht,ct,lt,at,vt,yt,pt,wt,kt){function gt(n,i,r,f,o,s,h){if(i&&(t&&(u==="linkTo"&&(e=t.to=t.to||[],e.push(nt)),(!u||l)&&t.push(nt)),i!==".")){var c=(r?'view.hlp("'+r+'")':f?"view":"data")+(h?(o?"."+o:r?"":f?"":"."+i)+(s||""):(h=r?"":f?o||"":i,""));return c=c+(h?"."+h:""),c.slice(0,9)==="view.data"?c.slice(5):c}return n}var dt;if(tt=tt||"",d=d||k||ht,nt=nt||et,ot=ot||yt||"",it)s(n);else return t&&vt&&!c&&!o&&(!u||l||e)&&(dt=p[r],kt.length-2>wt-dt&&(dt=kt.slice(dt,wt+1),vt=v+":"+dt+f,vt=w[vt]=w[vt]||ut(a+vt+h,i,!0),vt.paths||ft(dt,vt.paths=[],i),(e||t).push({_jsvOb:vt}))),c?(c=!ct,c?b:'"'):o?(o=!lt,o?b:'"'):(d?(r++,p[r]=wt++,d):"")+(pt?r?"":u?(u=l=e=!1,"\b"):",":rt?(r&&s(n),u=nt,l=g,"\b"+nt+":"):nt?nt.split("^").join(".").replace(bt,gt)+(ot?(y[++r]=!0,nt.charAt(0)!=="."&&(p[r]=wt),ot):tt):tt?tt:at?(y[r--]=!1,at)+(ot?(y[++r]=!0,ot):""):st?(y[r]||s(n),","):k?"":(c=ct,o=lt,'"'))}var u,e,l,w=i.links,y={},p={0:-1},r=0,o=!1,c=!1;return(n+" ").replace(kt,b)}function c(n,t){return n&&n!==t?t?o(o({},t),n):n:t&&o({},t)}function pt(n){return st[n]||(st[n]="&#"+n.charCodeAt(0)+";")}if((!t||!t.views)&&!n.jsviews){var u,g,l,et,a="{",v="{",f="}",h="}",w="^",bt=/^(?:null|true|false|\d[\d.]*|([\w$]+|\.|~([\w$]+)|#(view|([\w$]+))?)([\w$.^]*?)(?:[.[^]([\w$]+)\]?)?)$/g,kt=/(\()(?=\s*\()|(?:([([])\s*)?(?:(\^?)([#~]?[\w$.^]+)?\s*((\+\+|--)|\+|-|&&|\|\||===|!==|==|!=|<=|>=|[<>%*!:?\/]|(=))\s*|([#~]?[\w$.^]+)([([])?)|(,\s*)|(\(?)\\?(?:(')|("))|(?:\s*(([)\]])(?=\s*\.|\s*\^)|[)\]])([([]?))|(\s+)/g,nt=/[ \t]*(\r\n|\n|\r)/g,dt=/\\(['"])/g,gt=/['"\\]/g,ni=/\x08(~)?([^\x08]+)\x08/g,ti=/^if\s/,ii=/<(\w+)[>\s]/,ot=/[\x00`><"'&]/g,ri=ot,ui=0,fi=0,st={"&":"&amp;","<":"&lt;",">":"&gt;","\x00":"&#0;","'":"&#39;",'"':"&#34;","`":"&#96;"},ht="data-jsv-tmpl",tt={},y={template:{compile:at},tag:{compile:ai},helper:{},converter:{}},r={jsviews:"v1.0.0-beta",render:tt,settings:{delimiters:ct,debugMode:!0,tryCatch:!0},sub:{View:b,Error:it,tmplFn:ut,parse:ft,extend:o,error:p,syntaxError:s},_cnvt:si,_tag:ci,_err:function(n){return d.debugMode?"Error: "+(n.message||n)+". ":""}};(it.prototype=new Error).constructor=it,lt.depends=function(){return[this.get("item"),"index"]};for(g in y)vi(g,y[g]);var e=r.templates,wt=r.converters,pi=r.helpers,yi=r.tags,k=r.sub,d=r.settings;t?(u=t,u.fn.render=rt):(u=n.jsviews={},u.isArray=Array&&Array.isArray||function(n){return Object.prototype.toString.call(n)==="[object Array]"}),u.render=tt,u.views=r,u.templates=e=r.templates,yi({"else":function(){},"if":{render:function(n){var t=this;return t.rendering.done||!n&&(arguments.length||!t.tagCtx.index)?"":(t.rendering.done=!0,t.selected=t.tagCtx.index,t.tagCtx.render())},onUpdate:function(n,t,i){for(var r,f,u=0;(r=this.tagCtxs[u])&&r.args.length;u++)if(r=r.args[0],f=!r!=!i[u].args[0],!!r||f)return f;return!1},flow:!0},"for":{render:function(n){var t=this,f=t.tagCtx,e=!arguments.length,r="",o=e||0;return t.rendering.done||(e?r=i:n!==i&&(r+=f.render(n),o+=u.isArray(n)?n.length:1),(t.rendering.done=o)&&(t.selected=f.index)),r},onArrayChange:function(n,t){var i,u=this,r=t.change;if(this.tagCtxs[1]&&(r==="insert"&&n.target.length===t.items.length||r==="remove"&&!n.target.length||r==="refresh"&&!t.oldItems.length!=!n.target.length))this.refresh();else for(i in u._.arrVws)i=u._.arrVws[i],i.data===n.target&&i._.onArrayChange.apply(i,arguments);n.done=!0},flow:!0},include:{flow:!0},"*":{render:function(n){return n},flow:!0}}),wt({html:function(n){return n!=i?String(n).replace(ri,pt):""},attr:function(n){return n!=i?String(n).replace(ot,pt):n===null?null:""},url:function(n){return n!=i?encodeURI(String(n)):n===null?null:""}}),ct()}})(this,this.jQuery);
//JsRender v1.0.0-beta: http://jsviews.com

/**
 * Copyright (c) 2007-2013 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.6
 */
;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,targ,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);

/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(f,h,$){var a='placeholder' in h.createElement('input'),d='placeholder' in h.createElement('textarea'),i=$.fn,c=$.valHooks,k,j;if(a&&d){j=i.placeholder=function(){return this};j.input=j.textarea=true}else{j=i.placeholder=function(){var l=this;l.filter((a?'textarea':':input')+'[placeholder]').not('.placeholder').bind({'focus.placeholder':b,'blur.placeholder':e}).data('placeholder-enabled',true).trigger('blur.placeholder');return l};j.input=a;j.textarea=d;k={get:function(m){var l=$(m);return l.data('placeholder-enabled')&&l.hasClass('placeholder')?'':m.value},set:function(m,n){var l=$(m);if(!l.data('placeholder-enabled')){return m.value=n}if(n==''){m.value=n;if(m!=h.activeElement){e.call(m)}}else{if(l.hasClass('placeholder')){b.call(m,true,n)||(m.value=n)}else{m.value=n}}return l}};a||(c.input=k);d||(c.textarea=k);$(function(){$(h).delegate('form','submit.placeholder',function(){var l=$('.placeholder',this).each(b);setTimeout(function(){l.each(e)},10)})});$(f).bind('beforeunload.placeholder',function(){$('.placeholder').each(function(){this.value=''})})}function g(m){var l={},n=/^jQuery\d+$/;$.each(m.attributes,function(p,o){if(o.specified&&!n.test(o.name)){l[o.name]=o.value}});return l}function b(m,n){var l=this,o=$(l);if(l.value==o.attr('placeholder')&&o.hasClass('placeholder')){if(o.data('placeholder-password')){o=o.hide().next().show().attr('id',o.removeAttr('id').data('placeholder-id'));if(m===true){return o[0].value=n}o.focus()}else{l.value='';o.removeClass('placeholder');l==h.activeElement&&l.select()}}}function e(){var q,l=this,p=$(l),m=p,o=this.id;if(l.value==''){if(l.type=='password'){if(!p.data('placeholder-textinput')){try{q=p.clone().attr({type:'text'})}catch(n){q=$('<input>').attr($.extend(g(this),{type:'text'}))}q.removeAttr('name').data({'placeholder-password':true,'placeholder-id':o}).bind('focus.placeholder',b);p.data({'placeholder-textinput':q,'placeholder-id':o}).before(q)}p=p.removeAttr('id').hide().prev().attr('id',o).show()}p.addClass('placeholder');p[0].value=p.attr('placeholder')}else{p.removeClass('placeholder')}}}(this,document,jQuery));

/*********************
 * LIBRARIES 
 *********************/

/*!
  Tinycon - A small library for manipulating the Favicon
  Tom Moor, http://tommoor.com
  Copyright (c) 2012 Tom Moor
  MIT Licensed
  @version 0.5
*/
;(function(){var Tinycon={};var currentFavicon=null;var originalFavicon=null;var originalTitle=document.title;var faviconImage=null;var canvas=null;var options={};var defaults={width:7,height:9,font:'10px arial',colour:'#ffffff',background:'#F03D25',fallback:true,abbreviate:true};var ua=(function(){var agent=navigator.userAgent.toLowerCase();return function(browser){return agent.indexOf(browser)!==-1}}());var browser={ie:ua('msie'),chrome:ua('chrome'),webkit:ua('chrome')||ua('safari'),safari:ua('safari')&&!ua('chrome'),mozilla:ua('mozilla')&&!ua('chrome')&&!ua('safari')};var getFaviconTag=function(){var links=document.getElementsByTagName('link');for(var i=0,len=links.length;i<len;i++){if((links[i].getAttribute('rel')||'').match(/\bicon\b/)){return links[i]}}return false};var removeFaviconTag=function(){var links=document.getElementsByTagName('link');var head=document.getElementsByTagName('head')[0];for(var i=0,len=links.length;i<len;i++){var exists=(typeof(links[i])!=='undefined');if(exists&&(links[i].getAttribute('rel')||'').match(/\bicon\b/)){head.removeChild(links[i])}}};var getCurrentFavicon=function(){if(!originalFavicon||!currentFavicon){var tag=getFaviconTag();originalFavicon=currentFavicon=tag?tag.getAttribute('href'):'/favicon.ico'}return currentFavicon};var getCanvas=function(){if(!canvas){canvas=document.createElement("canvas");canvas.width=16;canvas.height=16}return canvas};var setFaviconTag=function(url){removeFaviconTag();var link=document.createElement('link');link.type='image/x-icon';link.rel='icon';link.href=url;document.getElementsByTagName('head')[0].appendChild(link)};var log=function(message){if(window.console)window.console.log(message)};var drawFavicon=function(label,colour){if(!getCanvas().getContext||browser.ie||browser.safari||options.fallback==='force'){return updateTitle(label)}var context=getCanvas().getContext("2d");var colour=colour||'#000000';var src=getCurrentFavicon();faviconImage=new Image();faviconImage.onload=function(){context.clearRect(0,0,16,16);context.drawImage(faviconImage,0,0,faviconImage.width,faviconImage.height,0,0,16,16);if((label+'').length>0)drawBubble(context,label,colour);refreshFavicon()};if(!src.match(/^data/)){faviconImage.crossOrigin='anonymous'}faviconImage.src=src};var updateTitle=function(label){if(options.fallback){if((label+'').length>0){document.title='('+label+') '+originalTitle}else{document.title=originalTitle}}};var drawBubble=function(context,label,colour){if(typeof label=='number'&&label>99&&options.abbreviate){label=abbreviateNumber(label)}var len=(label+'').length-1;var width=options.width+(6*len);var w=16-width;var h=16-options.height;context.font=(browser.webkit?'bold ':'')+options.font;context.fillStyle=options.background;context.strokeStyle=options.background;context.lineWidth=1;context.fillRect(w,h,width-1,options.height);context.beginPath();context.moveTo(w-0.5,h+1);context.lineTo(w-0.5,15);context.stroke();context.beginPath();context.moveTo(15.5,h+1);context.lineTo(15.5,15);context.stroke();context.beginPath();context.strokeStyle="rgba(0,0,0,0.3)";context.moveTo(w,16);context.lineTo(15,16);context.stroke();context.fillStyle=options.colour;context.textAlign="right";context.textBaseline="top";context.fillText(label,15,browser.mozilla?7:6)};var refreshFavicon=function(){if(!getCanvas().getContext)return;setFaviconTag(getCanvas().toDataURL())};var abbreviateNumber=function(label){var metricPrefixes=[['G',1000000000],['M',1000000],['k',1000]];for(var i=0;i<metricPrefixes.length;++i){if(label>=metricPrefixes[i][1]){label=round(label/metricPrefixes[i][1])+metricPrefixes[i][0];break}}return label};var round=function(value,precision){var number=new Number(value);return number.toFixed(precision)};Tinycon.setOptions=function(custom){options={};for(var key in defaults){options[key]=custom.hasOwnProperty(key)?custom[key]:defaults[key]}return this};Tinycon.setImage=function(url){currentFavicon=url;refreshFavicon();return this};Tinycon.setBubble=function(label,colour){label=label||'';drawFavicon(label,colour);return this};Tinycon.reset=function(){setFaviconTag(originalFavicon)};Tinycon.setOptions(defaults);window.Tinycon=Tinycon})();

// Copyright (c) 2012 Florian H., https://github.com/js-coder https://github.com/js-coder/cookie.js
!function(e,t){var n=function(){return n.get.apply(n,arguments)},r=n.utils={isArray:Array.isArray||function(e){return Object.prototype.toString.call(e)==="[object Array]"},isPlainObject:function(e){return!!e&&Object.prototype.toString.call(e)==="[object Object]"},toArray:function(e){return Array.prototype.slice.call(e)},getKeys:Object.keys||function(e){var t=[],n="";for(n in e)e.hasOwnProperty(n)&&t.push(n);return t},escape:function(e){return String(e).replace(/[,;"\\=\s%]/g,function(e){return encodeURIComponent(e)})},retrieve:function(e,t){return e==null?t:e}};n.defaults={},n.expiresMultiplier=86400,n.set=function(n,i,s){if(r.isPlainObject(n))for(var o in n)n.hasOwnProperty(o)&&this.set(o,n[o],i);else{s=r.isPlainObject(s)?s:{expires:s};var u=s.expires!==t?s.expires:this.defaults.expires||"",a=typeof u;a==="string"&&u!==""?u=new Date(u):a==="number"&&(u=new Date(+(new Date)+1e3*this.expiresMultiplier*u)),u!==""&&"toGMTString"in u&&(u=";expires="+u.toGMTString());var f=s.path||this.defaults.path;f=f?";path="+f:"";var l=s.domain||this.defaults.domain;l=l?";domain="+l:"";var c=s.secure||this.defaults.secure?";secure":"";e.cookie=r.escape(n)+"="+r.escape(i)+u+f+l+c}return this},n.remove=function(e){e=r.isArray(e)?e:r.toArray(arguments);for(var t=0,n=e.length;t<n;t++)this.set(e[t],"",-1);return this},n.empty=function(){return this.remove(r.getKeys(this.all()))},n.get=function(e,n){n=n||t;var i=this.all();if(r.isArray(e)){var s={};for(var o=0,u=e.length;o<u;o++){var a=e[o];s[a]=r.retrieve(i[a],n)}return s}return r.retrieve(i[e],n)},n.all=function(){if(e.cookie==="")return{};var t=e.cookie.split("; "),n={};for(var r=0,i=t.length;r<i;r++){var s=t[r].split("=");n[decodeURIComponent(s[0])]=decodeURIComponent(s[1])}return n},n.enabled=function(){if(navigator.cookieEnabled)return!0;var e=n.set("_","_").get("_")==="_";return n.remove("_"),e},typeof define=="function"&&define.amd?define(function(){return n}):typeof exports!="undefined"?exports.cookie=n:window.cookie=n}(document);

/* Copyright (c) 2010-2012 Marcus Westin */
this.JSON||(this.JSON={}),function(){function f(e){return e<10?"0"+e:e}function quote(e){return escapable.lastIndex=0,escapable.test(e)?'"'+e.replace(escapable,function(e){var t=meta[e];return typeof t=="string"?t:"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+e+'"'}function str(e,t){var n,r,i,s,o=gap,u,a=t[e];a&&typeof a=="object"&&typeof a.toJSON=="function"&&(a=a.toJSON(e)),typeof rep=="function"&&(a=rep.call(t,e,a));switch(typeof a){case"string":return quote(a);case"number":return isFinite(a)?String(a):"null";case"boolean":case"null":return String(a);case"object":if(!a)return"null";gap+=indent,u=[];if(Object.prototype.toString.apply(a)==="[object Array]"){s=a.length;for(n=0;n<s;n+=1)u[n]=str(n,a)||"null";return i=u.length===0?"[]":gap?"[\n"+gap+u.join(",\n"+gap)+"\n"+o+"]":"["+u.join(",")+"]",gap=o,i}if(rep&&typeof rep=="object"){s=rep.length;for(n=0;n<s;n+=1)r=rep[n],typeof r=="string"&&(i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i))}else for(r in a)Object.hasOwnProperty.call(a,r)&&(i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i));return i=u.length===0?"{}":gap?"{\n"+gap+u.join(",\n"+gap)+"\n"+o+"}":"{"+u.join(",")+"}",gap=o,i}}typeof Date.prototype.toJSON!="function"&&(Date.prototype.toJSON=function(e){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(e){return this.valueOf()});var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","	":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;typeof JSON.stringify!="function"&&(JSON.stringify=function(e,t,n){var r;gap="",indent="";if(typeof n=="number")for(r=0;r<n;r+=1)indent+=" ";else typeof n=="string"&&(indent=n);rep=t;if(!t||typeof t=="function"||typeof t=="object"&&typeof t.length=="number")return str("",{"":e});throw new Error("JSON.stringify")}),typeof JSON.parse!="function"&&(JSON.parse=function(text,reviver){function walk(e,t){var n,r,i=e[t];if(i&&typeof i=="object")for(n in i)Object.hasOwnProperty.call(i,n)&&(r=walk(i,n),r!==undefined?i[n]=r:delete i[n]);return reviver.call(e,t,i)}var j;text=String(text),cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(e){return"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),typeof reviver=="function"?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}(),function(){function o(){try{return r in t&&t[r]}catch(e){return!1}}var e={},t=window,n=t.document,r="localStorage",i="__storejs__",s;e.disabled=!1,e.set=function(e,t){},e.get=function(e){},e.remove=function(e){},e.clear=function(){},e.transact=function(t,n,r){var i=e.get(t);r==null&&(r=n,n=null),typeof i=="undefined"&&(i=n||{}),r(i),e.set(t,i)},e.getAll=function(){},e.serialize=function(e){return JSON.stringify(e)},e.deserialize=function(e){if(typeof e!="string")return undefined;try{return JSON.parse(e)}catch(t){return e||undefined}};if(o())s=t[r],e.set=function(t,n){return n===undefined?e.remove(t):(s.setItem(t,e.serialize(n)),n)},e.get=function(t){return e.deserialize(s.getItem(t))},e.remove=function(e){s.removeItem(e)},e.clear=function(){s.clear()},e.getAll=function(){var t={};for(var n=0;n<s.length;++n){var r=s.key(n);t[r]=e.get(r)}return t};else if(n.documentElement.addBehavior){var u,a;try{a=new ActiveXObject("htmlfile"),a.open(),a.write('<script>document.w=window</script><iframe src="/favicon.ico"></frame>'),a.close(),u=a.w.frames[0].document,s=u.createElement("div")}catch(f){s=n.createElement("div"),u=n.body}function l(t){return function(){var n=Array.prototype.slice.call(arguments,0);n.unshift(s),u.appendChild(s),s.addBehavior("#default#userData"),s.load(r);var i=t.apply(e,n);return u.removeChild(s),i}}var c=new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]","g");function h(e){return e.replace(c,"___")}e.set=l(function(t,n,i){return n=h(n),i===undefined?e.remove(n):(t.setAttribute(n,e.serialize(i)),t.save(r),i)}),e.get=l(function(t,n){return n=h(n),e.deserialize(t.getAttribute(n))}),e.remove=l(function(e,t){t=h(t),e.removeAttribute(t),e.save(r)}),e.clear=l(function(e){var t=e.XMLDocument.documentElement.attributes;e.load(r);for(var n=0,i;i=t[n];n++)e.removeAttribute(i.name);e.save(r)}),e.getAll=l(function(t){var n=t.XMLDocument.documentElement.attributes,r={};for(var i=0,s;s=n[i];++i){var o=h(s.name);r[s.name]=e.deserialize(t.getAttribute(o))}return r})}try{e.set(i,i),e.get(i)!=i&&(e.disabled=!0),e.remove(i)}catch(f){e.disabled=!0}e.enabled=!e.disabled,typeof module!="undefined"&&typeof module!="function"?module.exports=e:typeof define=="function"&&define.amd?define(e):this.store=e}();

/*!
* mustache.js - Logic-less {{mustache}} templates with JavaScript
* http://github.com/janl/mustache.js
*/

;(function(e,t){if(typeof exports==="object"&&exports){t(exports)}else{var n={};t(n);if(typeof define==="function"&&define.amd){define(n)}else{e.Mustache=n}}})(this,function(e){function a(e,t){return u.call(e,t)}function f(e){return!a(r,e)}function h(e){return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")}function d(e){return String(e).replace(/[&<>"'\/]/g,function(e){return p[e]})}function v(e){this.string=e;this.tail=e;this.pos=0}function m(e,t){this.view=e||{};this.parent=t;this._cache={}}function g(){this.clearCache()}function y(t,n,r,i){var s="";var o,u,a;for(var f=0,l=t.length;f<l;++f){o=t[f];u=o[1];switch(o[0]){case"#":a=r.lookup(u);if(typeof a==="object"){if(c(a)){for(var h=0,p=a.length;h<p;++h){s+=y(o[4],n,r.push(a[h]),i)}}else if(a){s+=y(o[4],n,r.push(a),i)}}else if(typeof a==="function"){var d=i==null?null:i.slice(o[3],o[5]);a=a.call(r.view,d,function(e){return n.render(e,r)});if(a!=null)s+=a}else if(a){s+=y(o[4],n,r,i)}break;case"^":a=r.lookup(u);if(!a||c(a)&&a.length===0){s+=y(o[4],n,r,i)}break;case">":a=n.getPartial(u);if(typeof a==="function")s+=a(r);break;case"&":a=r.lookup(u);if(a!=null)s+=a;break;case"name":a=r.lookup(u);if(a!=null)s+=e.escape(a);break;case"text":s+=u;break}}return s}function b(e){var t=[];var n=t;var r=[];var i;for(var s=0,o=e.length;s<o;++s){i=e[s];switch(i[0]){case"#":case"^":r.push(i);n.push(i);n=i[4]=[];break;case"/":var u=r.pop();u[5]=i[2];n=r.length>0?r[r.length-1][4]:t;break;default:n.push(i)}}return t}function w(e){var t=[];var n,r;for(var i=0,s=e.length;i<s;++i){n=e[i];if(n){if(n[0]==="text"&&r&&r[0]==="text"){r[1]+=n[1];r[3]=n[3]}else{r=n;t.push(n)}}}return t}function E(e){return[new RegExp(h(e[0])+"\\s*"),new RegExp("\\s*"+h(e[1]))]}function S(r,u){function y(){if(m&&!g){while(d.length){delete p[d.pop()]}}else{d=[]}m=false;g=false}r=r||"";u=u||e.tags;if(typeof u==="string")u=u.split(n);if(u.length!==2)throw new Error("Invalid tags: "+u.join(", "));var a=E(u);var l=new v(r);var c=[];var p=[];var d=[];var m=false;var g=false;var S,x,T,N,C;while(!l.eos()){S=l.pos;T=l.scanUntil(a[0]);if(T){for(var k=0,L=T.length;k<L;++k){N=T.charAt(k);if(f(N)){d.push(p.length)}else{g=true}p.push(["text",N,S,S+1]);S+=1;if(N=="\n")y()}}if(!l.scan(a[0]))break;m=true;x=l.scan(o)||"name";l.scan(t);if(x==="="){T=l.scanUntil(i);l.scan(i);l.scanUntil(a[1])}else if(x==="{"){T=l.scanUntil(new RegExp("\\s*"+h("}"+u[1])));l.scan(s);l.scanUntil(a[1]);x="&"}else{T=l.scanUntil(a[1])}if(!l.scan(a[1]))throw new Error("Unclosed tag at "+l.pos);C=[x,T,S,l.pos];p.push(C);if(x==="#"||x==="^"){c.push(C)}else if(x==="/"){if(c.length===0)throw new Error('Unopened section "'+T+'" at '+S);var A=c.pop();if(A[1]!==T)throw new Error('Unclosed section "'+A[1]+'" at '+S)}else if(x==="name"||x==="{"||x==="&"){g=true}else if(x==="="){u=T.split(n);if(u.length!==2)throw new Error("Invalid tags at "+S+": "+u.join(", "));a=E(u)}}var A=c.pop();if(A)throw new Error('Unclosed section "'+A[1]+'" at '+l.pos);p=w(p);return b(p)}var t=/\s*/;var n=/\s+/;var r=/\S/;var i=/\s*=/;var s=/\s*\}/;var o=/#|\^|\/|>|\{|&|=|!/;var u=RegExp.prototype.test;var l=Object.prototype.toString;var c=Array.isArray||function(e){return l.call(e)==="[object Array]"};var p={"&":"&","<":"<",">":">",'"':"\"","'":"&#39;","/":"&#x2F;"};v.prototype.eos=function(){return this.tail===""};v.prototype.scan=function(e){var t=this.tail.match(e);if(t&&t.index===0){this.tail=this.tail.substring(t[0].length);this.pos+=t[0].length;return t[0]}return""};v.prototype.scanUntil=function(e){var t,n=this.tail.search(e);switch(n){case-1:t=this.tail;this.pos+=this.tail.length;this.tail="";break;case 0:t="";break;default:t=this.tail.substring(0,n);this.tail=this.tail.substring(n);this.pos+=n}return t};m.make=function(e){return e instanceof m?e:new m(e)};m.prototype.push=function(e){return new m(e,this)};m.prototype.lookup=function(e){var t=this._cache[e];if(!t){if(e=="."){t=this.view}else{var n=this;while(n){if(e.indexOf(".")>0){t=n.view;var r=e.split("."),i=0;while(t&&i<r.length){t=t[r[i++]]}}else{t=n.view[e]}if(t!=null)break;n=n.parent}}this._cache[e]=t}if(typeof t==="function")t=t.call(this.view);return t};g.prototype.clearCache=function(){this._cache={};this._partialCache={}};g.prototype.compile=function(t,n){var r=this._cache[t];if(!r){var i=e.parse(t,n);r=this._cache[t]=this.compileTokens(i,t)}return r};g.prototype.compilePartial=function(e,t,n){var r=this.compile(t,n);this._partialCache[e]=r;return r};g.prototype.getPartial=function(e){if(!(e in this._partialCache)&&this._loadPartial){this.compilePartial(e,this._loadPartial(e))}return this._partialCache[e]};g.prototype.compileTokens=function(e,t){var n=this;return function(r,i){if(i){if(typeof i==="function"){n._loadPartial=i}else{for(var s in i){n.compilePartial(s,i[s])}}}return y(e,n,m.make(r),t)}};g.prototype.render=function(e,t,n){return this.compile(e)(t,n)};e.name="mustache.js";e.version="0.7.2";e.tags=["{{","}}"];e.Scanner=v;e.Context=m;e.Writer=g;e.parse=S;e.escape=d;var x=new g;e.clearCache=function(){return x.clearCache()};e.compile=function(e,t){return x.compile(e,t)};e.compilePartial=function(e,t,n){return x.compilePartial(e,t,n)};e.compileTokens=function(e,t){return x.compileTokens(e,t)};e.render=function(e,t,n){return x.render(e,t,n)};e.to_html=function(t,n,r,i){var s=e.render(t,n,r);if(typeof i==="function"){i(s)}else{return s}}});

/* 
 Midway.js
 http://brandonjacoby.com/
 http://shipp.co
 */
eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('7 5(){$(\'.3\').0(\'h\',- +$(\'.3\').g()/2);$(\'.4\').0(\'f\',- +$(\'.4\').e()/2);$(".3").0({\'6\':\'b\',\'c\':\'d\',\'9\':\'1%\',\'a\':\'1%\'});$(".4").0({\'6\':\'b\',\'c\':\'d\',\'9\':\'1%\',\'a\':\'1%\'})}$(8).i(7(){5();$(8).j(\'k\',5)});',21,21,'css|50||centerHorizontal|centerVertical|Midway|display|function|window|top|left|inline|position|absolute|height|marginTop|width|marginLeft|load|on|resize'.split('|'),0,{}));

//! moment.js
//! version : 2.4.0
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
;(function(a){function b(a,b){return function(c){return i(a.call(this,c),b)}}function c(a,b){return function(c){return this.lang().ordinal(a.call(this,c),b)}}function d(){}function e(a){u(a),g(this,a)}function f(a){var b=o(a),c=b.year||0,d=b.month||0,e=b.week||0,f=b.day||0,g=b.hour||0,h=b.minute||0,i=b.second||0,j=b.millisecond||0;this._input=a,this._milliseconds=+j+1e3*i+6e4*h+36e5*g,this._days=+f+7*e,this._months=+d+12*c,this._data={},this._bubble()}function g(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c]);return b.hasOwnProperty("toString")&&(a.toString=b.toString),b.hasOwnProperty("valueOf")&&(a.valueOf=b.valueOf),a}function h(a){return 0>a?Math.ceil(a):Math.floor(a)}function i(a,b){for(var c=a+"";c.length<b;)c="0"+c;return c}function j(a,b,c,d){var e,f,g=b._milliseconds,h=b._days,i=b._months;g&&a._d.setTime(+a._d+g*c),(h||i)&&(e=a.minute(),f=a.hour()),h&&a.date(a.date()+h*c),i&&a.month(a.month()+i*c),g&&!d&&bb.updateOffset(a),(h||i)&&(a.minute(e),a.hour(f))}function k(a){return"[object Array]"===Object.prototype.toString.call(a)}function l(a){return"[object Date]"===Object.prototype.toString.call(a)||a instanceof Date}function m(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&q(a[d])!==q(b[d]))&&g++;return g+f}function n(a){if(a){var b=a.toLowerCase().replace(/(.)s$/,"$1");a=Kb[a]||Lb[b]||b}return a}function o(a){var b,c,d={};for(c in a)a.hasOwnProperty(c)&&(b=n(c),b&&(d[b]=a[c]));return d}function p(b){var c,d;if(0===b.indexOf("week"))c=7,d="day";else{if(0!==b.indexOf("month"))return;c=12,d="month"}bb[b]=function(e,f){var g,h,i=bb.fn._lang[b],j=[];if("number"==typeof e&&(f=e,e=a),h=function(a){var b=bb().utc().set(d,a);return i.call(bb.fn._lang,b,e||"")},null!=f)return h(f);for(g=0;c>g;g++)j.push(h(g));return j}}function q(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=b>=0?Math.floor(b):Math.ceil(b)),c}function r(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function s(a){return t(a)?366:365}function t(a){return 0===a%4&&0!==a%100||0===a%400}function u(a){var b;a._a&&-2===a._pf.overflow&&(b=a._a[gb]<0||a._a[gb]>11?gb:a._a[hb]<1||a._a[hb]>r(a._a[fb],a._a[gb])?hb:a._a[ib]<0||a._a[ib]>23?ib:a._a[jb]<0||a._a[jb]>59?jb:a._a[kb]<0||a._a[kb]>59?kb:a._a[lb]<0||a._a[lb]>999?lb:-1,a._pf._overflowDayOfYear&&(fb>b||b>hb)&&(b=hb),a._pf.overflow=b)}function v(a){a._pf={empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function w(a){return null==a._isValid&&(a._isValid=!isNaN(a._d.getTime())&&a._pf.overflow<0&&!a._pf.empty&&!a._pf.invalidMonth&&!a._pf.nullInput&&!a._pf.invalidFormat&&!a._pf.userInvalidated,a._strict&&(a._isValid=a._isValid&&0===a._pf.charsLeftOver&&0===a._pf.unusedTokens.length)),a._isValid}function x(a){return a?a.toLowerCase().replace("_","-"):a}function y(a,b){return b.abbr=a,mb[a]||(mb[a]=new d),mb[a].set(b),mb[a]}function z(a){delete mb[a]}function A(a){var b,c,d,e,f=0,g=function(a){if(!mb[a]&&nb)try{require("./lang/"+a)}catch(b){}return mb[a]};if(!a)return bb.fn._lang;if(!k(a)){if(c=g(a))return c;a=[a]}for(;f<a.length;){for(e=x(a[f]).split("-"),b=e.length,d=x(a[f+1]),d=d?d.split("-"):null;b>0;){if(c=g(e.slice(0,b).join("-")))return c;if(d&&d.length>=b&&m(e,d,!0)>=b-1)break;b--}f++}return bb.fn._lang}function B(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function C(a){var b,c,d=a.match(rb);for(b=0,c=d.length;c>b;b++)d[b]=Pb[d[b]]?Pb[d[b]]:B(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function D(a,b){return a.isValid()?(b=E(b,a.lang()),Mb[b]||(Mb[b]=C(b)),Mb[b](a)):a.lang().invalidDate()}function E(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(sb.lastIndex=0;d>=0&&sb.test(a);)a=a.replace(sb,c),sb.lastIndex=0,d-=1;return a}function F(a,b){var c;switch(a){case"DDDD":return vb;case"YYYY":case"GGGG":case"gggg":return wb;case"YYYYY":case"GGGGG":case"ggggg":return xb;case"S":case"SS":case"SSS":case"DDD":return ub;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":return zb;case"a":case"A":return A(b._l)._meridiemParse;case"X":return Cb;case"Z":case"ZZ":return Ab;case"T":return Bb;case"SSSS":return yb;case"MM":case"DD":case"YY":case"GG":case"gg":case"HH":case"hh":case"mm":case"ss":case"M":case"D":case"d":case"H":case"h":case"m":case"s":case"w":case"ww":case"W":case"WW":case"e":case"E":return tb;default:return c=new RegExp(N(M(a.replace("\\","")),"i"))}}function G(a){var b=(Ab.exec(a)||[])[0],c=(b+"").match(Hb)||["-",0,0],d=+(60*c[1])+q(c[2]);return"+"===c[0]?-d:d}function H(a,b,c){var d,e=c._a;switch(a){case"M":case"MM":null!=b&&(e[gb]=q(b)-1);break;case"MMM":case"MMMM":d=A(c._l).monthsParse(b),null!=d?e[gb]=d:c._pf.invalidMonth=b;break;case"D":case"DD":null!=b&&(e[hb]=q(b));break;case"DDD":case"DDDD":null!=b&&(c._dayOfYear=q(b));break;case"YY":e[fb]=q(b)+(q(b)>68?1900:2e3);break;case"YYYY":case"YYYYY":e[fb]=q(b);break;case"a":case"A":c._isPm=A(c._l).isPM(b);break;case"H":case"HH":case"h":case"hh":e[ib]=q(b);break;case"m":case"mm":e[jb]=q(b);break;case"s":case"ss":e[kb]=q(b);break;case"S":case"SS":case"SSS":case"SSSS":e[lb]=q(1e3*("0."+b));break;case"X":c._d=new Date(1e3*parseFloat(b));break;case"Z":case"ZZ":c._useUTC=!0,c._tzm=G(b);break;case"w":case"ww":case"W":case"WW":case"d":case"dd":case"ddd":case"dddd":case"e":case"E":a=a.substr(0,1);case"gg":case"gggg":case"GG":case"GGGG":case"GGGGG":a=a.substr(0,2),b&&(c._w=c._w||{},c._w[a]=b)}}function I(a){var b,c,d,e,f,g,h,i,j,k,l=[];if(!a._d){for(d=K(a),a._w&&null==a._a[hb]&&null==a._a[gb]&&(f=function(b){return b?b.length<3?parseInt(b,10)>68?"19"+b:"20"+b:b:null==a._a[fb]?bb().weekYear():a._a[fb]},g=a._w,null!=g.GG||null!=g.W||null!=g.E?h=X(f(g.GG),g.W||1,g.E,4,1):(i=A(a._l),j=null!=g.d?T(g.d,i):null!=g.e?parseInt(g.e,10)+i._week.dow:0,k=parseInt(g.w,10)||1,null!=g.d&&j<i._week.dow&&k++,h=X(f(g.gg),k,j,i._week.doy,i._week.dow)),a._a[fb]=h.year,a._dayOfYear=h.dayOfYear),a._dayOfYear&&(e=null==a._a[fb]?d[fb]:a._a[fb],a._dayOfYear>s(e)&&(a._pf._overflowDayOfYear=!0),c=S(e,0,a._dayOfYear),a._a[gb]=c.getUTCMonth(),a._a[hb]=c.getUTCDate()),b=0;3>b&&null==a._a[b];++b)a._a[b]=l[b]=d[b];for(;7>b;b++)a._a[b]=l[b]=null==a._a[b]?2===b?1:0:a._a[b];l[ib]+=q((a._tzm||0)/60),l[jb]+=q((a._tzm||0)%60),a._d=(a._useUTC?S:R).apply(null,l)}}function J(a){var b;a._d||(b=o(a._i),a._a=[b.year,b.month,b.day,b.hour,b.minute,b.second,b.millisecond],I(a))}function K(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function L(a){a._a=[],a._pf.empty=!0;var b,c,d,e,f,g=A(a._l),h=""+a._i,i=h.length,j=0;for(d=E(a._f,g).match(rb)||[],b=0;b<d.length;b++)e=d[b],c=(F(e,a).exec(h)||[])[0],c&&(f=h.substr(0,h.indexOf(c)),f.length>0&&a._pf.unusedInput.push(f),h=h.slice(h.indexOf(c)+c.length),j+=c.length),Pb[e]?(c?a._pf.empty=!1:a._pf.unusedTokens.push(e),H(e,c,a)):a._strict&&!c&&a._pf.unusedTokens.push(e);a._pf.charsLeftOver=i-j,h.length>0&&a._pf.unusedInput.push(h),a._isPm&&a._a[ib]<12&&(a._a[ib]+=12),a._isPm===!1&&12===a._a[ib]&&(a._a[ib]=0),I(a),u(a)}function M(a){return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e})}function N(a){return a.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function O(a){var b,c,d,e,f;if(0===a._f.length)return a._pf.invalidFormat=!0,a._d=new Date(0/0),void 0;for(e=0;e<a._f.length;e++)f=0,b=g({},a),v(b),b._f=a._f[e],L(b),w(b)&&(f+=b._pf.charsLeftOver,f+=10*b._pf.unusedTokens.length,b._pf.score=f,(null==d||d>f)&&(d=f,c=b));g(a,c||b)}function P(a){var b,c=a._i,d=Db.exec(c);if(d){for(a._pf.iso=!0,b=4;b>0;b--)if(d[b]){a._f=Fb[b-1]+(d[6]||" ");break}for(b=0;4>b;b++)if(Gb[b][1].exec(c)){a._f+=Gb[b][0];break}Ab.exec(c)&&(a._f+="Z"),L(a)}else a._d=new Date(c)}function Q(b){var c=b._i,d=ob.exec(c);c===a?b._d=new Date:d?b._d=new Date(+d[1]):"string"==typeof c?P(b):k(c)?(b._a=c.slice(0),I(b)):l(c)?b._d=new Date(+c):"object"==typeof c?J(b):b._d=new Date(c)}function R(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function S(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function T(a,b){if("string"==typeof a)if(isNaN(a)){if(a=b.weekdaysParse(a),"number"!=typeof a)return null}else a=parseInt(a,10);return a}function U(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function V(a,b,c){var d=eb(Math.abs(a)/1e3),e=eb(d/60),f=eb(e/60),g=eb(f/24),h=eb(g/365),i=45>d&&["s",d]||1===e&&["m"]||45>e&&["mm",e]||1===f&&["h"]||22>f&&["hh",f]||1===g&&["d"]||25>=g&&["dd",g]||45>=g&&["M"]||345>g&&["MM",eb(g/30)]||1===h&&["y"]||["yy",h];return i[2]=b,i[3]=a>0,i[4]=c,U.apply({},i)}function W(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=bb(a).add("d",f),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function X(a,b,c,d,e){var f,g,h=new Date(Date.UTC(a,0)).getUTCDay();return c=null!=c?c:e,f=e-h+(h>d?7:0),g=7*(b-1)+(c-e)+f+1,{year:g>0?a:a-1,dayOfYear:g>0?g:s(a-1)+g}}function Y(a){var b=a._i,c=a._f;return"undefined"==typeof a._pf&&v(a),null===b?bb.invalid({nullInput:!0}):("string"==typeof b&&(a._i=b=A().preparse(b)),bb.isMoment(b)?(a=g({},b),a._d=new Date(+b._d)):c?k(c)?O(a):L(a):Q(a),new e(a))}function Z(a,b){bb.fn[a]=bb.fn[a+"s"]=function(a){var c=this._isUTC?"UTC":"";return null!=a?(this._d["set"+c+b](a),bb.updateOffset(this),this):this._d["get"+c+b]()}}function $(a){bb.duration.fn[a]=function(){return this._data[a]}}function _(a,b){bb.duration.fn["as"+a]=function(){return+this/b}}function ab(a){var b=!1,c=bb;"undefined"==typeof ender&&(this.moment=a?function(){return!b&&console&&console.warn&&(b=!0,console.warn("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.")),c.apply(null,arguments)}:bb)}for(var bb,cb,db="2.4.0",eb=Math.round,fb=0,gb=1,hb=2,ib=3,jb=4,kb=5,lb=6,mb={},nb="undefined"!=typeof module&&module.exports,ob=/^\/?Date\((\-?\d+)/i,pb=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,qb=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/,rb=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g,sb=/(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g,tb=/\d\d?/,ub=/\d{1,3}/,vb=/\d{3}/,wb=/\d{1,4}/,xb=/[+\-]?\d{1,6}/,yb=/\d+/,zb=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,Ab=/Z|[\+\-]\d\d:?\d\d/i,Bb=/T/i,Cb=/[\+\-]?\d+(\.\d{1,3})?/,Db=/^\s*\d{4}-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d:?\d\d|Z)?)?$/,Eb="YYYY-MM-DDTHH:mm:ssZ",Fb=["YYYY-MM-DD","GGGG-[W]WW","GGGG-[W]WW-E","YYYY-DDD"],Gb=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d{1,3}/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],Hb=/([\+\-]|\d\d)/gi,Ib="Date|Hours|Minutes|Seconds|Milliseconds".split("|"),Jb={Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6},Kb={ms:"millisecond",s:"second",m:"minute",h:"hour",d:"day",D:"date",w:"week",W:"isoWeek",M:"month",y:"year",DDD:"dayOfYear",e:"weekday",E:"isoWeekday",gg:"weekYear",GG:"isoWeekYear"},Lb={dayofyear:"dayOfYear",isoweekday:"isoWeekday",isoweek:"isoWeek",weekyear:"weekYear",isoweekyear:"isoWeekYear"},Mb={},Nb="DDD w W M D d".split(" "),Ob="M D H h m s w W".split(" "),Pb={M:function(){return this.month()+1},MMM:function(a){return this.lang().monthsShort(this,a)},MMMM:function(a){return this.lang().months(this,a)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(a){return this.lang().weekdaysMin(this,a)},ddd:function(a){return this.lang().weekdaysShort(this,a)},dddd:function(a){return this.lang().weekdays(this,a)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return i(this.year()%100,2)},YYYY:function(){return i(this.year(),4)},YYYYY:function(){return i(this.year(),5)},gg:function(){return i(this.weekYear()%100,2)},gggg:function(){return this.weekYear()},ggggg:function(){return i(this.weekYear(),5)},GG:function(){return i(this.isoWeekYear()%100,2)},GGGG:function(){return this.isoWeekYear()},GGGGG:function(){return i(this.isoWeekYear(),5)},e:function(){return this.weekday()},E:function(){return this.isoWeekday()},a:function(){return this.lang().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.lang().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return q(this.milliseconds()/100)},SS:function(){return i(q(this.milliseconds()/10),2)},SSS:function(){return i(this.milliseconds(),3)},SSSS:function(){return i(this.milliseconds(),3)},Z:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+i(q(a/60),2)+":"+i(q(a)%60,2)},ZZ:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+i(q(10*a/6),4)},z:function(){return this.zoneAbbr()},zz:function(){return this.zoneName()},X:function(){return this.unix()}},Qb=["months","monthsShort","weekdays","weekdaysShort","weekdaysMin"];Nb.length;)cb=Nb.pop(),Pb[cb+"o"]=c(Pb[cb],cb);for(;Ob.length;)cb=Ob.pop(),Pb[cb+cb]=b(Pb[cb],2);for(Pb.DDDD=b(Pb.DDD,3),g(d.prototype,{set:function(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(a){return this._months[a.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(a){return this._monthsShort[a.month()]},monthsParse:function(a){var b,c,d;for(this._monthsParse||(this._monthsParse=[]),b=0;12>b;b++)if(this._monthsParse[b]||(c=bb.utc([2e3,b]),d="^"+this.months(c,"")+"|^"+this.monthsShort(c,""),this._monthsParse[b]=new RegExp(d.replace(".",""),"i")),this._monthsParse[b].test(a))return b},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(a){return this._weekdays[a.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(a){return this._weekdaysShort[a.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(a){return this._weekdaysMin[a.day()]},weekdaysParse:function(a){var b,c,d;for(this._weekdaysParse||(this._weekdaysParse=[]),b=0;7>b;b++)if(this._weekdaysParse[b]||(c=bb([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b},_longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D YYYY",LLL:"MMMM D YYYY LT",LLLL:"dddd, MMMM D YYYY LT"},longDateFormat:function(a){var b=this._longDateFormat[a];return!b&&this._longDateFormat[a.toUpperCase()]&&(b=this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a]=b),b},isPM:function(a){return"p"===(a+"").toLowerCase().charAt(0)},_meridiemParse:/[ap]\.?m?\.?/i,meridiem:function(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},calendar:function(a,b){var c=this._calendar[a];return"function"==typeof c?c.apply(b):c},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)},pastFuture:function(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)},ordinal:function(a){return this._ordinal.replace("%d",a)},_ordinal:"%d",preparse:function(a){return a},postformat:function(a){return a},week:function(a){return W(a,this._week.dow,this._week.doy).week},_week:{dow:0,doy:6},_invalidDate:"Invalid date",invalidDate:function(){return this._invalidDate}}),bb=function(b,c,d,e){return"boolean"==typeof d&&(e=d,d=a),Y({_i:b,_f:c,_l:d,_strict:e,_isUTC:!1})},bb.utc=function(b,c,d,e){var f;return"boolean"==typeof d&&(e=d,d=a),f=Y({_useUTC:!0,_isUTC:!0,_l:d,_i:b,_f:c,_strict:e}).utc()},bb.unix=function(a){return bb(1e3*a)},bb.duration=function(a,b){var c,d,e,g=bb.isDuration(a),h="number"==typeof a,i=g?a._input:h?{}:a,j=null;return h?b?i[b]=a:i.milliseconds=a:(j=pb.exec(a))?(c="-"===j[1]?-1:1,i={y:0,d:q(j[hb])*c,h:q(j[ib])*c,m:q(j[jb])*c,s:q(j[kb])*c,ms:q(j[lb])*c}):(j=qb.exec(a))&&(c="-"===j[1]?-1:1,e=function(a){var b=a&&parseFloat(a.replace(",","."));return(isNaN(b)?0:b)*c},i={y:e(j[2]),M:e(j[3]),d:e(j[4]),h:e(j[5]),m:e(j[6]),s:e(j[7]),w:e(j[8])}),d=new f(i),g&&a.hasOwnProperty("_lang")&&(d._lang=a._lang),d},bb.version=db,bb.defaultFormat=Eb,bb.updateOffset=function(){},bb.lang=function(a,b){var c;return a?(b?y(x(a),b):null===b?(z(a),a="en"):mb[a]||A(a),c=bb.duration.fn._lang=bb.fn._lang=A(a),c._abbr):bb.fn._lang._abbr},bb.langData=function(a){return a&&a._lang&&a._lang._abbr&&(a=a._lang._abbr),A(a)},bb.isMoment=function(a){return a instanceof e},bb.isDuration=function(a){return a instanceof f},cb=Qb.length-1;cb>=0;--cb)p(Qb[cb]);for(bb.normalizeUnits=function(a){return n(a)},bb.invalid=function(a){var b=bb.utc(0/0);return null!=a?g(b._pf,a):b._pf.userInvalidated=!0,b},bb.parseZone=function(a){return bb(a).parseZone()},g(bb.fn=e.prototype,{clone:function(){return bb(this)},valueOf:function(){return+this._d+6e4*(this._offset||0)},unix:function(){return Math.floor(+this/1e3)},toString:function(){return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._offset?new Date(+this):this._d},toISOString:function(){return D(bb(this).utc(),"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var a=this;return[a.year(),a.month(),a.date(),a.hours(),a.minutes(),a.seconds(),a.milliseconds()]},isValid:function(){return w(this)},isDSTShifted:function(){return this._a?this.isValid()&&m(this._a,(this._isUTC?bb.utc(this._a):bb(this._a)).toArray())>0:!1},parsingFlags:function(){return g({},this._pf)},invalidAt:function(){return this._pf.overflow},utc:function(){return this.zone(0)},local:function(){return this.zone(0),this._isUTC=!1,this},format:function(a){var b=D(this,a||bb.defaultFormat);return this.lang().postformat(b)},add:function(a,b){var c;return c="string"==typeof a?bb.duration(+b,a):bb.duration(a,b),j(this,c,1),this},subtract:function(a,b){var c;return c="string"==typeof a?bb.duration(+b,a):bb.duration(a,b),j(this,c,-1),this},diff:function(a,b,c){var d,e,f=this._isUTC?bb(a).zone(this._offset||0):bb(a).local(),g=6e4*(this.zone()-f.zone());return b=n(b),"year"===b||"month"===b?(d=432e5*(this.daysInMonth()+f.daysInMonth()),e=12*(this.year()-f.year())+(this.month()-f.month()),e+=(this-bb(this).startOf("month")-(f-bb(f).startOf("month")))/d,e-=6e4*(this.zone()-bb(this).startOf("month").zone()-(f.zone()-bb(f).startOf("month").zone()))/d,"year"===b&&(e/=12)):(d=this-f,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-g)/864e5:"week"===b?(d-g)/6048e5:d),c?e:h(e)},from:function(a,b){return bb.duration(this.diff(a)).lang(this.lang()._abbr).humanize(!b)},fromNow:function(a){return this.from(bb(),a)},calendar:function(){var a=this.diff(bb().zone(this.zone()).startOf("day"),"days",!0),b=-6>a?"sameElse":-1>a?"lastWeek":0>a?"lastDay":1>a?"sameDay":2>a?"nextDay":7>a?"nextWeek":"sameElse";return this.format(this.lang().calendar(b,this))},isLeapYear:function(){return t(this.year())},isDST:function(){return this.zone()<this.clone().month(0).zone()||this.zone()<this.clone().month(5).zone()},day:function(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=T(a,this.lang()),this.add({d:a-b})):b},month:function(a){var b,c=this._isUTC?"UTC":"";return null!=a?"string"==typeof a&&(a=this.lang().monthsParse(a),"number"!=typeof a)?this:(b=this.date(),this.date(1),this._d["set"+c+"Month"](a),this.date(Math.min(b,this.daysInMonth())),bb.updateOffset(this),this):this._d["get"+c+"Month"]()},startOf:function(a){switch(a=n(a)){case"year":this.month(0);case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a?this.weekday(0):"isoWeek"===a&&this.isoWeekday(1),this},endOf:function(a){return a=n(a),this.startOf(a).add("isoWeek"===a?"week":a,1).subtract("ms",1)},isAfter:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)>+bb(a).startOf(b)},isBefore:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)<+bb(a).startOf(b)},isSame:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)===+bb(a).startOf(b)},min:function(a){return a=bb.apply(null,arguments),this>a?this:a},max:function(a){return a=bb.apply(null,arguments),a>this?this:a},zone:function(a){var b=this._offset||0;return null==a?this._isUTC?b:this._d.getTimezoneOffset():("string"==typeof a&&(a=G(a)),Math.abs(a)<16&&(a=60*a),this._offset=a,this._isUTC=!0,b!==a&&j(this,bb.duration(b-a,"m"),1,!0),this)},zoneAbbr:function(){return this._isUTC?"UTC":""},zoneName:function(){return this._isUTC?"Coordinated Universal Time":""},parseZone:function(){return"string"==typeof this._i&&this.zone(this._i),this},hasAlignedHourOffset:function(a){return a=a?bb(a).zone():0,0===(this.zone()-a)%60},daysInMonth:function(){return r(this.year(),this.month())},dayOfYear:function(a){var b=eb((bb(this).startOf("day")-bb(this).startOf("year"))/864e5)+1;return null==a?b:this.add("d",a-b)},weekYear:function(a){var b=W(this,this.lang()._week.dow,this.lang()._week.doy).year;return null==a?b:this.add("y",a-b)},isoWeekYear:function(a){var b=W(this,1,4).year;return null==a?b:this.add("y",a-b)},week:function(a){var b=this.lang().week(this);return null==a?b:this.add("d",7*(a-b))},isoWeek:function(a){var b=W(this,1,4).week;return null==a?b:this.add("d",7*(a-b))},weekday:function(a){var b=(this.day()+7-this.lang()._week.dow)%7;return null==a?b:this.add("d",a-b)},isoWeekday:function(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)},get:function(a){return a=n(a),this[a]()},set:function(a,b){return a=n(a),"function"==typeof this[a]&&this[a](b),this},lang:function(b){return b===a?this._lang:(this._lang=A(b),this)}}),cb=0;cb<Ib.length;cb++)Z(Ib[cb].toLowerCase().replace(/s$/,""),Ib[cb]);Z("year","FullYear"),bb.fn.days=bb.fn.day,bb.fn.months=bb.fn.month,bb.fn.weeks=bb.fn.week,bb.fn.isoWeeks=bb.fn.isoWeek,bb.fn.toJSON=bb.fn.toISOString,g(bb.duration.fn=f.prototype,{_bubble:function(){var a,b,c,d,e=this._milliseconds,f=this._days,g=this._months,i=this._data;i.milliseconds=e%1e3,a=h(e/1e3),i.seconds=a%60,b=h(a/60),i.minutes=b%60,c=h(b/60),i.hours=c%24,f+=h(c/24),i.days=f%30,g+=h(f/30),i.months=g%12,d=h(g/12),i.years=d},weeks:function(){return h(this.days()/7)},valueOf:function(){return this._milliseconds+864e5*this._days+2592e6*(this._months%12)+31536e6*q(this._months/12)},humanize:function(a){var b=+this,c=V(b,!a,this.lang());return a&&(c=this.lang().pastFuture(b,c)),this.lang().postformat(c)},add:function(a,b){var c=bb.duration(a,b);return this._milliseconds+=c._milliseconds,this._days+=c._days,this._months+=c._months,this._bubble(),this},subtract:function(a,b){var c=bb.duration(a,b);return this._milliseconds-=c._milliseconds,this._days-=c._days,this._months-=c._months,this._bubble(),this},get:function(a){return a=n(a),this[a.toLowerCase()+"s"]()},as:function(a){return a=n(a),this["as"+a.charAt(0).toUpperCase()+a.slice(1)+"s"]()},lang:bb.fn.lang,toIsoString:function(){var a=Math.abs(this.years()),b=Math.abs(this.months()),c=Math.abs(this.days()),d=Math.abs(this.hours()),e=Math.abs(this.minutes()),f=Math.abs(this.seconds()+this.milliseconds()/1e3);return this.asSeconds()?(this.asSeconds()<0?"-":"")+"P"+(a?a+"Y":"")+(b?b+"M":"")+(c?c+"D":"")+(d||e||f?"T":"")+(d?d+"H":"")+(e?e+"M":"")+(f?f+"S":""):"P0D"}});for(cb in Jb)Jb.hasOwnProperty(cb)&&(_(cb,Jb[cb]),$(cb.toLowerCase()));_("Weeks",6048e5),bb.duration.fn.asMonths=function(){return(+this-31536e6*this.years())/2592e6+12*this.years()},bb.lang("en",{ordinal:function(a){var b=a%10,c=1===q(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c}}),nb?(module.exports=bb,ab(!0)):"function"==typeof define&&define.amd?define("moment",function(b,c,d){return d.config().noGlobal!==!0&&ab(d.config().noGlobal===a),bb}):ab()}).call(this);

// Livestamp.js / v1.1.2 / (c) 2012 Matt Bradley / MIT License
;(function(d,g){var h=1E3,i=!1,e=d([]),j=function(b,a){var c=b.data("livestampdata");"number"==typeof a&&(a*=1E3);b.removeAttr("data-livestamp").removeData("livestamp");a=g(a);g.isMoment(a)&&!isNaN(+a)&&(c=d.extend({},{original:b.contents()},c),c.moment=g(a),b.data("livestampdata",c).empty(),e.push(b[0]))},k=function(){i||(f.update(),setTimeout(k,h))},f={update:function(){d("[data-livestamp]").each(function(){var a=d(this);j(a,a.data("livestamp"))});var b=[];e.each(function(){var a=d(this),c=a.data("livestampdata");
if(void 0===c)b.push(this);else if(g.isMoment(c.moment)){var e=a.html(),c=c.moment.fromNow();if(e!=c){var f=d.Event("change.livestamp");a.trigger(f,[e,c]);f.isDefaultPrevented()||a.html(c)}}});e=e.not(b)},pause:function(){i=!0},resume:function(){i=!1;k()},interval:function(b){if(void 0===b)return h;h=b}},l={add:function(b,a){"number"==typeof a&&(a*=1E3);a=g(a);g.isMoment(a)&&!isNaN(+a)&&(b.each(function(){j(d(this),a)}),f.update());return b},destroy:function(b){e=e.not(b);b.each(function(){var a=
d(this),c=a.data("livestampdata");if(void 0===c)return b;a.html(c.original?c.original:"").removeData("livestampdata")});return b},isLivestamp:function(b){return void 0!==b.data("livestampdata")}};d.livestamp=f;d(function(){f.resume()});d.fn.livestamp=function(b,a){l[b]||(a=b,b="add");return l[b](this,a)}})(jQuery,moment);



/********************
 * INTERNAL FUNCTIONS
 ********************/

/* parse_url php.js (http://phpjs.org) */
function parse_url(e,t){var n,r=["source","scheme","authority","userInfo","user","pass","host","port","relative","path","directory","file","query","fragment"],i=this.php_js&&this.php_js.ini||{},s=i["phpjs.parse_url.mode"]&&i["phpjs.parse_url.mode"].local_value||"php",o={php:/^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,strict:/^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,loose:/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/};var u=o[s].exec(e),a={},f=14;while(f--){if(u[f]){a[r[f]]=u[f]}}if(t){return a[t.replace("PHP_URL_","").toLowerCase()]}if(s!=="php"){var l=i["phpjs.parse_url.queryKey"]&&i["phpjs.parse_url.queryKey"].local_value||"queryKey";o=/(?:^|&)([^&=]*)=?([^&]*)/g;a[l]={};n=a[r[12]]||"";n.replace(o,function(e,t,n){if(t){a[l][t]=n}})}delete a.source;return a};

/* empty php.js (http://phpjs.org) */
function empty(e){var t,n,r,i;var s=[t,null,false,0,"","0"];for(r=0,i=s.length;r<i;r++){if(e===s[r]){return true}}if(typeof e==="object"){for(n in e){return false}return true}return false};

/* str_repeat php.js (http://phpjs.org) */
function str_repeat(e,t){var n="";while(true){if(t&1){n+=e}t>>=1;if(t){e+=e}else{break}}return n}

/* explode php.js (http://phpjs.org) */
function explode(e,t,n){if(arguments.length<2||typeof e=="undefined"||typeof t=="undefined")return null;if(e===""||e===false||e===null)return false;if(typeof e=="function"||typeof e=="object"||typeof t=="function"||typeof t=="object"){return{0:""}}if(e===true)e="1";e+="";t+="";var r=t.split(e);if(typeof n==="undefined")return r;if(n===0)n=1;if(n>0){if(n>=r.length)return r;return r.slice(0,n-1).concat([r.slice(n-1).join(e)])}if(-n>=r.length)return[];r.splice(r.length+n);return r};

/* implode php.js (http://phpjs.org) */
function implode(e,t){var n="",r="",i="";if(arguments.length===1){t=e;e=""}if(typeof t==="object"){if(Object.prototype.toString.call(t)==="[object Array]"){return t.join(e)}for(n in t){r+=i+t[n];i=e}return r}return t};

/* array_keys php.js (http://phpjs.org) */
function array_keys(e,t,n){var r=typeof t!=="undefined",i=[],s=!!n,o=true,u="";if(e&&typeof e==="object"&&e.change_key_case){return e.keys(t,n)}for(u in e){if(e.hasOwnProperty(u)){o=true;if(r){if(s&&e[u]!==t){o=false}else if(e[u]!=t){o=false}}if(o){i[i.length]=u}}}return i};

/* array_values php.js (http://phpjs.org) */
function array_values(e){var t=[],n="";if(e&&typeof e==="object"&&e.change_key_case){return e.values()}for(n in e){t[t.length]=e[n]}return t};

/* sprintf php.js (http://phpjs.org) */
function sprintf(){var e=/%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;var t=arguments,n=0,r=t[n++];var i=function(e,t,n,r){if(!n){n=" "}var i=e.length>=t?"":Array(1+t-e.length>>>0).join(n);return r?e+i:i+e};var s=function(e,t,n,r,s,o){var u=r-e.length;if(u>0){if(n||!s){e=i(e,r,o,n)}else{e=e.slice(0,t.length)+i("",u,"0",true)+e.slice(t.length)}}return e};var o=function(e,t,n,r,o,u,a){var f=e>>>0;n=n&&f&&{2:"0b",8:"0",16:"0x"}[t]||"";e=n+i(f.toString(t),u||0,"0",false);return s(e,n,r,o,a)};var u=function(e,t,n,r,i,o){if(r!=null){e=e.slice(0,r)}return s(e,"",t,n,i,o)};var a=function(e,r,a,f,l,c,h){var p;var d;var v;var m;var g;if(e=="%%"){return"%"}var y=false,b="",w=false,E=false,S=" ";var x=a.length;for(var T=0;a&&T<x;T++){switch(a.charAt(T)){case" ":b=" ";break;case"+":b="+";break;case"-":y=true;break;case"'":S=a.charAt(T+1);break;case"0":w=true;break;case"#":E=true;break}}if(!f){f=0}else if(f=="*"){f=+t[n++]}else if(f.charAt(0)=="*"){f=+t[f.slice(1,-1)]}else{f=+f}if(f<0){f=-f;y=true}if(!isFinite(f)){throw new Error("sprintf: (minimum-)width must be finite")}if(!c){c="fFeE".indexOf(h)>-1?6:h=="d"?0:undefined}else if(c=="*"){c=+t[n++]}else if(c.charAt(0)=="*"){c=+t[c.slice(1,-1)]}else{c=+c}g=r?t[r.slice(0,-1)]:t[n++];switch(h){case"s":return u(String(g),y,f,c,w,S);case"c":return u(String.fromCharCode(+g),y,f,c,w);case"b":return o(g,2,E,y,f,c,w);case"o":return o(g,8,E,y,f,c,w);case"x":return o(g,16,E,y,f,c,w);case"X":return o(g,16,E,y,f,c,w).toUpperCase();case"u":return o(g,10,E,y,f,c,w);case"i":case"d":p=+g||0;p=Math.round(p-p%1);d=p<0?"-":b;g=d+i(String(Math.abs(p)),c,"0",false);return s(g,d,y,f,w);case"e":case"E":case"f":case"F":case"g":case"G":p=+g;d=p<0?"-":b;v=["toExponential","toFixed","toPrecision"]["efg".indexOf(h.toLowerCase())];m=["toString","toUpperCase"]["eEfFgG".indexOf(h)%2];g=d+Math.abs(p)[v](c);return s(g,d,y,f,w)[m]();default:return e}};return r.replace(e,a)};

/* is_int php.js (http://phpjs.org) */
function is_int(e){return e===+e&&isFinite(e)&&!(e%1)};

/* is_float php.js (http://phpjs.org) */
function is_float(e){return+e===e&&(!isFinite(e)||!!(e%1))};

/* array_slice (http://phpjs.org) */
function array_slice(e,t,n,r){var i="";if(Object.prototype.toString.call(e)!=="[object Array]"||r&&t!==0){var s=0,o={};for(i in e){s+=1;o[i]=e[i]}e=o;t=t<0?s+t:t;n=n===undefined?s:n<0?s+n-t:n;var u={};var a=false,f=-1,l=0,c=0;for(i in e){++f;if(l>=n){break}if(f==t){a=true}if(!a){continue}++l;if(this.is_int(i)&&!r){u[c++]=e[i]}else{u[i]=e[i]}}return u}if(n===undefined){return e.slice(t)}else if(n>=0){return e.slice(t,t+n)}else{return e.slice(t,n)}};

/* array_merge (http://phpjs.org) */
function array_merge(){var e=Array.prototype.slice.call(arguments),t=e.length,n,r={},i="",s=0,o=0,u=0,a=0,f=Object.prototype.toString,l=true;for(u=0;u<t;u++){if(f.call(e[u])!=="[object Array]"){l=false;break}}if(l){l=[];for(u=0;u<t;u++){l=l.concat(e[u])}return l}for(u=0,a=0;u<t;u++){n=e[u];if(f.call(n)==="[object Array]"){for(o=0,s=n.length;o<s;o++){r[a++]=n[o]}}else{for(i in n){if(n.hasOwnProperty(i)){if(parseInt(i,10)+""===i){r[a++]=n[i]}else{r[i]=n[i]}}}}}return r};

/* change node name. @jakov, @Andrew Whitaker and @jazzbo ( stackoverflow ) */
$.fn.setElementType=function(e){var t=[];$(this).each(function(){var n={};$.each(this.attributes,function(e,t){n[t.nodeName]=t.nodeValue});var r=$("<"+e+"/>",n).append($(this).contents());$(this).replaceWith(r);t.push(r)});return $(t)};

/* $.hasAttr() */
(function(jQuery){jQuery.fn.hasAttr=function(name){for(vari=0,l=this.length;i<l;i++){if(!!(this.attr(name)!==undefined)){returntrue;}}returnfalse;};})(jQuery);
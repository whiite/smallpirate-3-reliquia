<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sprinkle - Asset management library
 * 
 * @author 		Edmundas Kondrašovas <as@edmundask.lt>
 * @license		http://www.opensource.org/licenses/MIT
 */

$config['assets']['jquery-with-ui'] = array
(
	'type'    			=>	'js',
	'combine' 			=>	false,
	'minify'  			=>	false,
	'src'				=>  preg_match('~msie([\d]*)~i', str_replace(' ', '', $_SERVER['HTTP_USER_AGENT'])) ? 'js/jquery-ie.js' : 'js/jquery.js'
);

$config['assets']['ie-css'] = array
(
	'type'    			=>	'css',
	'combine' 			=>	false,
	'minify'  			=>	false,
	'src'				=>	'css/ie.css'
);

$config['assets']['main-css'] = array
(
	'type'    			=>	'css',
	'combine' 			=>	false,
	'minify'  			=>	false,
	'src'				=>	'css/main.css'
);

$config['assets']['theme-css'] = array
(
	'type'    			=>	'css',
	'combine' 			=>	false,
	'minify'  			=>	false,
	'src'				=>	'css/main.css'
);

$config['assets']['main-js'] = array
(
	'type'    			=>	'js',
	'combine' 			=>	false,
	'minify'  			=>	false,
	'src'				=>	'js/main.js'
);

/*
|-------------------------------------------------------------------------
| Asset groups
|-------------------------------------------------------------------------
*/

$config['assets']['main-styles'] = array
(
	'type'  		=>	'group',
	'assets'		=>	array('main.css', 'theme-css')
);

$config['assets']['main-scripts'] = array
(
	'type'			=>	'group',
	'assets'		=>	array('jquery-with-ui', 'main-js')
);